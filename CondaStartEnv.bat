@echo off
@REM here
@Call "E:\Anaconda3\Scripts\activate.bat"
D:
cd %mlagents%

set proj_path="D:\UnityProjects\ThorML\ThorHammerMLA\Assets\ML-Agents_Settings
echo ML Agents
echo Project: %proj_path%"


set trainer_path=%proj_path%\config\ThorTrainerConfig.yaml"
set env_path="D:\UnityProjects\Builds\ThorML(il2cpp)\Hammer Brawl.exe"
set cur_path=%proj_path%\config\Curricula"
set train_cmd=mlagents-learn %trainer_path% --train --cpu --env=%env_path%
set /p choice=Use curriculum [y\n]?

IF "%choice%"=="y" goto UseCurriculum
IF "%choice%"=="n" goto IgnoreCurriculum

:UseCurriculum
set train_cmd=%train_cmd% --curriculum=%cur_path%
goto Finish

:IgnoreCurriculum
goto Finish

:Finish
echo %train_cmd%
%train_cmd%
pause