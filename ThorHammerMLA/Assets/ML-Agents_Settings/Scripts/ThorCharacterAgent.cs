﻿using UnityEngine;

using System;
using System.Collections.Generic;
using System.Linq;

using MLAgents;

#if UNITY_WEBGL
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif
using UnityUseful.Misc;

public class ThorCharacterAgent : Agent
{
    [SerializeField] ThorArea m_area;
    [Space]
    [SerializeField] Character m_character;
    [SerializeField, Reorderable] List<ReceiveDamageModule> m_receive_dmg_module;
    [Space]

    [Header("Settings:")]
    [SerializeField] string m_walls_tag = "ArenaWalls";
    [SerializeField] string m_hammer_tag = "Hammer";
    [SerializeField] string m_initialize_tag = "agent";
    [Space]
    [SerializeField] float m_search_radius = 15;
    [SerializeField] float m_sight_threshold = 10f;
    [SerializeField] float m_min_distance_threshold = 5f;
    [Space]
    [SerializeField] LayerMask m_sight_layer;

    [Header("Rewards:")]
    [SerializeField] float m_enemy_hit_reward = 1f;
    [SerializeField] float m_health_loss_reward = -.5f;
    [SerializeField] float m_walls_hit_reward = -0.01f;
    [SerializeField] float m_no_closest_enemy_reward = -0.001f;
    [SerializeField] float m_no_actions_reward = -0.001f;
    [SerializeField] float m_too_close_reward = -0.0005f;
    [SerializeField] float m_loking_towards_enemy_reward = 0.001f;

    [Header("Runtime:")]
    [SerializeField] float m_current_reward;
    [Space]
    [SerializeField] bool m_success;
    [Space]
    [SerializeField, Reorderable] List<float> m_observations = new List<float>();
    [SerializeField, Reorderable] List<float> m_actions = new List<float>();
    [SerializeField, Reorderable] List<string> m_labels = new List<string>();

    [Header("Enemy:")]
    [SerializeField] Character m_closest_enemy;
    [Space]
    [SerializeField] Vector3 m_local_dir_to_enemy;
    [SerializeField] float m_distance_to_enemy;
    [SerializeField] float m_player_enemy_angle;
    [SerializeField] int m_enemy_hp;

    [Header("Gizmos:")]
    [SerializeField] bool m_show_gizmos;

    public float SightRadius { get => m_search_radius; set => m_search_radius = value; }
    public bool Success { get => m_success; set => m_success = value; }
    public Character Character { get => m_character;}
    public List<float> Actions { get => m_actions; set => m_actions = value; }

    void Start()
    {
        m_character.HealthModule.DamageReceivers.ForEach(x => x.tag = m_initialize_tag);
    }
    #region Agent Override
    public override void InitializeAgent()
    {
        m_character = GetComponentInParent<Character>();

        m_character.OnPointsChanged += (points, delta) =>
        {
            AddReward(m_enemy_hit_reward);
        };

        foreach (var dmg_module in m_receive_dmg_module)
        {
            dmg_module.OnCollision += col => HandleCollision(col);
        }
        name = "ML Agent";
    }
    public override void CollectObservations()
    {
        SelfUpdate();

        var norm_rot = transform.eulerAngles / 180f - Vector3.one;  //Player Rot normalized [-1:1]
        var pos = /*m_area.transform.position -*/ transform.localPosition; //Player POS relative to Area
        var enemy_pos = Vector3.zero;
        m_enemy_hp = 0;

        if (m_closest_enemy)
        {
            enemy_pos = /*m_area.transform.position -*/ m_closest_enemy.transform.localPosition; // Enemy POS relative to Area

            m_distance_to_enemy = Vector3.Distance(transform.position, m_closest_enemy.transform.position);
            m_local_dir_to_enemy = transform.InverseTransformDirection((m_closest_enemy.transform.position - transform.position)).normalized;
            m_player_enemy_angle = Vector3.Angle(transform.forward, transform.TransformDirection(m_local_dir_to_enemy)) * (m_local_dir_to_enemy.x < 0 ? 1 : -1);
            m_enemy_hp = m_closest_enemy.HealthModule.Health;
        }
        else
        {
            m_local_dir_to_enemy = Vector3.zero;
            m_player_enemy_angle = 0f;
            m_distance_to_enemy = 0f;
            m_enemy_hp = 0;
        }

        AddVectorObs(pos); // 3
        AddVectorObs(norm_rot); //3


        AddVectorObs(m_character.MoveModule.Rb.velocity); //3
        AddVectorObs(m_character.MoveModule.Rb.angularVelocity); //3


        AddVectorObs(enemy_pos); //3
        AddVectorObs(m_local_dir_to_enemy); //3
        AddVectorObs(m_distance_to_enemy); //1
        AddVectorObs(m_min_distance_threshold); //1
        AddVectorObs(m_player_enemy_angle / 180f);  //1; Angle between Player`s Forward & Enemy [-1:1]
        AddVectorObs(m_enemy_hp);

        //AddVectorObs(m_character.IsInputBlocked);
        AddVectorObs(m_character.ThrowModule.HasHammer);
        AddVectorObs(m_character.ThrowModule.CurrentAttackCooldown);

        m_observations.Clear();
        m_observations.AddRange(collectObservationsSensor.Observations);
    }
    public override void AgentAction(float[] vectorAction)
    {
        if (!m_character.IsInputBlocked)
        {
            m_actions.Clear();
            m_actions.AddRange(vectorAction);

            var dir = Vector2.zero;
            var no_move = false;
            var no_rot = false;

            if (m_PolicyFactory.brainParameters.vectorActionSpaceType == SpaceType.Discrete)
            {
                if (vectorAction[1] == 2)
                {
                    dir.x -= 1;
                }
                else if (vectorAction[1] == 1)
                {
                    dir.x += 1;
                }
                else
                {
                    no_move = true;
                }

                if (vectorAction[0] == 1)
                {
                    dir.y += 1;
                }
                else if (vectorAction[0] == 2)
                {
                    dir.y -= 1;
                }
                else
                {
                    no_rot = true;
                }

                if (vectorAction[2] == 1)
                {
                    ThrowHammerAndGetItBack();
                }
            }
            else
            {
                dir = new Vector2(vectorAction[0], vectorAction[1]);
                if (vectorAction[2] > 0)
                {
                    m_character.ThrowHammer();
                }
                if (vectorAction[3] > 0)
                {
                    m_character.HammerGetBack();
                }
            }
            m_character.MoveModule.Direction = dir;
            //m_character.transform.Rotate(dir.x * Vector3.up, Time.deltaTime * 90);
            //m_character.MoveModule.Rb.AddForce(dir.y * 8 * m_character.transform.forward, ForceMode.VelocityChange);

            if (m_closest_enemy)
            {
                if (Mathf.Abs(m_player_enemy_angle) < 0.1f)
                {
                    AddReward(m_loking_towards_enemy_reward); // reward for loking towards enemy;

                }
                if (m_distance_to_enemy < m_min_distance_threshold)
                {
                    var influence = m_distance_to_enemy / m_min_distance_threshold;
                    AddReward(m_too_close_reward*influence);
                }
            }
            else
            {
                AddReward(m_no_closest_enemy_reward); //punish for no closest enemy
            }

            AddReward(m_no_actions_reward); // default time penalty
        }
    }
    public override float[] Heuristic()
    {
        if (m_PolicyFactory.brainParameters.vectorActionSpaceType == SpaceType.Discrete)
        {
            var input = new float[3];
            if (Input.GetKey(KeyCode.W))
            {
                input[0] = 1;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                input[0] = 2;
            }
            else
            {
                input[0] = 0;
            }


            if (Input.GetKey(KeyCode.A))
            {
                input[1] = 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                input[1] = 2;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                input[2] = 1;
            }
            else
            {
                input[2] = 0;
            }
            return input;
        }
        else
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                ThrowHammerAndGetItBack();
            }
            var result = new float[] { Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetKeyDown(KeyCode.Space) ? .5f : 0, Input.GetKeyDown(KeyCode.LeftControl) ? .5f : 0 };
            return result;
        }
    }
    public override void AgentOnDone()
    {
        Destroy(m_character.ThrowModule.Hammer.gameObject);
        Destroy(m_character.gameObject);
    }
    public override void AgentReset()
    {

    }
    #endregion

    public void HandleCollision(Collision collision)
    {
        var collider = collision.collider;
        if (collider.CompareTag(m_walls_tag))
        {
            //Debug.LogError($"Collision: {collision.gameObject.name}, tag: {collision.gameObject.tag}",collision.gameObject);
            AddReward(m_walls_hit_reward);
        }
        else if (collider.CompareTag(m_hammer_tag))
        {
            AddReward(m_health_loss_reward);
        }
    }
    Character FindClosestEnemy(Vector3 cur_pos, float m_search_radius, int m_sight_layer)
    {
        var colls = Physics.OverlapSphere(transform.position, m_search_radius, m_sight_layer).Where(x =>
        {
            var receiver = x.GetComponent<ReceiveDamageModule>();
            return receiver && !receiver.HealthModule.IsDead && receiver.HealthModule.GetComponent<Character>() != m_character;
        }).Select(x => x.GetComponent<ReceiveDamageModule>().HealthModule.GetComponent<Character>());
        Character closest_target = null;
        //Debug.LogError($"Colliders: {colls.Count()}, {string.Join(",", colls.ToList())}");

        if (colls.Count() > 0)
        {
            try
            {
                var closest = colls.Min(x => (distance: Vector3.Distance(x.transform.position, cur_pos), character: x));
                closest_target = closest.character;
            }
            catch (Exception e)
            {
                Debug.Log($"Failed to find Closest: {string.Join("; ", colls)}.\n{e}");
            }

            //Debug.LogWarning($"{name} closest: {closest.character.name}, distance: {closest.distance}");
            return closest_target;
        }
        else
        {
            return null;
        }
    }
    async void ThrowHammerAndGetItBack()
    {
        m_character.ThrowHammer();

#if UNITY_WEBGL
        this.TimerV(m_character.ThrowModule.AttackDelay,()=> 
        {
        this.TimerV(.1f,() => m_character.HammerGetBack());
        });
#elif !UNITY_WEBGL
        await Delay(m_character.ThrowModule.AttackDelay);
        await Delay(.1f); 
        m_character.HammerGetBack();
#endif

    }
    void SelfUpdate()
    {
        var new_enemy = FindClosestEnemy(transform.position, m_search_radius, m_sight_layer);
        m_closest_enemy = new_enemy;

        m_current_reward = GetCumulativeReward();

    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (m_show_gizmos)
        {
            var action = "";

            for (int i = 0; i < m_actions.Count; i++)
            {
                var safe_index = (int)Mathf.Repeat(i, m_labels.Count);
                action += $"\n{m_labels[safe_index]} { m_actions[i]}";
            }

            var text = $"\n\nR: {m_current_reward:0.00}" +
                $"\nClosest: {(m_closest_enemy != null ? m_closest_enemy.name : "-")}" +
                $"\n\n{action}"
                ;

            var position = transform.position;

            UnityEditor.Handles.Label(position, text);
            var length = 5;


            Gizmos.DrawLine(position, position + transform.TransformDirection(m_local_dir_to_enemy) * length);

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(position, position + transform.forward * length);
        }
    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = m_closest_enemy != null ? Color.red : Color.cyan;
        Gizmos.DrawWireSphere(transform.position, m_search_radius);
    }
#endif
}
