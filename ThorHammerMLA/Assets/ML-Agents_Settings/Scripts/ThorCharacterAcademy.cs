﻿using UnityEngine;

using MLAgents;

using UnityUseful.Misc;

public class ThorCharacterAcademy : Academy
{
    [SerializeField, Reorderable] ThorArea[] m_areas;
    [Space]
    [SerializeField] bool m_enable_learn;

    [Header("Academy stats:")]
    [SerializeField] int m_games_played;
    [SerializeField] int m_ml_winners;
    [SerializeField, Range(0, 1)] float m_percent;

    public int GamesPlayed
    {
        get => m_games_played;
        set
        {
            m_games_played = value;
            m_percent = m_ml_winners / (float)value;
        }
    }

    public int MlWinners
    {
        get => m_ml_winners;
        set
        {
            m_ml_winners = value;
            m_percent = value / (float)m_games_played;
        }
    }

    public override void AcademyReset()
    {
        Debug.Log("<b>Academy Reset</b>");
        if (m_enable_learn)
        {
            if (m_areas == null)
            {
                m_areas = FindObjectsOfType<ThorArea>();
            }

            foreach (var area in m_areas)
            {
                area.player_count = (int)resetParameters["player_count"];
                area.spawn_distance = resetParameters["spawn_distance"];

                if (area.Gm is ThorMLGameManager t_gm)
                {
                    t_gm.sight_distance = resetParameters["sight_distance"];
                }


                area.ResetArea();
            }
        }
    }


#if UNITY_EDITOR
    [ContextMenu("Find All")]
    public void FindAll()
    {
        m_areas = FindObjectsOfType<ThorArea>();
    }
    [Header("Grid Areas:")]
    [SerializeField] Vector3 m_area_grid = new Vector3(3, 0, 3);
    [SerializeField] Vector3 m_offset = new Vector3(100, 10, 100);

    [ContextMenu("Grid Child Areas")]
    public void GridChildAreas()
    {
        FindAll();
        var index = 0;

        for (int i = 0; i < m_area_grid.x; i++)
        {
            for (int j = 0; j < m_area_grid.y; j++)
            {
                for (int k = 0; k < m_area_grid.z; k++)
                {
                    if (m_areas.Length > index)
                    {
                        var area = m_areas[index];
                        var target_transform = area.transform;
                        var new_pos = new Vector3(i, j, k);

                        new_pos.Scale(m_offset);
                        target_transform.position = new_pos;
                        index++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        Debug.Log($"Academy.GridAreas Finished! {m_area_grid}");
    } 
#endif
}
