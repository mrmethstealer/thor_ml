﻿using TMPro;
using UnityEngine;

using System.Collections;
#if !UNITY_WEBGL
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityUseful.AsyncExtensions; 
#endif

public class LookAtText : MonoBehaviour
{
    [SerializeField] Camera m_cam;
    [SerializeField] Transform m_cam_transform;
    [Space]
    [SerializeField] TextMeshPro m_world_text;
    [SerializeField] Transform m_world_text_transform;

    [Header("Data Sources:")]
    [SerializeField] HealthModule m_health_module;
    [SerializeField] ThorCharacterAgent m_m_agent;

    void Awake()
    {
        m_cam = Camera.main;
        m_cam_transform = m_cam.transform.parent;
    }
    void Start() 
    {
        UpdateTexts();
    }
    void Update() 
    {
        m_world_text_transform.LookAt(m_cam_transform);
    }

#if !UNITY_WEBGL
    async Task UpdateTexts()
    {
        while (enabled)
        {
            await AsyncExtensions.Delay(.25f);

            var str = "";

            if (m_health_module)
            {
                str += m_health_module.name;
                str += "\nH: " + m_health_module.Health;
            }
            if (m_m_agent)
            {
                str += "\nR: " + m_m_agent.GetCumulativeReward();
            }

            m_world_text.text = str;
        }
    }
#elif UNITY_WEBGL
    void UpdateTexts()
    {
        StartCoroutine(UpdateTextsRoutine());
    }
    IEnumerator UpdateTextsRoutine() 
    {
        var interval = new WaitForSeconds(.25f);

        while (enabled)
        {
            yield return interval;

            var str = "";

            if (m_health_module)
            {
                str += m_health_module.name;
                str += "\nH: " + m_health_module.Health;
            }
            if (m_m_agent)
            {
                str += "\nR: " + m_m_agent.GetCumulativeReward();
            }

            m_world_text.text = str;
        }
    }
#endif
}
