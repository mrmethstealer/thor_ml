﻿using MLAgents;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityUseful.Misc;
#if !UNITY_WEBGL
using UnityUseful.AsyncExtensions; 
#elif UNITY_WEBGL
using UnityUseful.CorotineInstructionsWrappers;
#endif

public class ThorArea : Area
{
    [Header("Controllers:")]
    [SerializeField] Spawner m_spawner;
    [SerializeField] GameManager m_gm;
    [SerializeField] ThorCharacterAcademy m_academy;

    [Header("Visualization:")]
    [SerializeField] MeshRenderer m_mesh;

    [Header("Tint Materials:")]
    [SerializeField] Material m_base_mat;
    [SerializeField] Material m_succ_mat;
    [SerializeField] Material m_fail_mat;


    [Header("Other:")]
    [SerializeField] Transform m_parent;
    [SerializeField] LayerMask m_target_layer;
    [Space]
    [SerializeField] public float spawn_distance;
    [SerializeField] public int player_count;

    public ThorMLGameManager Gm { get => m_gm as ThorMLGameManager; }
    public ThorCharacterAcademy Academy { get => m_academy; }

    void Start()
    {
        m_base_mat = m_mesh.material;
        m_academy = FindObjectOfType<ThorCharacterAcademy>();
    }

    [ContextMenu("ResetArea")]
    public override void ResetArea()
    {
        //Debug.Log("Reset Area",gameObject);
        m_spawner.Distance = spawn_distance;
        m_spawner.Spawn(player_count, parent: m_parent);
    }

#if !UNITY_WEBGL
    public async void SwapMaterial(bool success)
    {
        m_mesh.material = success ? m_succ_mat : m_fail_mat;

        await AsyncExtensions.Delay(.5f, use_scale: false); 

        m_mesh.material = m_base_mat;
    }
#elif UNITY_WEBGL

    public void SwapMaterial(bool success)
    {
        m_mesh.material = success ? m_succ_mat : m_fail_mat;

        this.TimerV(.5f, () =>
        {

            m_mesh.material = m_base_mat;
        });
    }
#endif
}
