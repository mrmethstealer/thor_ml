﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CameraMLDemonstrate : MonoBehaviour
{
    [SerializeField] Camera m_cam;
    [SerializeField] Transform m_cam_transform;
    [Space]
    [SerializeField] Vector3 m_input;
    [Space]
    [SerializeField] float m_cam_speed=100;
    [SerializeField] float m_scroll_speed=100;

    void Start()
    {
        m_cam = GetComponent<Camera>();
        m_cam_transform = m_cam.transform.parent;
    }

    // Update is called once per frame
    void Update()
    {
        m_input = new Vector3(Input.GetAxis("Mouse ScrollWheel") * m_scroll_speed, -Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical"));
        m_cam_transform.position += m_cam_transform.InverseTransformDirection(m_input * m_cam_speed * Time.unscaledDeltaTime);
    }
}
