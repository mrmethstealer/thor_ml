﻿using UnityEngine;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityUseful.Misc;

public class ThorMLGameManager : GameManager
{
    [SerializeField, Reorderable] List<ThorCharacterAgent> m_ml_agents = new List<ThorCharacterAgent>();
    [SerializeField, Reorderable] List<Character> m_ai_agents = new List<Character>();
    [Space]
    [SerializeField] ThorArea m_area;
    [SerializeField] ThorCharacterAcademy m_academy;
    [Space]
    [SerializeField] float m_reward_restart_threshold = -5;
    [SerializeField] public float sight_distance;

    void Awake()
    {
        m_academy = FindObjectOfType<ThorCharacterAcademy>();
    }

    public override void InitPlayerData(PlayerData data)
    {
        //Debug.Log($"InitPlayerData data: {data}",gameObject);

        var character = data.Character;
        m_characters.Add(character);
        m_players_data.Add(data);
        var ml_agent = character.GetComponentInChildren<ThorCharacterAgent>(includeInactive: true);

        if (m_ml_agents.Count == 0)
        {
            Destroy(character.GetComponent<AIModule>());
            Destroy(character.GetComponent<UnityEngine.AI.NavMeshAgent>());

            ml_agent.gameObject.SetActive(true);
            ml_agent.SightRadius = sight_distance;
            m_ml_agents.Add(ml_agent);
            character.name += " (ML)";
        }
        else
        {
            Destroy(ml_agent.gameObject);
            m_ai_agents.Add(character);
            character.name += " (AI)";
        }

        data.Character.HealthModule.Init((int)m_area.Academy.resetParameters["player_health"]);
        data.Character.HealthModule.OnDeath.AddListener(async () =>
        {

            var hammer = data.Character.HealthModule.KilledBy.GetComponent<Hammer>();
            if (hammer)
            {
                //Debug.Log($"<b>GameManager.Shield.OnDeath:</b> +{m_player_kill_reward} to {hammer.Owner}, local:{data.Character.IsLocal}", hammer.Owner);
                hammer.Owner.Points += m_player_kill_reward;
            }
            data.Character.IsInputBlocked = true;

            var alive_ml_agents = m_ml_agents.FindAll(x => !x.Character.HealthModule.IsDead);
            if (alive_ml_agents.Count == 0)
            {
                FinishGame(GameEndReason.Lastman, new PlayerData(null, null, hammer.Owner));
            }
            else
            {
                var alive_bots = m_ai_agents.FindAll(x => !x.HealthModule.IsDead);
                if (alive_bots.Count == 0)
                {
                    FinishGame(GameEndReason.Lastman, new PlayerData(null, null, hammer.Owner));
                }
            }
            data.Character.ThrowModule.Hammer.CollidersEnabled = false;
        });
    }

    public async override void FinishGame(GameEndReason reason, PlayerData winner)
    {
        if (m_phase == GamePhase.Game)
        {
            var winner_char = winner?.Character;

            Debug.Log($"<b>ThorMLGameManager.FinishGame</b> area: {m_area.name}, reason: {reason}, winner: {winner_char?.name ?? "empty"}", m_area);
            m_phase = GamePhase.Finish;
            m_winner_data = winner;

            var is_winner_ml = winner_char?.GetComponentInChildren<ThorCharacterAgent>();
            if (winner_char)
            {

                if (is_winner_ml)
                {
                    m_academy.MlWinners++;
                }
            }
            m_area.SwapMaterial(success: is_winner_ml);
            m_academy.GamesPlayed++;

            Reset();
            m_area.ResetArea();
        }
    }
    public override void StartGame()
    {
        m_phase = GamePhase.Game;
        m_characters.ForEach(x => x.IsInputBlocked = false);
    }
    public override void Reset()
    {
        //Debug.Log("ThorMLGameManager.Reset"); 

        m_ml_agents.ForEach(x => { x.Success = false; x.Done(); });
        m_ai_agents.ForEach(x =>
        {
            Destroy(x.ThrowModule.Hammer.gameObject);
            Destroy(x.gameObject);
        });

        m_ai_agents.Clear();
        m_ml_agents.Clear();
        m_players_data.Clear();
        m_characters.Clear();
        m_winner_data = null;

        m_phase = GamePhase.Init;
    }
    void FixedUpdate()
    {
        var below_treshold = m_ml_agents.Count > 0 && m_ml_agents.All(x => x.GetCumulativeReward() < m_reward_restart_threshold);
        if (below_treshold)
        {
            FinishGame(GameEndReason.Timer, null);
        }
        if (m_ml_agents.All(x=>x.IsMaxStepReached()))
        {
            FinishGame(GameEndReason.Timer, null);
        }
    }
}
