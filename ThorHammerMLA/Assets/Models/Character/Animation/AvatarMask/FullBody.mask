%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: FullBody
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bake:Root_M
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_L/Bake:Knee_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_L/Bake:Knee_L/Bake:Ankle_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_R/Bake:Knee_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Hip_R/Bake:Knee_R/Bake:Ankle_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Neck_M
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Neck_M/Bake:Head_M
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_L/Bake:Shoulder_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_L/Bake:Shoulder_L/Bake:Elbow_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_L/Bake:Shoulder_L/Bake:Elbow_L/Bake:Wrist_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_L/Bake:Shoulder_L/Bake:Elbow_L/Bake:Wrist_L/Bake:joint1_L
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_R/Bake:Shoulder_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_R/Bake:Shoulder_R/Bake:Elbow_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_R/Bake:Shoulder_R/Bake:Elbow_R/Bake:Wrist_R
    m_Weight: 1
  - m_Path: Bake:Root_M/Bake:Spine1_M/Bake:Chest_M/Bake:Scapula_R/Bake:Shoulder_R/Bake:Elbow_R/Bake:Wrist_R/Bake:joint1_R
    m_Weight: 1
  - m_Path: Character_NewUvs1
    m_Weight: 1
