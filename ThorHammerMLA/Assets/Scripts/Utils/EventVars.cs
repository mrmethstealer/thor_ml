﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UnityUseful.EventVars
{
    #region EventVars
    [Serializable]
    public abstract class EventVar<T, TEvent> where TEvent : UnityEvent<T>
    {
        [SerializeField] T m_value = default(T);
        public T Value
        {
            get
            {
                return m_value;
            }
            set
            {
                if (!m_value.Equals(value))
                {
                    m_value = value;
                    OnValueChanged?.Invoke(m_value);
                }
            }
        }
        public TEvent OnValueChanged;

        public static implicit operator T(EventVar<T, TEvent> obj)
        {
            return obj.Value;
        }

        public EventVar()
        {
        }
        public EventVar(T _value)
        {
            Value = _value;
        }
    }

    [Serializable]
    public class BoolVar : EventVar<bool, BoolEvent> { }
    [Serializable]
    public class FloatVar : EventVar<float, FloatEvent> { }
    [Serializable]
    public class IntVar : EventVar<int, IntEvent>
    {
        public IntVar(int val) : base(_value: val) { }
    }
    [Serializable]
    public class StringVar : EventVar<string, StringEvent>
    {
        public StringVar(string val) : base(_value: val) { }
    }
    [Serializable]
    public class GOVar : EventVar<GameObject, GameObjectEvent> { }



    [Serializable]
    public class BoolEvent : UnityEvent<bool> { }
    [Serializable]
    public class IntEvent : UnityEvent<int> { }
    [Serializable]
    public class FloatEvent : UnityEvent<float> { }
    [Serializable]
    public class StringEvent : UnityEvent<string> { }
    [Serializable]
    public class GameObjectEvent : UnityEvent<GameObject> { }
    #endregion
}
