﻿using System;
using System.Collections;
using UnityEngine;

using static UnityUseful.IEnumeratorExtension.IEnumeratorExtension;

namespace UnityUseful.CorotineInstructionsWrappers
{
    public static class CIWrappers
    {
        #region CoroutineWrappers

        public static Coroutine ReverseTimerV(this MonoBehaviour mono, int seconds, Action<int> OnSecondTick, Func<bool> OnConditionCheck, params Action[] OnFinish)
        {
            return mono.StartCoroutine(ReverseTimer(seconds, OnSecondTick, OnConditionCheck, OnFinish));
        }
        public static Coroutine ReverseTimerV(this MonoBehaviour mono, float seconds, Action<float, float> OnSecondTick, Func<bool> break_condition, params Action[] finals)
        {
            return mono.StartCoroutine(ReverseTimer(seconds, OnSecondTick, break_condition, finals));
        }
        public static Coroutine ProgressTimerV(this MonoBehaviour mono, float time, Action<float, float> prog_and_delta,bool unscaled = false, params Action[] finals)
        {
            return mono.StartCoroutine(ProgressTimer(time, prog_and_delta, unscaled, finals));
        }
        public static Coroutine TimerV(this MonoBehaviour mono, float time, Action final)
        {
            return mono.StartCoroutine(Timer(time, final));
        }
        public static Coroutine CustomUpdateV(this MonoBehaviour mono, float time, Func<bool> condition, Action OnUpdate)
        {
            return mono.StartCoroutine(CustomUpdate(time, condition, OnUpdate));
        }
        public static Coroutine WaitUntilV(this MonoBehaviour mono, Func<bool> predicate, Action final)
        {
            return mono.StartCoroutine(WaitUntil(predicate, final));
        }
        public static Coroutine ChainV(this MonoBehaviour mono, params IEnumerator[] enums)
        {
            return mono.StartCoroutine(ChainIEnum(mono, enums));
        }

        #endregion
    }
}
