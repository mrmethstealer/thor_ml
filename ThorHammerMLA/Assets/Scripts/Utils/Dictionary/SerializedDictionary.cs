﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


    [Serializable]
    public class SerializedDictionary : Dictionary<string, float>, ISerializationCallbackReceiver
    {
        [Serializable]
        public struct ResetParameter
        {
            public string key;
            public float value;
        }

        public SerializedDictionary() { }

        public SerializedDictionary(IDictionary<string, float> dict) : base(dict)
        {
            UpdateSerializedDictionary();
        }

        void UpdateSerializedDictionary()
        {
            m_SerializedDictionary.Clear();
            foreach (var pair in this)
            {
                m_SerializedDictionary.Add(new ResetParameter { key = pair.Key, value = pair.Value });
            }
        }

        [FormerlySerializedAs("SerializedDictionary")]
        [SerializeField]
        List<ResetParameter> m_SerializedDictionary = new List<ResetParameter>();

        public void OnBeforeSerialize()
        {
            UpdateSerializedDictionary();
        }

        public void OnAfterDeserialize()
        {
            Clear();


            for (var i = 0; i < m_SerializedDictionary.Count; i++)
            {
                if (ContainsKey(m_SerializedDictionary[i].key))
                {
                    Debug.LogError("The SerializedDictionary contains the same key twice");
                }
                else
                {
                    Add(m_SerializedDictionary[i].key, m_SerializedDictionary[i].value);
                }
            }
        }
    }

