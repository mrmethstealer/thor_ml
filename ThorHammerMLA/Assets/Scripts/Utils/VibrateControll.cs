﻿using UnityEngine;

using System.Collections;
using System.Threading.Tasks;

namespace UnityUseful.Vibrate
{

    public static class VibrateControll
    {

        public static async Task Vibrate(float time = -1, int milliseconds_step = 250)
        {
#if UNITY_EDITOR
            if (GameData.Instance && GameData.Instance.EnableVibro)
            {
                var left = time;

                while (time == -1 || left > 0)
                {
                    Handheld.Vibrate();

                    await Task.Delay(milliseconds_step);

                    left -= .5f;
                }
            } 
#endif
        }
    }
}
