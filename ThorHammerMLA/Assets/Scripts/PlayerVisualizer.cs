﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Collections.Generic;
using TMPro;

public class PlayerVisualizer : MonoBehaviour
{
    [Header("Components:")]
    [SerializeField] CameraController m_cam_controller;
    [SerializeField] GameManager m_gm;
    [SerializeField] Spawner m_spawner;
    [SerializeField] List<GameObject> m_disable_components;

    [Header("UI:")]
    [SerializeField] Button m_next;
    [SerializeField] Button m_prev;
    [Space]
    [SerializeField] TextMeshProUGUI m_description;
    [Space]
    [SerializeField] GameObject m_ml_visualizer;
    [Space]
    [SerializeField] TextMeshProUGUI m_title;
    [SerializeField] List<Image> m_actions;


    [Header("Runtime:")]
    [SerializeField] Character m_current;

    void Awake()
    {
        m_prev.onClick.AddListener(() => ChangeCurrent(-1));
        m_next.onClick.AddListener(() => ChangeCurrent(1));

        m_gm.OnPregame.AddListener(() => ChangeCurrent(1));
        m_gm.OnFinish.AddListener(() => m_ml_visualizer.SetActive(false));
    }

    void OnEnable()
    {
        if (m_spawner.LocalPlayersToSpawn > 0)
        {
            gameObject.SetActive(false);
            m_ml_visualizer.SetActive(false);
        }
        else
        {
            m_disable_components.ForEach(x => x.SetActive(false));
        }
    }
    void Update()
    {
        if (m_ml_visualizer.activeSelf && m_current)
        {
            var ml = m_current.GetComponentInChildren<ThorCharacterAgent>();
            if (ml)
            {
                var actions = ml.Actions;

                for (int i = 0; i < actions.Count; i++)
                {
                    var normalized_value = (actions[i]+1)/2;
                    m_actions[i].color = Color.Lerp(Color.red,Color.green, normalized_value);
                }
            }
        }
    }
    void ChangeCurrent(int offset)
    {
        var index = m_gm.Characters.FindIndex(x => x == m_current) + offset;

        m_current = m_gm.Characters[(int)Mathf.Repeat(index, m_gm.Characters.Count)];
        m_cam_controller.Target = m_current.transform;

        var ml_agent = m_current.GetComponentInChildren<ThorCharacterAgent>();
        var is_ml_agent = (bool)ml_agent;

        m_description.text = $"Name: {m_current.Nickname}({m_current.SkinModule.Skin})\n" +
            $"IsScripted: {m_current.GetComponent<AIModule>().isActiveAndEnabled}\n" +
            $"IsLearning: {is_ml_agent}";

        m_ml_visualizer.SetActive(is_ml_agent);

        if (is_ml_agent)
        {
            m_title.text = $"Observations: {24}, Actions: {4}, Reward: [InferenceOnly]";            
        }
    }
}
