﻿using UnityEngine;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public class CameraController : Singleton<CameraController>
{
    [Header("Camera:")]
    [SerializeField] Camera m_cam;
    [SerializeField] GameManager m_gm;
    [Space]
    [SerializeField] Transform m_target;
    [SerializeField] Transform m_root;
    [Space]
    [SerializeField] Vector3 m_camera_offset;
    [SerializeField] Vector3 m_camera_angle;
    [Space]
    [SerializeField] float m_camera_speed = .5f;
    [Space]
    [SerializeField] bool m_enable_camera_controll;
    [SerializeField] bool m_use_smoothing = true;
    [SerializeField] bool m_local_space;
    [SerializeField] bool m_follow_rotation = true;
    [SerializeField] bool m_follow_position = true;

    [Header("Camera Shake:")]
    [SerializeField] Vector3 m_camera_shake_dir;
    [SerializeField] Vector4 m_ez_shake_settings = new Vector4(1f, 1f, .1f, .1f);
    [Space]
    [SerializeField] float m_camera_shake_factor = 1f;
    [SerializeField] float m_camera_shake_restore_factor = 1f;
    [Space]
    [SerializeField] bool m_camera_is_shaking;

    #region Properties
    public Transform Target
    {
        get => m_target;
        set
        {
            m_target = value;
        }
    }
    public Camera Camera { get => m_cam; }
    public bool EnableTargetFollow { get => m_enable_camera_controll; set => m_enable_camera_controll = value; }
    public bool CameraShaking { get => m_camera_is_shaking; set => m_camera_is_shaking = value; }
    public Transform Root { get => m_root; }
    #endregion

    protected override void Awake()
    {
        Application.targetFrameRate = 60;

        if (!m_root)
        {
            m_root = m_cam.transform;
        }
    }
    void Update()
    {
        var delta = Time.deltaTime * m_camera_shake_restore_factor;
        m_camera_shake_dir = new Vector3(Mathf.Max(0, m_camera_shake_dir.x - delta), Mathf.Max(0, m_camera_shake_dir.y - delta), Mathf.Max(0, m_camera_shake_dir.z - delta));
        //m_camera_is_shaking = m_camera_shake_dir.sqrMagnitude != 0;
    }
    void LateUpdate()
    {
        if (m_target && EnableTargetFollow)
        {
            var step = Time.deltaTime * m_camera_speed;
            if (m_follow_position)
            {
                var camera_point = m_local_space ? m_target.TransformPoint(m_camera_offset) : m_target.position + m_camera_offset;
                camera_point += m_camera_shake_dir;
                if (m_use_smoothing)
                {
                    camera_point = Vector3.Lerp(m_root.position, camera_point, step);
                }
                m_root.position = camera_point;
            }


            var angle = m_follow_rotation ? m_target.eulerAngles.y : 0;
            var current_target_rot = Quaternion.Euler(m_camera_angle.x, m_camera_angle.y + angle, m_camera_angle.z);
            if (m_use_smoothing)
            {
                current_target_rot = Quaternion.Lerp(m_root.rotation, current_target_rot, step);
            }
            m_root.rotation = current_target_rot;
        }
    }
#if !UNITY_WEBGL
    public async void WinnerShot(Transform target)
    {
        Debug.Log($"<b>CameraController.WinnerShot</b> {target.name}");

        EnableTargetFollow = false;

        await Delay(.5f);

        Target = target;
        var point = new GameObject("FrontPoint");

        point.transform.position = m_target.position + m_target.forward * 3 + new Vector3(0, 1f, 0);

        await ProgressTimer(1f, (prog, delta) =>
         {
             m_root.position = Vector3.Lerp(m_root.position, point.transform.position, prog);
             var dir = (target.position - m_root.position).normalized + Vector3.up * .35f;
             m_root.rotation = Quaternion.Lerp(m_root.rotation, Quaternion.LookRotation(dir, Vector3.up), prog);
         });

        //transform.LookAt(Target);
        //transform.rotation = Quaternion.Euler(m_camera_angle.x,Mathf.Abs(m_camera_angle.y), m_camera_angle.z);
    } 
#elif UNITY_WEBGL
    public void WinnerShot(Transform target)
    {
        StartCoroutine(WinnerShotRoutine(target));
    }

    IEnumerator WinnerShotRoutine(Transform target)
    {
        Debug.Log($"<b>CameraController.WinnerShot</b> {target.name}");

        EnableTargetFollow = false;

        yield return new WaitForSeconds(.5f);

        Target = target;
        var point = new GameObject("FrontPoint");

        point.transform.position = m_target.position + m_target.forward * 3 + new Vector3(0, 1f, 0);

        yield return  this.ProgressTimerV(1f, (prog, delta) =>
        {
            m_root.position = Vector3.Lerp(m_root.position, point.transform.position, prog);
            var dir = (target.position - m_root.position).normalized + Vector3.up * .35f;
            m_root.rotation = Quaternion.Lerp(m_root.rotation, Quaternion.LookRotation(dir, Vector3.up), prog);
        });

        //transform.LookAt(Target);
        //transform.rotation = Quaternion.Euler(m_camera_angle.x,Mathf.Abs(m_camera_angle.y), m_camera_angle.z);
    }
#endif

    public async void Shake(float scale = 1f)
    {
        //m_camera_shaking = true;
        //m_camera_shake_dir = Random.insideUnitSphere * m_camera_shake_factor;
        if (!m_camera_is_shaking)
        {
            m_camera_is_shaking = true;
            var scaled_power = m_ez_shake_settings * scale;
            EZCameraShake.CameraShaker.Instance.ShakeOnce(scaled_power.x, scaled_power.y, scaled_power.z, scaled_power.w);

            var delay = (int)((scaled_power.z + scaled_power.w) * 1000);

            await System.Threading.Tasks.Task.Delay(delay);

            m_camera_is_shaking = false;
        }
    }


#if UNITY_EDITOR
    [ContextMenu(nameof(ShakeOnce))]
    public void ShakeOnce() 
    {
        Shake();
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, transform.forward);
    }

    public void ShotAboveArea()
    {
        var m_cam_root = Root;
        var dest_pos = new Vector3(0, 20, 0);
        var dest_rot = Quaternion.Euler(90, -180, 0);

        if (m_gm.Phase != GamePhase.Finish)
            Target = null;

        EnableTargetFollow = false;
        this.ProgressTimerV(2f, (progress, delta) =>
        {
            if (m_gm.Phase != GamePhase.Finish)
            {
                m_cam_root.position = Vector3.Lerp(m_cam_root.position, dest_pos, progress);
                m_cam_root.rotation = Quaternion.Lerp(m_cam_root.rotation, dest_rot, progress);
            }
        });
    }
#endif
}
