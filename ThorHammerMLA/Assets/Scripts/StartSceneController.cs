﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections;
using System.Collections.Generic;

public class StartSceneController : MonoBehaviour
{
    [SerializeField] Button m_load_bots;
    [SerializeField] Button m_load_multiplayer;
    [Space]
    [SerializeField] int m_bots_scene_id = 1;
    [SerializeField] int m_multiplayer_scene_id = 2;

    void Start()
    {
        m_load_bots.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(m_bots_scene_id);
        });

        m_load_multiplayer.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(m_multiplayer_scene_id);
        });
    }
}
