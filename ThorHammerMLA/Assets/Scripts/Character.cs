﻿using UnityEngine;

using System;
using System.Collections.Generic;

#if !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif
using UnityUseful.Vibrate;
using System.Collections;

public class Character : MonoBehaviour
{
    [Header("Modules:")]
    [SerializeField] HealthModule m_health_module;
    [SerializeField] MoveModule m_move_module;
    [SerializeField] SkinModule m_skin;
    [SerializeField] AudioModule m_audio;
    [SerializeField] ThrowModule m_throw_module;

    [Header("Runtime:")]
    [SerializeField] string m_nickname;
    [SerializeField] string m_id;
    [Space]
    [SerializeField] int m_points;
    [Space]
    [SerializeField] bool m_is_input_blocked;
    [SerializeField] bool m_is_local;

    static List<string> m_names_dictionary = new List<string>()
    {
        "LazyInspiredOctopus", "IckyDancingTub", "TameSilentGolf", "SlowWiseChip", "CalmSocialCadet", "ColdNarrowDolphin", "CoolShallowLogic", "RoseAquaticKnave", "MegaNoisyHobby", "MeekGloomyStraw", "LongWindyFlag", "BoldLovelyGoalie", "RosyCaringSparrow", "EvenPastApron", "EvenPastEnd", "PinkSkillfulSign", "RedWinkingChild", "FastNinthFang", "MeekFormalVolcano", "CuteCoolTyphoon", "MadYoungFootball", "SourTastefulCone", "PureJumboClass", "DullComfyFur", "TanMarchingPhone", "PurePuffyDeal", "NewFavoriteEscape", "SoreHummingPasture", "LateFluffyShoe", "DearMajesticDeal", "RosyInspiredBrush", "SlowCivilBarn", "SafeQueenlyJacket", "NextRuddyRacecar", "DullGlitteryFence", "MeekMajorRock", "OldPoeticBin", "TinyLikeableCobbler", "FastSavoryStork", "SoreDashingHarp", "FunExtraAir", "CuteRoyalIsland", "LeftDancingTrip", "FarDartingCastle", "GladCuteDryad", "CoolJadeRhino", "DarkRedSailboat", "BigFavoriteBugle", "SlimTwirlingActress", "LeftBentShell", "MegaSoftClue", "ColdItchySilver", "KeenPleasantFish", "VastHummingBracelet", "SoreCopperDuckling", "PureFriendlyVulture", "LoudTallPotato", "SlowEvenArmy", "OpenOpenMaestro", "HugeFlappingBuddy", "AbleVioletShop", "NeatStridingFriend", "MegaIndigoHose", "BestMajorShell", "VastHangingFeet"
    };


    #region Properties

    public bool IsLocal
    {
        get => m_is_local;
        set
        {
            m_is_local = value;
            m_move_module.ShowDirectionAsPoints = value;
        }
    }

    public bool IsInputBlocked
    {
        get => m_is_input_blocked;
        set
        {
            m_is_input_blocked = value;
            m_move_module.IsMoving = false;
        }
    }
    public HealthModule HealthModule { get => m_health_module; }
    public string Id { get => m_id; }
    public string Nickname
    {
        get => m_nickname;
        set
        {
            m_nickname = value;
            name = m_nickname;
        }
    }
    public int Points
    {
        get => m_points;
        set
        {
            var dif = value - m_points;
            m_points = value;
            OnPointsChanged?.Invoke(value, dif);
        }
    }


    public SkinModule SkinModule { get => m_skin; }
    public AudioModule Audio { get => m_audio; }
    public MoveModule MoveModule { get => m_move_module; }
    public ThrowModule ThrowModule { get => m_throw_module; }

    #endregion

    #region Events
    /// <summary>
    /// 1. New Value, 2. Delta
    /// </summary>
    public event Action<int, int> OnPointsChanged;

    public event Action<float> OnStunned;
    public event Action<float> OnAttack;
    #endregion


    #region MonoCallbacks

    void Start()
    {

        m_health_module.OnDeath.AddListener(() =>
        {
            m_is_input_blocked = true;
            m_move_module.IsMoving = false;

            name = Nickname + "(Dead)";
        });

        m_throw_module.OnPower.AddListener(() =>
        {
            m_audio.PlaySound(Sound.PowerEvent);
            if (IsLocal)
            {
                CameraController.Instance.Shake();
            }
        });
    }


    void Update()
    {
        m_throw_module.AllowPowerIncreaseOverTime = !IsInputBlocked && m_move_module.IsMoving;
    }
    #endregion

    public static string IdToNickname(string id)
    {
        var random_id = UnityEngine.Random.Range(0, m_names_dictionary.Count);
        var random_element = m_names_dictionary[random_id];

#if UNITY_EDITOR
        return id.Substring(0, 3); // short name for Editor
#elif !UNITY_EDITOR
        return random_element;
#endif
    }
    public void Init(string _id)
    {
        m_id = _id;
        Nickname = IdToNickname(_id);
    }

#if !UNITY_WEBGL
    public async void ThrowHammer(Vector3? target = null, float override_cd = -1f)
    {
        //Debug.Log($"<b>Character.ThrowHammer</b> target: {target}, cd: {override_cd}");
        if (!m_is_input_blocked)
        {
            await m_throw_module.ThrowHammer(target, override_cd, cd =>
              {
                  m_is_input_blocked = true;
                  m_move_module.Direction = Vector2.zero;
                  m_audio.PlaySound(Sound.Throw);
              });

            if (!m_health_module.IsDead)
            {
                m_is_input_blocked = false;
            }
        }
    }
#elif UNITY_WEBGL
    public void ThrowHammer(Vector3? target = null, float override_cd = -1f)
    {
        //Debug.Log($"<b>Character.ThrowHammer</b> target: {target}, cd: {override_cd}");
        StartCoroutine(ThrowHammerRoutine(target, override_cd));
    }
    IEnumerator ThrowHammerRoutine(Vector3? target = null, float override_cd = -1f) 
    {
        if (!m_is_input_blocked)
        {
            yield return m_throw_module.ThrowHammer(target, override_cd, cd =>
            {
                m_is_input_blocked = true;
                m_move_module.Direction = Vector2.zero;
                m_audio.PlaySound(Sound.Throw);
            });

            if (!m_health_module.IsDead)
            {
                m_is_input_blocked = false;
            }
        }
    }
#endif


    public async void HammerGetBack()
    {
        if (!m_is_input_blocked)
        {
            m_throw_module.HammerGetBack(() =>
            {
                if (IsLocal)
                {
                    if (m_throw_module.PowerProgress > .5f)
                    {
                        CameraController.Instance.Shake(.5f);
                    }
                    VibrateControll.Vibrate(0.2f, 250);
                }
            });
        }
    } 

    public async void OnHammerHit()
    {
        //Debug.Log($"OnHammerHit {name}, hp: {HealthModule.Health}, isDead: {HealthModule.IsDead}", gameObject);

        if (HealthModule.IsDead)
        {

        }
        //else
        //{

        //    m_is_stunned = true;
        //    m_animator.SetTrigger(hash_stunned);
        //    OnStunned?.Invoke(3.5f);
        //    await Delay(3.6f);

        //    OnHammerHitRestore();
        //}
    }

    public void OnHammerHitRestore()
    {
        if (!HealthModule.IsDead)
        {
            IsInputBlocked = false;
        }
        m_move_module.Restore();

        if (!m_throw_module.HasHammer)
        {
            HammerGetBack();
        }
    }

    public void OnGroundTouch(AnimationEvent animationEvent) 
    {
        //Debug.Log($"<b>Character.OnGroundTouch</b> {animationEvent.functionName}", gameObject);      
        if (!m_audio.AdditionalSource.isPlaying)
        {
            m_audio.AdditionalSource.Play();
        }
    }

    public override string ToString()
    {
        return name;
    }
#if UNITY_EDITOR
    [ContextMenu(nameof(ThrowHammer))]
    void ThrowHammerContext()
    {
        ThrowHammer();
    }
    [ContextMenu(nameof(HammerGetBack))]
    async void HammerGetBackContext()
    {
        HammerGetBack();
    }
    [ContextMenu("OnHammerHit")]
    void OnHammerHitContext()
    {
        OnHammerHit();
    }
#endif
}
