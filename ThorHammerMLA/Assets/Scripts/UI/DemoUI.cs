﻿using UnityEngine;
using System.Collections;
using TMPro;

public class DemoUI : Singleton<DemoUI>
{
    [SerializeField] GameManager m_gm;
    [Space]
    [SerializeField] FilledCountdown m_attack_cd;
    [SerializeField] FilledCountdown m_stun_cd;
    [Space]
    [SerializeField] TextMeshProUGUI m_network_info;

    public TextMeshProUGUI NetworkInfo { get => m_network_info; }

    protected override void Awake()
    {
        base.Awake();
        m_gm.OnLocalPlayerAssigned += local_player =>
        {
            Debug.Log($"<b>UI.OnLocalPlayerAssigned</b>");
            local_player.OnAttack += m_attack_cd.ValueListner;
            local_player.OnStunned += m_stun_cd.ValueListner;
        };
    }
}
