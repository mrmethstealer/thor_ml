﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityUseful.Misc;

public class GameUI : Singleton<GameUI>
{
    [SerializeField] Camera m_cam;
    [Space]
    [SerializeField] RectTransform m_aim;
    [Space]
    [SerializeField] Transform m_from;
    [SerializeField, Reorderable] List<Transform> m_to = new List<Transform>();
    [Space]
    [SerializeField] float m_half_angle = 10;

    static Vector3 m_half_resolution;

    #region Properties
    public bool EnableAim { set => m_aim.gameObject.SetActive(value); }
    public List<Transform> Targets { get => m_to; }
    public Transform From { get => m_from; set => m_from = value; }
    public Color AimColor { set => Array.ForEach(m_aim.GetComponentsInChildren<Image>(), img => img.color = value); }
    #endregion


    protected override void Awake()
    {
        m_cam = Camera.main;
        base.Awake();
    }

    void Update()
    {
        if (m_from && m_to.Count > 0)
        {
            var ordered_angle_list = m_to.Select(temp_to_item =>
            {
                var dir = (temp_to_item.position - m_from.position).normalized;
                var angle = Vector3.Angle(m_from.forward, dir);

                var in_range = angle < m_half_angle;

                return (temp_to_item, angle, in_range);

            }).Where(x => x.in_range).OrderBy(x => x.angle);
            var exist = ordered_angle_list.Count() > 0;

            if (exist)
            {
                var closest_target = ordered_angle_list.First().temp_to_item;
                var is_dead = closest_target.GetComponent<HealthModule>().IsDead;
                var color = closest_target.GetComponent<SkinModule>().Color;
                var screen_point = m_cam.WorldToScreenPoint(closest_target.position + Vector3.up);

                screen_point -= m_half_resolution;
                m_aim.anchoredPosition = screen_point;
                AimColor = color;

                var dir = (closest_target.position - m_from.position).normalized;

                //if (Physics.Raycast(m_from.position, closest_target.position, out var hit_info,100,LayerMask.NameToLayer("Player")))
                //{
                //    Debug.Log($"<b>GameUI.Update</b> Raycats: {hit_info.collider.name}, tag: {hit_info.collider.tag}", hit_info.collider);
                //}
                EnableAim = !is_dead;
            }
            else
            {
                EnableAim = false;
            }
        }
    }
    void OnRectTransformDimensionsChange()
    {
        //Debug.Log($"<b>GameUI.OnRectTransformDimensionsChange</b> {Screen.currentResolution}");
        m_half_resolution = new Vector3(Screen.width * .5f, Screen.height * .5f);
    }
    //void OnDrawGizmos()
    //{
    //    foreach (var item in m_to)
    //    {
    //        var dir = (item.position - m_from.position).normalized;
    //        Gizmos.color = Color.green;
    //        Gizmos.DrawRay(m_from.transform.position,dir);
    //    }
    //}
}
