﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using System.Collections.Generic;

using UnityUseful.Misc;



public class UI : Singleton<UI>
{
    const string score_format = "<color={3}>{0} - Pts: {1}, {2}";

    [SerializeField] Canvas m_canvas;
    [SerializeField] GameManager m_gm;

    [Header("Message Panels:")]
    [SerializeField] BattleLoggerUI m_battle_logger_ui;
    [SerializeField] MessagePanel m_center_panel;


    [Header("Gameplay UI:")]
    [SerializeField] Button m_pause_b;
    [SerializeField] Button m_play_b;
    [Space]
    [SerializeField] TextMeshProUGUI m_time_t;
    [SerializeField] TextMeshProUGUI m_score_t;
    [Space]
    [SerializeField] TMP_InputField m_nickname_if;

    [Header("Other Refs:")]
    [SerializeField] List<GameObject> m_disable_on_death;

    [Header("Settings:")]
    [SerializeField] Toggle m_sound_tgl;
    [SerializeField] Toggle m_vibro_tgl;


    #region Properties
    public TextMeshProUGUI CountdownTimerText { get => m_time_t; set => m_time_t = value; }
    public Canvas Canvas { get => m_canvas; }
    public TMP_InputField NicknameIF { get => m_nickname_if; }
    public BattleLoggerUI BattleLoggerUI { get => m_battle_logger_ui; }
    public MessagePanel CenterMessagePanel { get => m_center_panel; }
    public TextMeshProUGUI Score { get => m_score_t;}
    #endregion


    protected override void Awake()
    {
        base.Awake();

        m_nickname_if.text = GameData.Instance.Nickname;
        m_nickname_if.onEndEdit.AddListener(str => GameData.Instance.Nickname = str);
    }
    void Start()
    {
        m_pause_b.onClick.AddListener(() => m_gm.IsPaused = !m_gm.IsPaused);
        m_play_b.onClick.AddListener(() => 
        {
            //var count = GameData.Instance.SpawnCount;
            //GameManager.Instance.Spawner.Spawn(count);
        }); // UnityListners: Disable StartScreen, Enable PlayModeScreen 

        m_sound_tgl.isOn = GameData.Instance.EnableSound;
        m_vibro_tgl.isOn = GameData.Instance.EnableVibro;

        m_sound_tgl.onValueChanged.AddListener(isOn=> {  });
    }

    public void Init(string player_name, Character character)
    {
        var color = character.SkinModule.Color;
        var color_html = color.ToHTMLColor();
        var hp_module = character.HealthModule;

        hp_module.OnHealthChanged.AddListener(health =>
        {
            //Debug.Log($"<b>UI.character.Shield.OnHealthChanged</b> {health}", shield);

            //var score_text = new_score_ui_item;

            UpdateScoreUI(player_name, character, 0);
        });
        hp_module.OnDeath.AddListener(() =>
        {
            var attacker = hp_module.KilledBy;
            if (attacker)
            {
                Debug.Log($"<b>UI.HealthModule.OnDeath</b> {attacker.name}->{character.name}");
                var killer = attacker.GetComponent<Hammer>().Owner;
                UpdateScoreUI(player_name, character, killer.Points);

                var name = $"<color={character.SkinModule.Color.ToHTMLColor()}>{character.Nickname}</color>";
                var killer_name = $"<color={killer.SkinModule.Color.ToHTMLColor()}>{killer.Nickname}</color>";

                m_battle_logger_ui.ShowMessage($"{killer_name} <sprite name=Dead> {name} ", duration: 2f);

                if (character.IsLocal)
                {
                    m_disable_on_death.ForEach(x => x.SetActive(false));
                }
            }
        });

        character.OnPointsChanged += async (points,dif) =>
        {
            UpdateScoreUI(player_name, character, character.Points);
        };
    }

    public void UpdateScoreUI(string player_name, Character character, int points)
    {
        var char_health = character.HealthModule.Health;
        var character_color = character.SkinModule.Color.ToHTMLColor();
        var health_msg = (char_health == 0 ? "Eliminated" : "Health: " + char_health.ToString());
        //m_hp_parent.Find(player_name).GetComponent<TextMeshProUGUI>().text = string.Format(score_format, player_name, points, health_msg, character_color);
    }
}
