﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

using System;

using UnityEngine.Events;

#if UNITY_WEBGL
using System.Collections;
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public class FilledCountdown : MonoBehaviour
{
    [Header("StatusBar:")]
    [SerializeField] TextMeshProUGUI m_text_mesh;
    [Space]
    [SerializeField] Image m_filled_image;
    [Space]
    [SerializeField] string m_text_format = "Value: {0}";

    void Start()
    {
        m_filled_image.fillAmount = 0f;

        m_text_mesh.text = string.Empty;
    }
    public void Init(UnityEvent<float> m_event) 
    {
        m_event.AddListener(ValueListner);
    }

    public void Init(Action<float> m_event)
    {
        m_event += ValueListner;
    }

#if UNITY_WEBGL
    public void ValueListner(float duration)
    {
        StartCoroutine(ValueListenerRoutine(duration));
    }

    IEnumerator ValueListenerRoutine(float duration)
    {
        //Debug.Log($"<b>UI.OnLocalPlayerAssigned.OnAttack</b> {duration}");
        var left = 0f;
        do
        {
            left += Time.deltaTime;
            m_filled_image.fillAmount = 1 - left / duration;
            m_text_mesh.text = string.Format(m_text_format, (duration - left).ToString("0.0"));

            yield return null;

        } while (left < duration);

        m_filled_image.fillAmount = 0;
        m_text_mesh.text = string.Empty;
    }
#elif !UNITY_WEBGL
    public async void ValueListner(float duration)
    {
        //Debug.Log($"<b>UI.OnLocalPlayerAssigned.OnAttack</b> {duration}");
        var left = 0f;
        do
        {
            left += Time.deltaTime;
            m_filled_image.fillAmount = 1 - left / duration;
            m_text_mesh.text = string.Format(m_text_format, (duration - left).ToString("0.0"));

            await Task.Yield();
            EditorCheckPlayMode();

        } while (left < duration);

        m_filled_image.fillAmount = 0;
        m_text_mesh.text = string.Empty;
    } 
#endif
}
    [Serializable]
    public class UnityFloatEvent : UnityEvent<float> { }
