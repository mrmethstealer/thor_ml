﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using System;
using System.Threading;
using System.Collections.Generic;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

using UnityUseful.Misc;
using Random = UnityEngine.Random;

public class PregameUI : MonoBehaviour
{
    [SerializeField] GameManager m_gm;

    [Header("Prefs:")]
    [SerializeField] GameObject m_player_entry_item_pref;

    [Header("Data:")]
    [SerializeField] GameData m_data;
    [Space]
    [SerializeField] TextMeshProUGUI m_countdown_t;
    [Space]
    [SerializeField] Transform m_parent;

    [Space]
    [SerializeField] Vector2 m_appearence_delay = new Vector2(.2f, .5f);

    [Header("Unity Events:")]
    [SerializeField] UnityEvent m_OnMatchmakingFinished;

    CancellationTokenSource m_countdown_cancel_token;
    void OnEnable()
    {
        StartCountdown(m_data.SpawnCount);
    }
    void OnDisable()
    {
        m_parent.DestroyChildren();
        if (m_countdown_cancel_token != null)
        {
            m_countdown_cancel_token.Cancel();
        }
    }
#if !UNITY_WEBGL
    public async void StartCountdown(int players = 11)
    {
        m_countdown_cancel_token = new CancellationTokenSource();

        var local_player_place = Random.Range(0, 2);
        var guids = new List<string>();

        for (int i = 0; i < players; i++)
        {
            var guid = Guid.NewGuid().ToString();
            var nickname = Character.IdToNickname(guid);

            if (local_player_place == i)
            {
                nickname = m_data.Nickname;
            }
            guids.Add(guid);

            var entry = Instantiate(m_player_entry_item_pref, m_parent);

            entry.GetComponentInChildren<TextMeshProUGUI>(true).text = nickname;

            var rand_offset = Random.Range(0.01f, .25f);
            CIWrappers.TimerV(this, rand_offset, () => entry.transform.GetChild(1).gameObject.SetActive(true));
        }

        for (int i = 0; i < players; i++)
        {
            var rand_delay = Random.Range(m_appearence_delay.x, m_appearence_delay.y);

            await Delay(rand_delay);

            if (m_countdown_cancel_token.IsCancellationRequested)
            {
                Debug.Log($"<b>PregameUI.StartCountdown</b> <color=red>cancel OP called</color>");
                return;
            }

            var child = m_parent.GetChild(i);

            child.GetChild(0).gameObject.SetActive(true);
            child.GetChild(1).gameObject.SetActive(false);
            if (local_player_place == i)
            {
                child.GetComponent<Image>().enabled = true;
            }

            m_countdown_t.text = (players - i - 1).ToString();
        }

        await Delay(.5f);

        m_OnMatchmakingFinished?.Invoke();
        m_gm.Spawner.Spawn(m_data.SpawnCount, guids: guids.ToArray());
    } 
#elif UNITY_WEBGL
    public void StartCountdown(int players = 11)
    {
        StartCoroutine(StartCountdownRoutine(players));
    }
    IEnumerator StartCountdownRoutine(int players) 
    {
        m_countdown_cancel_token = new CancellationTokenSource();

        var local_player_place = Random.Range(0, 2);
        var guids = new List<string>();

        for (int i = 0; i < players; i++)
        {
            var guid = Guid.NewGuid().ToString();
            var nickname = Character.IdToNickname(guid);

            if (local_player_place == i)
            {
                nickname = m_data.Nickname;
            }
            guids.Add(guid);

            var entry = Instantiate(m_player_entry_item_pref, m_parent);

            entry.GetComponentInChildren<TextMeshProUGUI>(true).text = nickname;

            var rand_offset = Random.Range(0.01f, .25f);
            CIWrappers.TimerV(this, rand_offset, () => entry.transform.GetChild(1).gameObject.SetActive(true));
        }

        for (int i = 0; i < players; i++)
        {
            var rand_delay = Random.Range(m_appearence_delay.x, m_appearence_delay.y);

            yield return new WaitForSeconds(rand_delay);

            if (m_countdown_cancel_token.IsCancellationRequested)
            {
                Debug.Log($"<b>PregameUI.StartCountdown</b> <color=red>cancel OP called</color>");
                yield break;
            }

            var child = m_parent.GetChild(i);

            child.GetChild(0).gameObject.SetActive(true);
            child.GetChild(1).gameObject.SetActive(false);
            if (local_player_place == i)
            {
                child.GetComponent<Image>().enabled = true;
            }

            m_countdown_t.text = (players - i - 1).ToString();
        }

        yield return new WaitForSeconds(.5f);

        m_OnMatchmakingFinished?.Invoke();
        m_gm.Spawner.Spawn(m_data.SpawnCount, guids: guids.ToArray());
    }
#endif
}
