﻿using TMPro;
using UnityEngine;

using System;
using System.Threading;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public class MessagePanel : MonoBehaviour
{
    [Header("Message:")]
    [SerializeField] TextMeshProUGUI m_msg_label;

    [Space]
    [SerializeField] CanvasGroup m_msg_canvas_group;

    [SerializeField] Vector3 m_base_scale;
    [Space]
    [SerializeField] bool m_user_scale_animation;
    [Space]
    [SerializeField] AnimationCurve m_scale_animation;

    CancellationTokenSource m_show_msg_token = new CancellationTokenSource();

    void Awake()
    {
        m_base_scale = m_msg_label.transform.localScale;
    }
#if UNITY_WEBGL
    public void ShowMessage(string msg, bool priority = false, float duration = 0f, float fade_in = 0f, float fade_out = 0f)
    {
        StartCoroutine(ShowMessageRoutine(msg, priority, duration, fade_in, fade_out));
    }

    IEnumerator ShowMessageRoutine(string msg, bool priority, float duration, float fade_in, float fade_out)
    {
        Debug.Log($"<b>UI.ShowMessage</b> {msg}, duration: {duration}, fade_in: {fade_in}, fade_out: {fade_out}");

        var canvas_group = m_msg_canvas_group;
        if (priority)
        {
            m_show_msg_token.Cancel();
            m_show_msg_token = new CancellationTokenSource();
        }
        else
        {
            if (canvas_group && canvas_group.alpha != 0)
            {
                yield return this.WaitUntilV(() => canvas_group.alpha == 0, null);
            }
        }

        m_msg_label.text = msg;

        if (fade_in > 0)
        {
            yield return this.ProgressTimerV(fade_in, (prog, delta) =>
            {
                canvas_group.alpha = prog;
            });
        }
        else
        {
            canvas_group.alpha = 1;
        }


        if (duration != 0)
        {
            if (m_user_scale_animation)
            {
                yield return this.ProgressTimerV(duration, (prog, delta) => m_msg_label.transform.localScale = Vector3.one * m_scale_animation.Evaluate(prog));
            }
            else
            {
                yield return new WaitForSeconds(duration);
            }

            if (fade_out > 0)
            {
                yield return this.ProgressTimerV(fade_out, (prog, delta) =>
                {
                    canvas_group.alpha = 1f - prog;
                });

            }
            canvas_group.alpha = 0;
        }
    }
#elif !UNITY_WEBGL
    public async void ShowMessage(string msg, bool priority = false, float duration = 0f, float fade_in = 0f, float fade_out = 0f)
    {
        Debug.Log($"<b>UI.ShowMessage</b> {msg}, duration: {duration}, fade_in: {fade_in}, fade_out: {fade_out}");

        var canvas_group = m_msg_canvas_group;
        if (priority)
        {
            m_show_msg_token.Cancel();
            m_show_msg_token = new CancellationTokenSource();
        }
        else
        {
            if (canvas_group && canvas_group.alpha != 0)
            {
                await WaitWhile(() => canvas_group.alpha != 0);
            }
        }

        m_msg_label.text = msg;

        if (fade_in > 0)
        {
            await ProgressTimer(fade_in, (prog, delta) =>
            {
                canvas_group.alpha = prog;
            });
        }
        else
        {
            canvas_group.alpha = 1;
        }


        if (duration != 0)
        {
            if (m_user_scale_animation)
            {
                await ProgressTimer(duration, (prog, delta) => m_msg_label.transform.localScale = Vector3.one * m_scale_animation.Evaluate(prog));
            }
            else
            {
                await Delay(duration);
            }

            if (fade_out > 0)
            {
                await ProgressTimer(fade_out, (prog, delta) =>
                {
                    canvas_group.alpha = 1f - prog;
                });

            }
            canvas_group.alpha = 0;
        }
    } 
#endif

#if UNITY_EDITOR
    [ContextMenu("ShowMessage")]
    void ShowMessage()
    {
        ShowMessage(Guid.NewGuid().ToString(), priority: true, duration: 2f, fade_out: .25f);
    }
    [ContextMenu("ShowMessage Priority:false")]
    void ShowMessageP()
    {
        ShowMessage(Guid.NewGuid().ToString(), priority: false, duration: 2f, fade_in: .25f, fade_out: .25f);
    }
#endif
}
