﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using UnityUseful.Misc;
using UnityEngine.UI;

public class OffscreenDirection : Singleton<OffscreenDirection>
{
    [Header("Init:")]
    [SerializeField] Camera m_cam;
    [Space]
    [SerializeField] GameObject m_offscreen_direction_pointer_pref;
    [Space]
    [SerializeField] Transform m_offscreen_pointer_parent;

    [Header("Runtime:")]
    [SerializeField] Transform m_target;
    [Space]
    [SerializeField, Reorderable] List<Character> m_targets;
    [SerializeField, Reorderable] List<RectTransform> m_arrows;

    void Update()
    {
        var screen_size = new Vector2(Screen.width, Screen.height);

        m_targets.RemoveAll(x => x.HealthModule.IsDead);
        m_arrows.RemoveAll(x => x == null);

        for (int i = 0; i < m_targets.Count; i++)
        {
            var character = m_targets[i];
            var view = m_cam.WorldToViewportPoint(character.transform.position);
            var is_outside_screen = view.x > 1 || view.y > 1 || view.x < 0 || view.y < 0 || view.z < 0;
            var arrow = m_arrows[i];

            arrow.gameObject.SetActive(is_outside_screen);

            var direction = (character.transform.position - m_target.position).normalized;
            direction.y = 0;
            var new_dir = m_target.InverseTransformDirection(direction);

            var angle = Vector3.Angle(m_target.forward, direction);

            //var sign = direction.x > 0 ? 1 : -1;
            var sign = new_dir.x < 0 ? 1 : -1;

            arrow.localRotation = Quaternion.Euler(0, 0, angle * sign);

            //arrow.anchoredPosition = screen_size * .5f * new Vector2(-direction.x, -direction.z);
            arrow.anchoredPosition = screen_size * .5f * new Vector2(new_dir.x, new_dir.z);

        }
    }
    public void Init(Transform local, Character[] targets)
    {
        m_target = local;
        m_targets = new List<Character>(targets);

        for (int i = 0; i < targets.Length; i++)
        {
            var target = targets[i];

            var new_arrow = CreateArrow(target.Nickname, target.SkinModule.Color);

            target.HealthModule.OnDeath.AddListener(() => Destroy(new_arrow));
        }
    }
    GameObject CreateArrow(string name, Color color)
    {
        var world_player_pointer = Instantiate(m_offscreen_direction_pointer_pref, m_offscreen_pointer_parent);
        world_player_pointer.name = "Pointer to " + name;
        world_player_pointer.GetComponentInChildren<Image>().color = color;
        world_player_pointer.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

        m_arrows.Add(world_player_pointer.GetComponent<RectTransform>());

        var color_html = color.ToHTMLColor();

        return world_player_pointer;
    }
#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        if (m_target)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(m_target.position, m_target.position + m_target.forward);
        }

        if (m_targets != null && m_targets.Count > 0)
        {
            Gizmos.color = Color.cyan;

            var character = m_targets[0];
            var dir = (character.transform.position - m_target.position).normalized;
            var local_dir = m_target.InverseTransformDirection(dir);


            Gizmos.color = character.SkinModule.Color;
            Gizmos.DrawLine(m_target.position, m_target.position + dir);

            var w_angle = Vector3.Angle(m_target.forward, dir);
            var l_angle = Vector3.Angle(m_target.forward, local_dir);

            UnityEditor.Handles.Label(m_target.position, $"{character.Nickname}({character.SkinModule.Skin})" +
                $"\nW Dir:{dir}" +
                $"\nL Dir:{local_dir}" +
                $"\nW Angle: {w_angle}" +
                $"\nL Angle: {l_angle}"
                );
            foreach (var target in m_targets)
            {
                var view = m_cam.WorldToViewportPoint(character.transform.position);


                UnityEditor.Handles.Label(target.transform.position, $"{target.Nickname}({target.SkinModule.Skin})" +
                $"\nViewport: {view}"
                );
            }
        }

    }
#endif
}
