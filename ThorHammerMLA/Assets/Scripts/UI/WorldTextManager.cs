﻿using UnityEngine;
using TMPro;

using System;
using System.Collections;
using System.Collections.Generic;

using UnityUseful.Misc;
#if !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif
using UnityEngine.UI;
using UnityUseful.CorotineInstructionsWrappers;

public class WorldTextManager : Singleton<WorldTextManager>
{
    [Header("Components:")]
    [SerializeField] Camera m_cam;
    [SerializeField] CameraController m_cam_controller;
    [SerializeField] GameManager m_gm;

    [Header("Prefs:")]
    [SerializeField, Reorderable] List<GameObject> m_world_texts_pref;

    [Header("Init Data:")]
    [SerializeField] Vector3 m_world_offset = new Vector3(0, 1.6f, 0);
    [SerializeField] Vector3 m_world_spacing = new Vector3(0, .5f, 0);
    [Space]
    [SerializeField] bool m_use_character_colors;

    List<Action> m_update_actions = new List<Action>();

    [Header("Dynamic:")]
    [SerializeField, Reorderable] List<Vector3> m_viewports = new List<Vector3>();
    [SerializeField, Reorderable] List<Vector3> m_dirs = new List<Vector3>();
    [SerializeField, Reorderable] List<Vector3> m_pointer_viewports = new List<Vector3>();
    [Space]
    [SerializeField] int m_index;
    [Space]
    [SerializeField] Transform m_relative_point;

    public Transform RelativePoint { get => m_relative_point; set => m_relative_point = value; }

    protected override void Awake()
    {
        base.Awake();
        m_cam = Camera.main;
        m_cam_controller = m_cam.GetComponent<CameraController>();
        m_gm.OnLocalPlayerAssigned += player => m_relative_point = player.transform;
    }

    void Update()
    {
        if (m_cam_controller.Target)
        {
            m_relative_point = m_cam_controller.Target;
        }
        m_update_actions.ForEach(x => x?.Invoke());
    }

    public void Init(Character character)
    {
        Debug.Log($"Init character: {character.name}"); 
        
        var color = character.SkinModule.Color;
        var color_html = color.ToHTMLColor();
        var health_text = character.HealthModule.Health.ToString();
        var nickname_text = character.Nickname.WrapWithTag("size", "50%");

        if (m_use_character_colors)
        {
            health_text = health_text.WrapWithTag(tag_value: color_html);
            nickname_text = nickname_text.WrapWithTag(tag_value: color_html);
        }

        //var world_shield_hp_text = CreateText(health_text, character.transform.position);
        var world_nickname_text = CreateText(nickname_text, character.transform.position);

        m_viewports.Add(Vector3.zero);
        m_dirs.Add(Vector3.zero);
        m_pointer_viewports.Add(Vector3.zero);

        var id = m_index;

        //var shield_transform = world_shield_hp_text.transform.parent;
        var nickname_transform = world_nickname_text.transform.parent;

        var world_hp = Instantiate(m_world_texts_pref[2]).transform;
        var world_hp_slider = world_hp.GetComponentInChildren<Slider>();
        var filler_2 = world_hp.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>();

        world_hp.name = character.Nickname;

        character.HealthModule.OnHealthChangedNormalized.AddListener(value =>
        {
            var current = world_hp_slider.value;
            var current_anchors = filler_2.anchorMin;

            world_hp_slider.value = value;


            this.ProgressTimerV(.5f, (prog, delta) =>
            {
                filler_2.anchorMin = Vector2.Lerp(current_anchors, world_hp_slider.fillRect.anchorMin, prog);
                filler_2.anchoredPosition = Vector2.zero;
            });
        });

        void update_postion()
        {
            if (m_relative_point)
            {

                world_hp.position = character.transform.position + m_world_offset + m_world_spacing * 2;
                world_hp.LookAt(m_cam.transform, Vector3.up);

                var view = m_cam.WorldToViewportPoint(character.transform.position + m_world_offset);
                m_viewports[id] = view;

                var new_pos = Vector3.zero;
                var is_outside_screen = view.x > 1 || view.y > 1 || view.x < 0 || view.y < 0;

                var dir = (character.transform.position - m_relative_point.position).normalized;

                var angle_chars = Vector3.Angle(character.transform.position, m_relative_point.position);

                var angle = Vector3.Angle(dir, m_relative_point.forward);
                var sign = dir.x > 0 ? -1f : 1f; //m_relative_point.worldToLocalMatrix.MultiplyPoint(senderGO.transform.position).x > 0 ? -1f : 1f;

                m_dirs[id] = dir;

                if (is_outside_screen)
                {

                }
                else
                {
                    nickname_transform.position = character.transform.position + m_world_offset;
                    new_pos = character.transform.position + m_world_offset;
                }
                nickname_transform.gameObject.SetActive(!is_outside_screen);
                //world_player_pointer.SetActive(is_outside_screen);

                nickname_transform.position = new_pos + m_world_spacing;

                nickname_transform.LookAt(m_cam.transform, Vector3.up);

                if (m_gm.Phase != GamePhase.Finish)
                {
                    //var cur_euler = nickname_transform.localEulerAngles;
                    //cur_euler.y = 90;
                    //nickname_transform.rotation = Quaternion.Euler(cur_euler);
                }
                else
                {
                    if (m_gm.WinnerData != null && character != m_gm.WinnerData.Character)
                    {
                        nickname_transform.gameObject.SetActive(false);
                        //world_shield_hp_text.gameObject.SetActive(false);
                    }
                }

                if (world_nickname_text.text != character.Nickname)
                {
                    var new_text = character.Nickname.WrapWithTag("size", "50%");
                    if (m_use_character_colors)
                    {
                        new_text = new_text.WrapWithTag(tag_value: color_html);
                    }

                    world_nickname_text.text = new_text;
                }
            }
        }

        m_update_actions.Add(update_postion);
        

        character.HealthModule.OnHealthChanged.AddListener(health =>
        {
            var new_text = health.ToString().WrapWithTag("size", "50%");

            if (m_use_character_colors)
            {
                new_text = new_text.WrapWithTag(tag_value: color_html);
            }
            //world_shield_hp_text.text = new_text;
        });

        character.HealthModule.OnDeath.AddListener(() =>
        {
            world_nickname_text.text = "";
            world_hp.gameObject.SetActive(false);
            //world_shield_hp_text.text = "";
        });

        character.OnPointsChanged += (pts, delta) =>
        {
            FlyingText(character, delta);
        };

        m_index++;
    }
    public TextMeshPro CreateText(string msg, Vector3 base_pos, int pref_id = 0)
    {
        var pref = m_world_texts_pref[pref_id];
        var new_shield_hp_text_item = Instantiate(pref, base_pos + m_world_offset, Quaternion.Euler(0, 0, 0));
        var new_shield_hp_text_item_text = new_shield_hp_text_item.GetComponentInChildren<TextMeshPro>();

#if UNITY_EDITOR
        new_shield_hp_text_item.name = "[WorldText] " + msg;
#endif

        new_shield_hp_text_item_text.text = msg;

        return new_shield_hp_text_item_text;
    }
#if UNITY_WEBGL
    public async void FlyingText(Character character, int diff, float fade_in = 1f, float fade_out = 1f, float height = 1f, int pref_id = 1)
    {
        StartCoroutine(FlyingTextRoutine(character, diff, fade_in, fade_out, height, pref_id));
    }

    public IEnumerator FlyingTextRoutine(Character character, int diff, float fade_in = 1f, float fade_out = 1f, float height = 1f, int pref_id = 1) 
    {
        if (character.IsLocal)
        {
            var offset = Vector3.up * 2f;
            var pos = character.transform.position + offset;
            var fly_distance = Vector3.up * height;
            var pref = m_world_texts_pref[pref_id];
            var new_ponts = Instantiate(pref, pos, Quaternion.identity);
            var new_points_t = new_ponts.GetComponentInChildren<TextMeshPro>();
            var new_posints_transform = new_ponts.transform;
            var points_color = new_points_t.color;
            var cam_transform = m_cam.transform;

            new_points_t.text = $"+{diff}";
            new_ponts.name = $"{character.name} Points +" + diff;

            yield return this.ProgressTimerV(fade_in, (prog, delta) =>
            {
                new_posints_transform.position = character.transform.position + offset + fly_distance * prog;
                new_posints_transform.LookAt(cam_transform);
                points_color.a = prog;
                new_points_t.color = points_color;
            });

            yield return this.ProgressTimerV(fade_out, (prog, delta) =>
            {
                new_posints_transform.position = character.transform.position + offset + fly_distance;
                new_posints_transform.LookAt(cam_transform);
                points_color.a = 1 - prog;
                new_points_t.color = points_color;
            });

            Destroy(new_ponts.gameObject);
        }
    }
#elif !UNITY_WEBGL
    public async void FlyingText(Character character, int diff, float fade_in = 1f, float fade_out = 1f, float height = 1f, int pref_id = 1)
    {
        if (character.IsLocal)
        {
            var offset = Vector3.up * 2f;
            var pos = character.transform.position + offset;
            var fly_distance = Vector3.up * height;
            var pref = m_world_texts_pref[pref_id];
            var new_ponts = Instantiate(pref, pos, Quaternion.identity);
            var new_points_t = new_ponts.GetComponentInChildren<TextMeshPro>();
            var new_posints_transform = new_ponts.transform;
            var points_color = new_points_t.color;
            var cam_transform = m_cam.transform;

            new_points_t.text = $"+{diff}";
            new_ponts.name = $"{character.name} Points +" + diff;

            await ProgressTimer(fade_in, (prog, delta) =>
            {
                new_posints_transform.position = character.transform.position + offset + fly_distance * prog;
                new_posints_transform.LookAt(cam_transform);
                points_color.a = prog;
                new_points_t.color = points_color;
            });

            await ProgressTimer(fade_out, (prog, delta) =>
            {
                new_posints_transform.position = character.transform.position + offset + fly_distance;
                new_posints_transform.LookAt(cam_transform);
                points_color.a = 1 - prog;
                new_points_t.color = points_color;
            });

            Destroy(new_ponts.gameObject);
        }
    } 
#endif

    void OnDrawGizmos()
    {
        var cam = Camera.main;
        var current_resolution = new Vector2(Screen.width, Screen.height);
        var center_pixels = current_resolution * .5f;
        var center_view = Vector2.one * .5f;

        Gizmos.color = Color.green;
        foreach (var dir in m_dirs)
        {
            var from = cam.ViewportToWorldPoint(center_view);
            var to = cam.ViewportToWorldPoint(((Vector2)dir+center_view).normalized);

            Gizmos.DrawLine(from, to);
        }

        //go_from.transform.position = from;
        //go_to.transform.position = to;
    }
}
