﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class ScoreItem : MonoBehaviour
{
    [Header("Texts:")]
    [SerializeField] TextMeshProUGUI m_place;
    [SerializeField] TextMeshProUGUI m_nickname;
    [SerializeField] TextMeshProUGUI m_points;

    [Header("Other")]
    [SerializeField] GameObject m_highlight_obj;
    [Space]
    [SerializeField] Image m_local;

    [Header("Color")]
    [SerializeField] Color m_local_color;
    [SerializeField] Color m_other_color;

    #region Properties

    public string Place { set => m_place.text = value; }
    public string Nickname { set => m_nickname.text = value; }
    public string Points { set => m_points.text = value; }
    public bool EnableHighlight { set => m_highlight_obj.SetActive(value); }
    public bool IsLocal 
    {
        set
        {
            m_local.color = value ? m_local_color : m_other_color;
            EnableHighlight = value;
        }
    }

    #endregion
}
