﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

using System;
using System.Linq;
using System.Collections.Generic;

using UnityUseful.Misc;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
using System.Threading.Tasks;
#endif

public class GameEndUI : Singleton<GameEndUI>
{
    [Header("GameOverPanel:")]
    [SerializeField] GameManager m_gm;
    [Space]
    [SerializeField] GameObject m_endgame_p;
    [Space]
    [SerializeField] Transform m_endgame_score_content;
    [Space]
    [SerializeField] Button m_panel_b;
    [Space]
    [SerializeField] TextMeshProUGUI m_win_place;

    [Header("Prefs:")]
    [SerializeField] ScoreItem m_scoreboard_pref;

    [Header("Static Data:")]
    [SerializeField] float m_fade_in_per_score_item = .25f;

    public string WinPlace
    {
        set
        {
            m_win_place.text = value;
        }
    }

#if UNITY_WEBGL
    public void ShowGameOverPanel(string body, Action onClick) // Remove "body"?
    {
        StartCoroutine(ShowGameOverPanelRoutine(body, onClick));
    }

    IEnumerator ShowGameOverPanelRoutine(string body, Action onClick)
    {
        Debug.Log($"<b>GameEndUI.ShowGameOverPanel</b> body: {body}");
        m_endgame_score_content.DestroyChildren();
        m_endgame_p.SetActive(true);

        var players = m_gm.Characters;
        var dead_players = players.FindAll(x => x.HealthModule.IsDead);
        var alive_players = players.FindAll(x => !x.HealthModule.IsDead);
        var empty_slots = players.Count - dead_players.Count;

        dead_players.Sort((x, y) => y.Points.CompareTo(x.Points));
        players.Sort((x, y) => y.Points.CompareTo(x.Points));
        alive_players.Sort((x, y) => y.Points.CompareTo(x.Points));

        if (m_gm.Phase == GamePhase.Finish)
        {
            for (int i = 0; i < players.Count; i++)
            {
                var player = players[i];
                if (player.IsLocal)
                {
                    WinPlace = (i + 1).ToString();
                }

                yield return CreateItem(player, i);
            }
        }
        else
        {
            for (int i = 0; i < empty_slots; i++) // draw blank for alive players
            {
                var player = alive_players[i];
                yield return CreateItem(player, i);
            }

            for (int i = empty_slots; i < players.Count; i++) // draw filled for dead
            {
                var index = i - empty_slots;
                var player = dead_players[index];

                if (player.IsLocal)
                {
                    WinPlace = (i + 1).ToString();
                }

                yield return CreateItem(player, i);

            }
        }

        m_panel_b.onClick.RemoveAllListeners();
        m_panel_b.onClick.AddListener(() => onClick?.Invoke());
    }
#elif !UNITY_WEBGL
    public async void ShowGameOverPanel(string body, Action onClick) // Remove "body"?
    {
        Debug.Log($"<b>GameEndUI.ShowGameOverPanel</b> body: {body}");
        m_endgame_score_content.DestroyChildren();
        m_endgame_p.SetActive(true);

        var players = m_gm.Characters;
        var dead_players = players.FindAll(x => x.HealthModule.IsDead);
        var alive_players = players.FindAll(x => !x.HealthModule.IsDead);
        var empty_slots = players.Count - dead_players.Count;

        dead_players.Sort((x, y) => y.Points.CompareTo(x.Points));
        players.Sort((x, y) => y.Points.CompareTo(x.Points));
        alive_players.Sort((x, y) => y.Points.CompareTo(x.Points));

        if (m_gm.Phase == GamePhase.Finish)
        {
            for (int i = 0; i < players.Count; i++)
            {
                var player = players[i];
                if (player.IsLocal)
                {
                    WinPlace = (i + 1).ToString();
                }

                await CreateItem(player, i);
            }
        }
        else
        {
            for (int i = 0; i < empty_slots; i++) // draw blank for alive players
            {
                var player = alive_players[i];
                await CreateItem(player, i);
            }

            for (int i = empty_slots; i < players.Count; i++) // draw filled for dead
            {
                var index = i - empty_slots;
                var player = dead_players[index];

                if (player.IsLocal)
                {
                    WinPlace = (i + 1).ToString();
                }

                await CreateItem(player, i);

            }
        }

        m_panel_b.onClick.RemoveAllListeners();
        m_panel_b.onClick.AddListener(() => onClick?.Invoke());
    } 
#endif

    public string GetPlaceLiterals(int id)
    {
        var str = id.ToString();
        var ending = "th";
        switch (id)
        {
            case 1: ending = "st"; break;
            case 2: ending = "nd"; break;
            case 3: ending = "rd"; break;
            default: break;
        }
        return str + ending;
    }

#if UNITY_WEBGL
    IEnumerator CreateItem(Character player, int i)
    {
        var score_item = Instantiate(m_scoreboard_pref, m_endgame_score_content) as ScoreItem;

        score_item.Place = GetPlaceLiterals(i + 1);
        score_item.Nickname = player.Nickname.ToShortForm(12);
        score_item.Points = player.Points.ToString();
        score_item.name = player.Nickname + player.Points;

        score_item.IsLocal = player.IsLocal;


        var group = score_item.GetComponent<CanvasGroup>();

        yield return this.ProgressTimerV(m_fade_in_per_score_item, (prog, delta) =>
        {
            group.alpha = prog;
        },unscaled: true);
    }
#elif !UNITY_WEBGL
    async Task CreateItem(Character player,int i) 
    {
        var score_item = Instantiate(m_scoreboard_pref, m_endgame_score_content) as ScoreItem;

        score_item.Place = GetPlaceLiterals(i + 1);
        score_item.Nickname = player.Nickname.ToShortForm(12);
        score_item.Points = player.Points.ToString();
        score_item.name = player.Nickname + player.Points;

        score_item.IsLocal = player.IsLocal;


        var group = score_item.GetComponent<CanvasGroup>();

        await ProgressTimer(m_fade_in_per_score_item, (prog, delta) =>
{
    group.alpha = prog;
}, unscaled: true); 
    }
#endif
}
