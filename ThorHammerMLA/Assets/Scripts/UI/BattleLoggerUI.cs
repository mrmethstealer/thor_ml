﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System.Threading;

#if UNITY_WEBGL
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions;
using System.Threading.Tasks;
#endif

public class BattleLoggerUI : MonoBehaviour
{
    [SerializeField] ScrollRect m_rect;
    [Space]
    [SerializeField] Transform m_parent;
    [Space]
    [SerializeField] TextMeshProUGUI m_text_pref;

    [SerializeField] Vector3 m_base_scale;
    [Space]
    [SerializeField] bool m_user_scale_animation;
    [Space]
    [SerializeField] AnimationCurve m_scale_animation;

    CancellationTokenSource m_show_msg_token = new CancellationTokenSource();


#if !UNITY_WEBGL
    public async void ShowMessage(string msg, bool priority = false, float duration = 0f, float fade_in = 0f, float fade_out = 0f)
    {
        //Debug.Log($"<b>UI.ShowMessage</b> {msg}, duration: {duration}, fade_in: {fade_in}, fade_out: {fade_out}");


        var m_msg_label = Instantiate(m_text_pref, m_parent);
        m_msg_label.transform.SetAsFirstSibling();

        var m_msg_color = m_msg_label.color;

        m_msg_label.text = msg;

        if (fade_in > 0)
        {
            await ProgressTimer(fade_in, (prog, delta) =>
            {
                m_msg_color.a = prog;
                m_msg_label.color = m_msg_color;
            });
        }
        else
        {
            m_msg_color.a = 1;
            m_msg_label.color = m_msg_color;
        }


        if (duration != 0)
        {
            if (m_user_scale_animation)
            {
                await ProgressTimer(duration, (prog, delta) => m_msg_label.transform.localScale = Vector3.one * m_scale_animation.Evaluate(prog));
            }
            else
            {
                await Delay(duration);
            }

            if (fade_out > 0)
            {
                await ProgressTimer(fade_out, (prog, delta) =>
                {
                    m_msg_color.a = 1f - prog;
                    m_msg_label.color = m_msg_color;
                });

            }
            m_msg_color.a = 0;
            m_msg_label.color = m_msg_color;
        }
        Destroy(m_msg_label.gameObject);
    } 
#elif UNITY_WEBGL
    public void ShowMessage(string msg, bool priority = false, float duration = 0f, float fade_in = 0f, float fade_out = 0f)
    {
        StartCoroutine(ShowMessageRoutine(msg, duration, fade_in, fade_out));
    }

    IEnumerator ShowMessageRoutine(string msg, float duration, float fade_in, float fade_out)
    {
        //Debug.Log($"<b>UI.ShowMessage</b> {msg}, duration: {duration}, fade_in: {fade_in}, fade_out: {fade_out}");


        var m_msg_label = Instantiate(m_text_pref, m_parent);
        m_msg_label.transform.SetAsFirstSibling();

        var m_msg_color = m_msg_label.color;

        m_msg_label.text = msg;

        if (fade_in > 0)
        {
            yield return this.ProgressTimerV(fade_in, (prog, delta) =>
            {
                m_msg_color.a = prog;
                m_msg_label.color = m_msg_color;
            });
        }
        else
        {
            m_msg_color.a = 1;
            m_msg_label.color = m_msg_color;
        }


        if (duration != 0)
        {
            if (m_user_scale_animation)
            {
                yield return this.ProgressTimerV(duration, (prog, delta) => m_msg_label.transform.localScale = Vector3.one * m_scale_animation.Evaluate(prog));
            }
            else
            {
                yield return new WaitForSeconds(duration);
            }

            if (fade_out > 0)
            {
                yield return this.ProgressTimerV(fade_out, (prog, delta) =>
                {
                    m_msg_color.a = 1f - prog;
                    m_msg_label.color = m_msg_color;
                });

            }
            m_msg_color.a = 0;
            m_msg_label.color = m_msg_color;
        }
        Destroy(m_msg_label.gameObject);
    }
#endif
}
