﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using System.Collections.Generic;
using System;
using System.Linq;

using UnityUseful.CorotineInstructionsWrappers;
using static UnityUseful.IEnumeratorExtension.IEnumeratorExtension;

public class TurnUI : MonoBehaviour
{
    const string m_pref_path = "Prefs/Minigames/UI/";
    const string m_tgl_pref_path = m_pref_path + "ToggleAsPoint_";
    const string m_cur_user_graphic_path = m_pref_path + "CurrentUserGraphic_";

    const float m_footer_fade_out_time = 0.5f;

    [Header("UI:")]
    [SerializeField] Button m_fire_b;
    [Space]
    [SerializeField] TextMeshProUGUI m_cur_user_score;
    [SerializeField] TextMeshProUGUI m_cur_user_name;
    [Space]
    [SerializeField] Image m_turn_notif;

    [Header("Parents:")]
    [SerializeField] Transform m_tgl_attempt_parent;
    [SerializeField] Transform m_footer_score_items_parent;

    [Header("Avatars:")]
    [SerializeField] List<Sprite> m_avatars;

    [Header("Time panel:")]
    [SerializeField] List<Sprite> m_digits;
    [SerializeField] List<Image> m_img_refs;

    [Header("Prefabs:")]
    [SerializeField] GameObject m_footer_score_item_pref;
    [Space]
    [SerializeField, Tooltip("From Resources.Dont need reference.")] GameObject m_tgl_attempt_pref;
    [SerializeField, Tooltip("From Resources.Dont need reference.")] GameObject m_cur_user_pref;

    [Header("Settings:")]
    [SerializeField] int default_avatar_index = 4;
    [Space]
    [SerializeField] bool m_const_attempts_text;
    [Header("Dynamic:")]
    [SerializeField] List<GameObject> m_footers=new List<GameObject>();

    public Button Fire_b
    {
        get
        {
            return m_fire_b;
        }
    }

    public List<Sprite> Avatars { get => m_avatars; }
    public List<GameObject> Footers { get => m_footers; }

    WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();
    Coroutine m_notification;

    public void AddFooterItem(int view_id, string nick_name, string score, Color bg_color, int user_icon_index = -1, Sprite icon = null, int sibling_index=0)
    {
        //Debug.Log($"<b>TurnUI.AddFooterItem</b> name: {view_id}", m_footer_score_items_parent);
        //var new_footer_item = Instantiate(m_footer_score_item_pref, m_footer_score_items_parent);
        
        //new_footer_item.SetActive(true);
        //new_footer_item.name = view_id.ToString();

        //var tgl_comp = new_footer_item.GetComponent<Toggle>();
        //var user_item_comp = tgl_comp.GetComponent<UserItem>();

        //user_item_comp.Label.text = nick_name.ToShortForm();
        //user_item_comp.ScoreText.text = score;
        //user_item_comp.UserAvatar.Icon = user_icon_index == -1 ? (icon ?? m_avatars[default_avatar_index]) : m_avatars[user_icon_index];
        //user_item_comp.GetComponentInChildren<Image>().color = bg_color;
        //user_item_comp.Label.color = Color.black;

        //tgl_comp.onValueChanged.AddListener((isOn) =>
        //{
        //    user_item_comp.Label.color = user_item_comp.ScoreText.color = isOn ? Color.white : Color.black;
        //}); // Inverse Color 
        //tgl_comp.isOn = false;

        //new_footer_item.transform.SetSiblingIndex(sibling_index);
        //m_footers.Add(new_footer_item);
    }
    public void ShowFooter(int view_id, float fade_out = m_footer_fade_out_time)
    {
        Debug.Log($"TurnUI.ShowFooter {view_id}");
        var target = m_footers.Find(x => x.name == view_id.ToString());
        if (target!=null)
        {
            Debug.Log($"TurnUI.ShowFooter target found with view id {view_id}",target);
            var c_group = target.GetComponent<CanvasGroup>();
            this.ProgressTimerV(fade_out, (prog, delta) =>c_group.alpha=prog);
        }
        else
        {
            Debug.Log($"<b>TurnUI.ShowFooter</b> <color=red>target not found</color>!", m_footer_score_items_parent);
        }
    }
    public async void UpdateFooterItem(int footer_item_id, string score, string new_title = "", Color? update_color = null, Color? text_color = null, Sprite icon = null)
    {
        //await waitForEndOfFrame;
        //var item = m_footer_score_items_parent.Find(footer_item_id.ToString());
        //if (item)
        //{
        //    var user_item_comp = item.GetComponent<UserItem>();
        //    user_item_comp.ScoreText.text = score;

        //    if (update_color != null)
        //    {
        //        user_item_comp.Score_bg.color = (Color)update_color; //ScoreBg - reference to Bg Image
        //    }
        //    if (text_color != null)
        //    {

        //        user_item_comp.Label.color = user_item_comp.ScoreText.color = (Color)text_color;
        //    }
        //    if (icon)
        //    {
        //        user_item_comp.UserAvatar.Icon = icon;
        //    }
        //    if (!string.IsNullOrEmpty(new_title))
        //    {
        //        user_item_comp.Label.text = new_title.ToShortForm();
        //    }
        //}
        //else
        //{
        //    Debug.Log($"UpdateFooterItem item <color=red>NOT FOUND</color>({footer_item_id})");
        //}
    }
    public void RenameFooterItem(int view_id, string label)
    {
        var result = m_footers.Find(x=>x.name == view_id.ToString());
        if (result)
        {
            //result.GetComponent<UserItem>().Label.text = label;
        }
        else
        {
            Debug.Log($"<b>TurnUI.RenameFooterItem</b> {view_id} <color=red>not found in footers</color>",m_footer_score_items_parent);
        }
    }
    public void RemoveFooterItem(string footer_name)
    {
        Debug.Log($"Remove from ui: {footer_name}");
        if (m_footer_score_items_parent.Find(footer_name))
        {
            Destroy(m_footer_score_items_parent.Find(footer_name).gameObject);
        }

    }

    public async void SetCurrentUser(string nick_name, string score = null, int user_icon = -1, Sprite sprite = null)
    {
        //await waitForEndOfFrame;
        //m_cur_user_name.text = nick_name;
        //if (!string.IsNullOrEmpty(score) && !m_const_attempts_text)
        //{
        //    m_cur_user_score.text = score;
        //}
        //m_user_avatar.Icon = user_icon == -1 ? (sprite ?? m_avatars[default_avatar_index]) : m_avatars[user_icon];
    }
    public async void SetCurrentUserFromFooter(int footer_item_id, string full_nickname)
    {
        //await waitForEndOfFrame;
        //var target_footer = m_footer_score_items_parent.Find(footer_item_id.ToString());
        //if (target_footer)
        //{
        //    var footer_user_item = target_footer.GetComponent<UserItem>();
        //    m_cur_user_name.text = full_nickname;
        //    if (!m_const_attempts_text)
        //    {
        //        m_cur_user_score.text = footer_user_item.ScoreText.text;
        //    }
        //    m_user_avatar.Icon = footer_user_item.UserAvatar.Icon;
        //}
    }
    public void UpdateCurrentUser(string nick_name = null, string score = null)
    {
        if (nick_name != null)
        {
            m_cur_user_name.text = nick_name;
        }
        if (score != null && !m_const_attempts_text)
        {
            m_cur_user_score.text = score;
        }
    }

    public void InitAttemptToggles(int count)
    {
        Debug.Log($"<b>TurnUI.InitAttemptToggles</b>: {count}");
        for (int i = 0; i < count; i++)
        {
            var new_obj = Instantiate(m_tgl_attempt_pref, m_tgl_attempt_parent);
            new_obj.SetActive(true);
        } // Attempts Toggle Instantiate
    }
    public void UpdateAttemptToggle(Func<Toggle, bool> predicate, bool state)
    {
        foreach (Toggle item in m_tgl_attempt_parent.GetComponentsInChildren<Toggle>())
        {
            if (predicate(item))
            {
                item.isOn = state;
            }
        }

    }

    public void InitWithSceneID(int game_id)
    {
        m_tgl_attempt_pref = Resources.Load(m_tgl_pref_path + game_id) as GameObject;
        m_cur_user_pref = Resources.Load(m_cur_user_graphic_path + game_id) as GameObject;
        if (m_cur_user_pref != null)
        {
            var new_obj = Instantiate(m_cur_user_pref, transform);
            new_obj.transform.SetAsFirstSibling();
        }
        else
        {
            Debug.LogError($"Prefab [{game_id}] not found!");
        }
    }
    public void FireTurnNotification(string content, float stand_time = 1.5f, float fade_time = .5f)
    {
        //Debug.Log("FireTurnNotification");

        if (m_notification!=null)
        {
            StopCoroutine(m_notification);
        }

        var text = m_turn_notif.GetComponentInChildren<TextMeshProUGUI>();

        text.text = content;

        var cur_text_color = text.color;
        cur_text_color.a = 1f;
        text.color = cur_text_color;

        var cur_panel_color = m_turn_notif.color;
        cur_panel_color.a = 1f;
        m_turn_notif.color = cur_panel_color;

        m_turn_notif.gameObject.SetActive(true);

        m_notification = this.ChainV(
            Timer(stand_time, () => { }),
            ProgressTimer(fade_time, (prog, delta) =>
                {
                    var inv_prog = 1 - prog;

                    cur_text_color.a = inv_prog;
                    cur_panel_color.a = inv_prog;

                    m_turn_notif.color = cur_panel_color;
                    text.color = cur_text_color;
                },
                finals: () => m_turn_notif.gameObject.SetActive(false))
                );
    }
    public void SetTime(int seconds)
    {
        var span = new TimeSpan(0, 0, seconds);
        var secondsList = span.Seconds.ToString().Select(digit => int.Parse(digit.ToString()));
        var minutesList = span.Minutes.ToString().Select(digit => int.Parse(digit.ToString()));

        //Debug.Log($"Time: {secondsList.Count()} Normal: {span.Seconds}");

        m_img_refs[0].sprite = m_digits[minutesList.Count() > 1 ? minutesList.ElementAt(secondsList.Count() - 2) : 0]; //M
        m_img_refs[1].sprite = m_digits[minutesList.ElementAt(minutesList.Count() - 1)]; //M
        m_img_refs[2].sprite = m_digits[secondsList.Count() > 1 ? secondsList.ElementAt(secondsList.Count() - 2) : 0]; //S
        m_img_refs[3].sprite = m_digits[secondsList.ElementAt(secondsList.Count() - 1)]; //S
    }


    void Awake()
    {
        InitWithSceneID(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
