﻿using UnityEngine;
using UnityEditor;

using System.Collections;

using UnityUseful.Misc;
using System.Linq;
using System.Collections.Generic;
using System.IO;

[CreateAssetMenu]
public class SkinAsset : ScriptableObject
{
    [Header("Main:")]
    [SerializeField] Skin m_skin;
    [Space]
    [SerializeField] Color m_color;

    [Header("Materials:")]
    [SerializeField] Material m_shield_skin_material;
    [SerializeField] Material m_char_skin_material;
    [SerializeField] Material m_hammer_skin_material;

    [Header("Particle Systems:")]
    [SerializeField, Reorderable] ParticleSystem[] m_hammer_particles;
    [Space]
    [SerializeField, Reorderable] ParticleSystem[] m_shield_collide_particles;
    [Space]
    [SerializeField, Reorderable] ParticleSystem[] m_character_collide_particles;

    #region Properties
    public Skin Skin { get => m_skin; }
    public Color Color { get => m_color; }

    public Material HammerSkinMaterial { get => m_hammer_skin_material; }
    public Material CharSkinMaterial { get => m_char_skin_material; }
    public Material ShieldSkinMaterial { get => m_shield_skin_material; }

    public ParticleSystem[] ShieldCollideParticles { get => m_shield_collide_particles; }
    public ParticleSystem[] HammerParticles { get => m_hammer_particles; }
    public ParticleSystem[] CharacterCollideParticles { get => m_character_collide_particles; }
    #endregion


#if UNITY_EDITOR
    [Header("[Edtior.Recreate Paths:]")]
    [SerializeField] string m_shield_skin_material_path = "Models/Shield/Materials";
    [SerializeField] string m_char_skin_material_path = "Models/Character/Materials";
    [SerializeField] string m_hammer_skin_material_path = "Models/Hammer/Materials";
    [Space]
    [SerializeField, Reorderable] string[] m_hammer_ps_prefs_path = new[] { "Resources/Prefs/FX/HammerNova" };
    [SerializeField, Reorderable] string[] m_shield_ps_prefs_path = new[] { "Resources/Prefs/FX/ShieldExplosion" };
    [SerializeField, Reorderable] string[] m_char_ps_prefs_path = new[] { "Resources/Prefs/FX/Character" };

    [ContextMenu("[Edtior." + nameof(Recreate) + "from Paths:]")]

    void Recreate()
    {
        Debug.Log($"<b>SkinAsset.Recreate</b> ", this);
        RenameAsset();

        var text_check = m_skin.ToString();

        m_shield_skin_material = LoadFromAssetDB<Material>(m_shield_skin_material_path, text_check)[0];
        m_char_skin_material = LoadFromAssetDB<Material>(m_char_skin_material_path, text_check)[0];
        m_hammer_skin_material = LoadFromAssetDB<Material>(m_hammer_skin_material_path, text_check)[0];

        m_hammer_particles = LoadEffects(m_hammer_ps_prefs_path, text_check);

        m_shield_collide_particles = LoadEffects(m_shield_ps_prefs_path, text_check);

        m_character_collide_particles = LoadEffects(m_char_ps_prefs_path, text_check);
    }
    ParticleSystem[] LoadEffects(string[] load_paths, string text_check = "")
    {
        var temp_list = new List<ParticleSystem>();

        foreach (var item_path in load_paths)
        {

            var char_ps = LoadFromAssetDB<ParticleSystem>(item_path, text_check);
            temp_list.AddRange(char_ps);
        }

        return temp_list.ToArray();
    }
    void RenameAsset()
    {
        var new_name = m_skin.ToString();
        var assetPath = AssetDatabase.GetAssetPath(GetInstanceID());

        AssetDatabase.RenameAsset(assetPath, new_name);
        AssetDatabase.SaveAssets();
    }

    T[] LoadFromAssetDB<T>(string path, string text_check = "") where T : Object
    {
        var full_path = Path.Combine(Application.dataPath, path);
        var files = Directory.GetFiles(full_path);
        var result_paths = string.IsNullOrEmpty(text_check) ? files :  files.Where(x => x.Contains(text_check));
        var assets = new List<T>();

        foreach (var item in result_paths)
        {
            var file = Path.GetFileName(item);
            var asset_path = Path.Combine("Assets/", path, file);
            var asset = AssetDatabase.LoadAssetAtPath<T>(asset_path) as T;

            Debug.Log($"<b>SkinAsset.LoadFromAssetDB</b> files[{files.Length}]" +
                $"\nresult: {result_paths}" +
                $"\nassetPath: {asset_path}");
            assets.Add(asset);
        }
        return assets.ToArray();

    }
#endif
}
