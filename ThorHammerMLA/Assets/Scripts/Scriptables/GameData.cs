﻿using UnityEngine;

using System;

using Newtonsoft.Json;

[CreateAssetMenu]
public class GameData : ScriptableObject
{
    static GameData m_instance;

    [Header("Settings:")]
    [SerializeField] SavedData m_saved_data;


    [Header("Gameplay Settings:")]
    [SerializeField, Range(60, 900)] float m_game_time = 90;
    [SerializeField, Range(0, 3)] float m_pregame_time = 3;
    [Space]
    [SerializeField, Range(1, 12)] int m_spawn_count = 4;

    [Header("Character Setting:")]
    [SerializeField, Range(1, 5)] int m_health_max = 5;
    [SerializeField, Range(1, 10), Tooltip("Time to charge hammer, increases only when moving")] float m_max_power_time = 2f;
    [SerializeField, Range(1, 50)] float m_character_move_speed = 6f;
    [SerializeField, /*Range(0,10)*/] float m_character_rot_speed = 10f;
    [SerializeField, Range(1, 5)] float m_character_attack_cooldown = 1f;

    #region Properties
    public static GameData Instance
    {
        get => m_instance ?? (m_instance = Resources.Load<GameData>($"ScriptableObjects/{nameof(GameData)}"));
    }
    public string Nickname
    {
        get
        {
            return m_saved_data.Nickname;
        }
        set
        {
            m_saved_data.Nickname = value;
        }
    }
    public float GameTime { get => m_game_time; }
    public int SpawnCount { get => m_spawn_count; }
    public int GlobalScore { get => m_saved_data.Score; set => m_saved_data.Score = value; }
    public float PregameTime { get => m_pregame_time; }
    public float Volume { get => m_saved_data.Volume; }
    public int Health { get => m_health_max; }
    public float MaxPowerTime { get => m_max_power_time; }
    public float CharacterRotSpeed { get => m_character_rot_speed; }
    public float CharacterMoveSpeed { get => m_character_move_speed; }
    public float CharacterAttackCooldown { get => m_character_attack_cooldown; }
    public bool EnableSound
    {
        get
        {
            return m_saved_data.Enable_sound;
        }
        set
        {
            m_saved_data.Enable_sound = value;
        }
    }
    public bool EnableVibro
    {
        get
        {
            return m_saved_data.Enable_vibro;
        }
        set
        {
            m_saved_data.Enable_vibro = value;
        }
    }
    #endregion

    [Serializable]
    public class SavedData
    {
        [Header("Strings:")]
        [SerializeField] string m_nickname = "Player";

        [Header("Ints:")]
        [SerializeField] int m_score;

        [Header("Floats:")]
        [SerializeField, Range(0, 1)] float m_volume = 1;

        [Header("Bools:")]
        [SerializeField] bool m_enable_sound;
        [SerializeField] bool m_enable_vibro;

        public string Nickname
        {
            get => m_nickname;
            set
            {
                m_nickname = value;
                Save();
            }
        }
        public float Volume
        {
            get => m_volume; 
            set
            {
                m_volume = value;
                Save();
            }
        }
        public bool Enable_sound
        {
            get => m_enable_sound;
            set
            {
                m_enable_sound = value;
                Save();
            }


        }
        public bool Enable_vibro
        {
            get => m_enable_vibro; 
            set
            {
                m_enable_vibro = value;
                Save();
            }
        }
        public int Score
        {
            get => m_score;
            set
            {
                m_score = value;
                Save();
            }
        }

        public void Save()
        {
            //Debug.Log($"<b>GameData.Save</b>");
            var json = JsonConvert.SerializeObject(this);
            PlayerPrefs.SetString(nameof(SavedData), json);
        }
        public SavedData Load()
        {
            //Debug.Log($"<b>GameData.Load</b>");
            if (PlayerPrefs.HasKey(nameof(SavedData)))
            {
                var json = PlayerPrefs.GetString(nameof(SavedData));
                return JsonConvert.DeserializeObject<SavedData>(json);
            }
            else
            {
                return new SavedData();
            }
        }
        public void Update()
        {
            //Debug.Log($"<b>GameData.Update</b>");
            Save();
            Load();
        }
    }

    void OnValidate()
    {
        m_saved_data.Update();
    }

#if UNITY_EDITOR
    [ContextMenu(nameof(TestModeSingle))]
    void TestModeSingle()
    {
        m_spawn_count = 1;
    }
    [ContextMenu(nameof(TestModeEndless))]
    void TestModeEndless()
    {
        m_game_time = 600;
    }
    [ContextMenu(nameof(RestoreTime))]
    void RestoreTime()
    {
        m_game_time = 90;
    }
    [ContextMenu(nameof(RestorePlayers))]
    void RestorePlayers()
    {
        m_spawn_count = 4;
    }
    [ContextMenu(nameof(AddScore))]
    void AddScore()
    {
        GlobalScore += 5;
    }
#endif
}
