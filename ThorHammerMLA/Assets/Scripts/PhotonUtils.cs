﻿#if PUN
using UnityEngine;

using ExitGames.Client.Photon;

namespace UnityUseful.PhotonUtils
{
    public static class PhotonUtils
    {
        public static Hashtable AddOrCreateKey(this Hashtable table, object key, object value)
        {
            if (table.ContainsKey(key))
            {
                table[key] = value;
            }
            else
            {
                table.Add(key, value);
            }
            return table;
        }
        public static void DebugHashtable(this Hashtable table)
        {
            var str = "";
            foreach (var item in table)
            {
                str += $"[{item.Key}] = [{item.Value}]\n";
            }
            Debug.LogError($"<color=red>Room properites changed:</color> {str}");
        }
    }
}

#endif