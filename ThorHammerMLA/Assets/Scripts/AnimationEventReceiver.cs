﻿using UnityEngine;
using System.Collections;

public class AnimationEventReceiver : MonoBehaviour
{
    [SerializeField] Character m_character;

    [SerializeField] bool m_enable_debug;

    void Awake()
    {
        m_character = GetComponentInParent<Character>();
    }
    public void OnThrowComplete(AnimationEvent a_event)
    {
        //Debug.Log($"<b>AnimationEventReceiver.OnThrowComplete</b> {m_character.name}", m_character);
        //m_character.Agent.isStopped = false;
        //m_character.Agent.ResetPath();
    }
    public void OnThrowStart(AnimationEvent a_event)
    {
        //Debug.Log($"<b>AnimationEventReceiver.OnThrowStart</b> {m_character.name}", m_character);
        //m_character.Agent.isStopped = false;
        //m_character.Agent.ResetPath();
    }

    public void OnThrow(AnimationEvent a_event)
    {
        //m_character.CompleteThrow();
    }
    public void OnStandUp(AnimationEvent a_event)
    {
        //Debug.Log($"<b>AnimationEventReceiver.OnStanUp</b> {m_character.name}", m_character);
        //m_character.OnHammerHitRestore();
    }
    public void OnStun(AnimationEvent a_event)
    {
        //Debug.Log($"<b>AnimationEventReceiver.OnStun</b> {m_character.name}",m_character);
    }
}
