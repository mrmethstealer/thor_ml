﻿using UnityEngine;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityUseful.Misc;

#if UNITY_WEBGL
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions;
#endif

public class Hammer : MonoBehaviour
{
    [Header("Static Reffs:")]
    [SerializeField, Reorderable] List<Collider> m_colliders;
    [SerializeField] Rigidbody m_rb;
    [SerializeField] TrailRenderer m_trail;
    [SerializeField] Transform m_center_point;

    [Header("Static Data:")]
    [SerializeField] float m_return_speed = 2f;

    [Header("Dynamic:")]
    [SerializeField] Character m_owner;
    [Space]
    [Space]
    [SerializeField] bool m_has_hammer_fly;
    [SerializeField] bool m_has_hammer_returning;
    [SerializeField] bool m_block_second_collision_msg;
    [Space]
    [SerializeField] Vector3 m_next_pos;
    [Space]
    [SerializeField] Vector3 m_hammer_rot_vector;
    [Space]
    [SerializeField] float m_distance;
    [SerializeField] float m_rot_diff;
    [SerializeField] float elapsed;
    [SerializeField, Range(0, 1)] float m_strength_progress;
    [Space]
    [SerializeField] AnimationCurve m_curve_X;
    [SerializeField] AnimationCurve m_curve_Y;
    [SerializeField] AnimationCurve m_curve_Z;

    #region Properties
    public Rigidbody Rigidbody { get => m_rb; }
    public Character Owner { get => m_owner; }
    public bool IsFlying { get => m_has_hammer_fly; }
    public bool IsReturning { get => m_has_hammer_returning; }
    public bool CollidersEnabled { set => m_colliders.ForEach(x => x.enabled = value); }
    public Vector3 ColliderSize
    {
        get => ((BoxCollider)m_colliders[0]).size;
        set
        {
            ((BoxCollider)m_colliders[0]).size = value;
        }
    }

    public float StrengthProgress
    {
        set
        {
            m_strength_progress = value;

            //var burst = m_particles.emission.GetBurst(0);
            //burst.count = new ParticleSystem.MinMaxCurve(Mathf.Lerp(5, 25, m_strength_progress));
            //m_particles.emission.SetBurst(0, burst);
            //m_particles.transform.localScale = Vector3.one * Mathf.Lerp(.2f, .4f, m_strength_progress);
        }
    }

    public Transform CenterPoint { get => m_center_point; }
    public List<Collider> Colliders { get => m_colliders; }
    #endregion

    void OnValidate()
    {
        if (Application.isPlaying)
        {
            StrengthProgress = m_strength_progress;
        }
    }
    void Awake()
    {
        m_rb = GetComponent<Rigidbody>();

        m_rb.isKinematic = true;
    }
    public void Init(Character owner)
    {
        m_owner = owner;
        name = owner.Nickname + " Hammer";
    }
    void FixedUpdate()
    {
        if (m_next_pos.sqrMagnitude > 0.01f)
        {
            m_rb.MovePosition(m_next_pos);
        }

        if (m_hammer_rot_vector.sqrMagnitude > 0.01f)
        {
            m_rb.MoveRotation(Quaternion.Euler(m_hammer_rot_vector));
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (m_owner)
        {
            var collider_go = collision.gameObject;

            string to = null;
            var reward = 0;
            var succ_hit = false;
            var is_body = false;

            if (collider_go.CompareTag("Hammer"))
            {
                var other_hammer = collider_go.GetComponent<Hammer>();
                if (!other_hammer.Owner.ThrowModule.HasHammer) // Ignore collision if hit enemy, that holds hammer
                {
                    reward = GameManager.m_hammer_hammer_reward;
                    m_owner.Points += reward;

                    to = $"<color={other_hammer.Owner.SkinModule.Color.ToHTMLColor()}>{other_hammer.Owner.Nickname}</color>";
                    if (!m_block_second_collision_msg)
                    {
                        other_hammer.m_block_second_collision_msg = true;
                        succ_hit = true;
                    }
                    else
                    {
                        m_block_second_collision_msg = false;
                    }
                }
            }
            else
            if (collider_go.CompareTag("Player"))
            {
                //if (collider_go != gameObject)
                //{
                //    reward = collider_go.GetComponent<ReceiveDamageModule>().Damage;
                //    m_owner.Points += reward;
                //    m_owner.Audio.PlaySound(Sound.HitHammerSuccess, Source.Shield);
                //    var receiver = collider_go.GetComponent<Character>();
                //    receiver.OnHammerHit();
                //    to = $"<color={receiver.SkinModule.Color.ToHTMLColor()}>{receiver.Nickname}</color>";
                //    succ_hit = true;
                //    is_body = true;
                //}
            }
            else
            if (collider_go.CompareTag("Shield"))
            {
                if (collider_go != m_owner.gameObject) // Self check
                {
                    var receiver = collider_go.GetComponent<ReceiveDamageModule>();
                    if (receiver && receiver.HealthModule != m_owner.HealthModule && receiver.HealthModule.Health > GameManager.m_hammer_damage)
                    {
                        reward = receiver.ReceivedDamage;
                        m_owner.Points += reward;

                        var character = receiver.HealthModule.GetComponent<Character>();
                        to = $"<color={character.SkinModule.Color.ToHTMLColor()}>{character.Nickname}</color>";
                        succ_hit = true;
                    }
                }
            }

            if (succ_hit)
            {
                var from = $"<color={m_owner.SkinModule.Color.ToHTMLColor()}>{m_owner.Nickname}</color>";
                var receiver_icon = is_body ? "" : $"<sprite name=X> <sprite name={collider_go.tag}>";
                var msg = $"{from} <sprite name={tag}> {receiver_icon} {to}"; // + reward?

                UI.Instance?.BattleLoggerUI.ShowMessage(msg, fade_in: .25f, duration: 1f, fade_out: .25f);

                if (m_owner.IsLocal)
                {
                    UnityUseful.Vibrate.VibrateControll.Vibrate(0.2f, 250);
                    if (m_owner.ThrowModule.PowerProgress > .5f)
                    {
                        CameraController.Instance?.Shake();
                    }
                }
            }
        }
    }
    
#if !UNITY_WEBGL
    public async void HammerGetBack(Transform hand_tranform, float main_translation_time = 1f, float last_translation_time = .25f)
    {
        if (!m_has_hammer_returning)
        {
            //Debug.Log($"<b>Character.HammerGetBack</b> positions: {m_trail.positionCount}");

            m_distance = float.MaxValue;
            m_rot_diff = float.MaxValue;

            m_has_hammer_returning = true;
            m_colliders.ForEach(x => x.enabled = false);

            var lerp_t = Time.deltaTime * m_return_speed;

            m_rb.isKinematic = true;

            await TrajectoryReturn(main_translation_time);


            await ProgressTimer(last_translation_time, (progress, delta) =>
            {
                m_next_pos = Vector3.Lerp(transform.position, hand_tranform.position, progress);
                m_hammer_rot_vector = Quaternion.Lerp(Quaternion.Euler(transform.eulerAngles), Quaternion.Euler(hand_tranform.eulerAngles), elapsed).eulerAngles;
            });

            elapsed = 0;
            m_hammer_rot_vector = Vector3.zero;
            m_next_pos = Vector3.zero;
            transform.position = hand_tranform.position;
            transform.eulerAngles = hand_tranform.eulerAngles;

            transform.SetParent(hand_tranform);

            m_has_hammer_returning = false;
            m_owner.ThrowModule.HasHammer = true;
        }
    }
    
    async Task TrajectoryReturn(float translation_time)
    {
        var positions = new Vector3[m_trail.positionCount];
        m_trail.GetPositions(positions);


        var distances = new List<float>();

        for (int i = 0; i < positions.Length - 1; i++)
        {
            var distance = Vector3.Distance(positions[i], positions[i + 1]);
            //Debug.Log($"<b>Hammer.TrajectoryReturn</b>  [{i}]->[{i+1}] distance: {distance}");
            distances.Add(distance);
        }
        var max = distances.Sum();

        //Debug.Log($"<b>Hammer.TrajectoryReturn</b> positions: {m_trail.positionCount}, max: {max}, links[{distances.Count}]");

        var time = 0f;
        m_curve_X = new AnimationCurve();
        m_curve_Y = new AnimationCurve();
        m_curve_Z = new AnimationCurve();

        for (int j = positions.Length - 1; j > 1; j--)
        {
            var current_pos = transform.position;
            var target_pos = positions[j];

            var time_contribution = distances[j - 1] / max;

            m_curve_X.AddKey(time, positions[j].x);
            m_curve_Y.AddKey(time, positions[j].y);
            m_curve_Z.AddKey(time, positions[j].z);
            time += time_contribution * translation_time;
        }

        await ProgressTimer(translation_time, (progress, delta) =>
         {
             m_progress = progress;
             var next_pos = new Vector3(m_curve_X.Evaluate(progress), m_curve_Y.Evaluate(progress), m_curve_Z.Evaluate(progress));
             m_next_pos = Vector3.Lerp(transform.position, next_pos, progress);
         });
    }
#elif UNITY_WEBGL
    public void HammerGetBack(Transform hand_tranform, float main_translation_time = 1f, float last_translation_time = .25f)
    {
        StartCoroutine(HammerGetBackRoutine(hand_tranform, main_translation_time, last_translation_time));
    }

    IEnumerator HammerGetBackRoutine(Transform hand_tranform, float main_translation_time, float last_translation_time)
    {
        if (!m_has_hammer_returning)
        {
            //Debug.Log($"<b>Character.HammerGetBack</b> positions: {m_trail.positionCount}");

            m_distance = float.MaxValue;
            m_rot_diff = float.MaxValue;

            m_has_hammer_returning = true;
            m_colliders.ForEach(x => x.enabled = false);

            var lerp_t = Time.deltaTime * m_return_speed;

            m_rb.isKinematic = true;

            yield return TrajectoryReturn(main_translation_time);


            yield return this.ProgressTimerV(last_translation_time, (progress, delta) =>
            {
                m_next_pos = Vector3.Lerp(transform.position, hand_tranform.position, progress);
                m_hammer_rot_vector = Quaternion.Lerp(Quaternion.Euler(transform.eulerAngles), Quaternion.Euler(hand_tranform.eulerAngles), elapsed).eulerAngles;
            });

            elapsed = 0;
            m_hammer_rot_vector = Vector3.zero;
            m_next_pos = Vector3.zero;
            transform.position = hand_tranform.position;
            transform.eulerAngles = hand_tranform.eulerAngles;

            transform.SetParent(hand_tranform);

            m_has_hammer_returning = false;
            m_owner.ThrowModule.HasHammer = true;
        }
    }
    IEnumerator TrajectoryReturn(float translation_time)
    {
        var positions = new Vector3[m_trail.positionCount];
        m_trail.GetPositions(positions);


        var distances = new List<float>();

        for (int i = 0; i < positions.Length - 1; i++)
        {
            var distance = Vector3.Distance(positions[i], positions[i + 1]);
            //Debug.Log($"<b>Hammer.TrajectoryReturn</b>  [{i}]->[{i+1}] distance: {distance}");
            distances.Add(distance);
        }
        var max = distances.Sum();

        //Debug.Log($"<b>Hammer.TrajectoryReturn</b> positions: {m_trail.positionCount}, max: {max}, links[{distances.Count}]");

        var time = 0f;
        m_curve_X = new AnimationCurve();
        m_curve_Y = new AnimationCurve();
        m_curve_Z = new AnimationCurve();

        for (int j = positions.Length - 1; j > 1; j--)
        {
            var current_pos = transform.position;
            var target_pos = positions[j];

            var time_contribution = distances[j - 1] / max;

            m_curve_X.AddKey(time, positions[j].x);
            m_curve_Y.AddKey(time, positions[j].y);
            m_curve_Z.AddKey(time, positions[j].z);
            time += time_contribution * translation_time;
        }

        yield return this.ProgressTimerV(translation_time, (progress, delta) =>
        {
            m_progress = progress;
            var next_pos = new Vector3(m_curve_X.Evaluate(progress), m_curve_Y.Evaluate(progress), m_curve_Z.Evaluate(progress));
            m_next_pos = Vector3.Lerp(transform.position, next_pos, progress);
        });
    } 
#endif

#if UNITY_EDITOR
    [ContextMenu("Throw")]
    public void HammerShoot1()
    {
        HammerShoot(300);
    }
#endif

#if !UNITY_WEBGL
    public async void HammerShoot(float force)
    {
        if (!m_has_hammer_returning)
        {
            //Debug.Log($"<b>Character.HammerShoot</b>");
            if (m_trail)
            {

                m_trail.Clear();
                m_trail.enabled = true;
            }
            if (m_rb) //async object check 
            {

                m_rb.isKinematic = false;
                m_has_hammer_fly = true;
                transform.SetParent(null);

                transform.forward = m_owner.transform.forward;
                m_rb.AddForce(transform.forward * force, ForceMode.Impulse);
                m_colliders.ForEach(x => x.enabled = true);
            }

            await Delay(1f);

            if (m_trail)
            {
                m_has_hammer_fly = false;
                m_trail.enabled = false;
            }
        }
    }
#elif UNITY_WEBGL
    public void HammerShoot(float force)
    {
        StartCoroutine(HammerShootRoutine(force));
    }
    IEnumerator HammerShootRoutine(float force)
    {
        if (!m_has_hammer_returning)
        {
            //Debug.Log($"<b>Character.HammerShoot</b>");
            if (m_trail)
            {

                m_trail.Clear();
                m_trail.enabled = true;
            }
            if (m_rb) //async object check 
            {

                m_rb.isKinematic = false;
                m_has_hammer_fly = true;
                transform.SetParent(null);

                transform.forward = m_owner.transform.forward;
                m_rb.AddForce(transform.forward * force, ForceMode.Impulse);
                m_colliders.ForEach(x => x.enabled = true);
            }

            yield return new WaitForSeconds(1f);

            if (m_trail)
            {
                m_has_hammer_fly = false;
                m_trail.enabled = false;
            }
        }
    } 
#endif

    [SerializeField] float m_progress;
    [SerializeField] bool m_trail_gizmos = true;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        Gizmos.color = Color.cyan;

        if (m_trail_gizmos && m_trail && m_trail.positionCount > 1)
        {
            for (int i = 0; i < m_trail.positionCount - 2; i++)
            {
                var from = m_trail.GetPosition(i);
                var to = m_trail.GetPosition(i + 1);

                //Gizmos.DrawWireSphere(from,.2f);
                Gizmos.color = Color.grey;
                Gizmos.DrawLine(from, to);
            }
            Gizmos.color = Color.red;
            Gizmos.DrawLine(m_owner.ThrowModule.HammerPoint.position, m_trail.GetPosition(0));
        }
    }
}
