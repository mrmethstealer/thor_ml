﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityUseful.CorotineInstructionsWrappers;

#if !UNITY_WEBGL
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public class ControlActionsController : Singleton<ControlActionsController>
{
    [SerializeField] EventTrigger m_event_trigger;

    [Header("Buttons:")]
    [SerializeField] Button m_fire_b;
    [SerializeField] Button m_back_b;

    [Space]
    [SerializeField] Character m_local_player;
    [Space]
    [SerializeField] float m_rot_speed = 10f;

    #region Properties
    public Button BackB { get => m_back_b; set => m_back_b = value; }
    public Button FireB { get => m_fire_b; set => m_fire_b = value; }
    #endregion

    protected override void Awake()
    {
        base.Awake();
        FireB.onClick.AddListener(OnThrow);
        BackB.onClick.AddListener(OnCatch);
    }

    public void InitLocalPlayer(Character character)
    {
        var entry = new EventTrigger.Entry();
        entry.callback.AddListener(event_data => character.transform.Rotate(Vector3.up, ((PointerEventData)event_data).delta.x * Time.deltaTime * m_rot_speed));
        entry.eventID = EventTriggerType.Drag;
        m_event_trigger.triggers.Add(entry);

        m_local_player = character;
    }

    public async void OnThrow()
    {
        FireB.interactable = false;
        BackB.interactable = false;

        m_local_player.ThrowHammer();
#if !UNITY_WEBGL
        await WaitWhile(() => !m_local_player.ThrowModule.CanAttack); 
        BackB.interactable = true;
#elif UNITY_WEBGL
        this.WaitUntilV(() => m_local_player.ThrowModule.CanAttack, () =>
          {
              BackB.interactable = true;
          });
#endif
    }

    public async void OnCatch()
    {
        BackB.interactable = false;
        FireB.interactable = false;

        m_local_player.HammerGetBack();

#if !UNITY_WEBGL
        await WaitWhile(() => !m_local_player.ThrowModule.HasHammer);
        FireB.interactable = true;
#elif UNITY_WEBGL
        this.WaitUntilV(() => m_local_player.ThrowModule.HasHammer, () =>
        {
            FireB.interactable = true;
        });
#endif
    }
}