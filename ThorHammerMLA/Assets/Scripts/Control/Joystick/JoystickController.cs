﻿using UnityEngine;
using UnityEngine.EventSystems;

using System;

public class JoystickController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{
    [Header("Static:")]
    [SerializeField] RectTransform joystick;
    [SerializeField] GameManager m_gm;
    [Space]
    [SerializeField] Vector2 joystickPosConst;
    [Space]
    [SerializeField] float m_range = 250;

    [Header("Runtime:")]
    [SerializeField] Vector2 eventDeltaNormalized;
    [SerializeField] Vector2 eventDelta;
    [Space]
    [SerializeField] Character m_target;
    public Vector2 InputNormalized { get => eventDeltaNormalized; }
    public Vector2 Input { get => eventDelta;  }

    void Awake()
    {
        joystickPosConst = joystick.position;
        m_gm.OnLocalPlayerAssigned += player =>
        {
            m_target = player;
        };  
    }
    void Update()
    {
        if (m_target)
        {
            m_target.MoveModule.Direction = eventDeltaNormalized;
        }
    }
    #region IDrag

    public void OnDrag(PointerEventData eventData)
    {
        eventDelta = (eventData.position - joystickPosConst);
        eventDeltaNormalized = eventDelta.normalized;

        if (eventDelta.magnitude > m_range)
        {
            joystick.position = transform.position + (Vector3)eventDeltaNormalized * m_range;
        }
        else
        {
            joystick.position = eventData.position;
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        joystick.anchoredPosition = Vector3.zero;
        eventDeltaNormalized = Vector2.zero;
    }

    #endregion

    public void OnPointerDown(PointerEventData eventData)
    {
        joystick.position = eventData.position;
        eventDelta = (eventData.pressPosition - joystickPosConst);
        eventDeltaNormalized = eventDelta.normalized;
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        eventDeltaNormalized = Vector2.zero;
        eventDelta = Vector2.zero;
        joystick.anchoredPosition = Vector2.zero;
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.CircleHandleCap(0, transform.position, Quaternion.identity, m_range, EventType.Repaint);
    } 
#endif
}
