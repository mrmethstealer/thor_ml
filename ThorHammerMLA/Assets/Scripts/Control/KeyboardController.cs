﻿#if UNITY_STANDALONE
using UnityEngine;

using System.Collections;
using System.Threading.Tasks;

public class KeyboardController : MonoBehaviour
{
    [SerializeField] GameManager m_gm;
    [Space]
    [SerializeField] Character m_local_player;
    [Space]
    [SerializeField] float m_sqrt_magnitude_treshold = 0.01f;

    void Awake()
    {
        m_gm.OnLocalPlayerAssigned += player => Init(player);
    }
    void Init(Character character)
    {
        m_local_player = character;
    }

    void Update()
    {
        if (m_local_player && m_local_player.IsLocal && !m_local_player.IsInputBlocked)
        {
            if (m_local_player.ThrowModule.HasHammer)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    ThrowLocal();
                }
            }
            else
            {
                if (Input.GetButtonDown("Jump"))
                {
                    CatchLocal();
                }
            }

            //if (Input.GetKey(KeyCode.Q))
            //{
            //    m_local_player.transform.Rotate(Vector3.up, -10 * Time.deltaTime);
            //}
            //if (Input.GetKey(KeyCode.E))
            //{
            //    m_local_player.transform.Rotate(Vector3.up, 10 * Time.deltaTime);
            //}
            var horiz = Input.GetAxis("Horizontal");
            var vert = Input.GetAxis("Vertical");
            var axis_input = new Vector2(horiz, vert);

            if (axis_input.sqrMagnitude > m_sqrt_magnitude_treshold)
            {
                m_local_player.MoveModule.Direction = axis_input;
            }
            else
            {
                m_local_player.MoveModule.Direction = Vector2.zero;
            }

        }
    }

    public async void ThrowLocal()
    {
        m_local_player.ThrowHammer();
    }
    public async void CatchLocal()
    {
        m_local_player.HammerGetBack();
    }
}

#endif