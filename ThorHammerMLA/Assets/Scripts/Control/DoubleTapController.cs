﻿using UnityEngine;
using UnityEngine.EventSystems;

using System;
using System.Collections;

public class DoubleTapController : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] Camera m_cam;
    [SerializeField] Character m_local_player;
    [Space]
    [SerializeField] LayerMask m_mask;

    [Header("Double Tap Data")]
    [SerializeField] int TapCount;
    [SerializeField] float MaxDubbleTapTime=.5f;
    [SerializeField] float NewTime;
    [Space]
    [SerializeField] bool m_double_tap_initialized;
    [SerializeField] float m_double_tap_active;

    public event Action<Vector3> OnWorldPointClick;
    public event Action<Vector3> OnWorldPointDoubleClick;

    public void Init(Character character) 
    {
        m_local_player = character;
        var agent = character.GetComponent<UnityEngine.AI.NavMeshAgent>();

        OnWorldPointClick += pos =>
        {
            if (!m_local_player.IsInputBlocked)
            {

                agent.SetDestination(pos);
            }
            else
            {
                agent.ResetPath();
            }
        };
        OnWorldPointDoubleClick += target =>
        {

            if (m_local_player.ThrowModule.HasHammer)
            {
                m_local_player.ThrowHammer(target);
            }
            else if (!m_local_player.ThrowModule.Hammer.IsReturning)
            {
                m_local_player.HammerGetBack();
            }

        };
    }

    public void OnPointerClick(PointerEventData eventData)
    {
#if UNITY_EDITOR 
        //Debug.Log($"<b>ScreenController.OnPointerClick</b> press: {eventData.pressPosition}, pos: {eventData.position}, click_count: {eventData.clickCount}");
        //if (eventData.clickCount == 1)
        //{
        //    SingleTap(eventData.pressPosition);
        //}
        //else 
        if (eventData.clickCount == 2) // WARNING. Works only in standalone systems!!
        {
            DoubleTap(eventData.pressPosition);
        }
#endif
    }
    async void SingleTap(Vector2 screen_pos)
    {
        Debug.Log($"<b>ScreenController.SingleTap</b>");
        //m_double_tap_active = m_double_tap_max_delay;

        //await Task.Delay((int)(m_double_tap_max_delay * 1000));

        //if (m_double_tap_active <= 0)
        //{
        //    m_double_tap_initialized = false;
        //}

        //if (m_double_tap_initialized)
        //{
        //    m_double_tap_initialized = false;
        //}
        //else
        //{
        //    var ray = m_cam.ScreenPointToRay(screen_pos);
        //    if (Physics.Raycast(ray, out var hit_info))
        //    {
        //        m_from_point = m_to_point;
        //        m_to_point = hit_info.point;
        //        OnWorldPointClick?.Invoke(m_to_point);
        //    }
        //}
    }
    void DoubleTap(Vector2 screen_pos)
    {
        m_double_tap_initialized = true;

        var ray = m_cam.ScreenPointToRay(screen_pos);
        if (Physics.Raycast(ray, out var hit_info, 100, layerMask: m_mask))
        {
            OnWorldPointDoubleClick?.Invoke(hit_info.point);
        }
    }
    void Update()
    {
        if (m_double_tap_active > 0)
        {
            m_double_tap_active -= Time.deltaTime;
        }

        CheckDoubleTap();
    }
    void CheckDoubleTap()
    {
        if (Input.touchCount == 1)
        {
            var touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Ended)
            {
                TapCount += 1;
            }

            if (TapCount == 1)
            {
                NewTime = Time.time + MaxDubbleTapTime;
            }
            else if (TapCount == 2 && Time.time <= NewTime)
            {
                DoubleTap(touch.position);
                TapCount = 0;
            }

        }
        if (Time.time > NewTime)
        {
            TapCount = 0;
        }
    }
}
