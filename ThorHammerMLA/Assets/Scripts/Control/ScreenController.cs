﻿using UnityEngine;
using UnityEngine.EventSystems;

using System;

#if UNITY_WEBGL
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using System.Threading.Tasks;

using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public class ScreenController : Singleton<ScreenController>, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [Header("Static:")]
    [SerializeField] Camera m_cam;
    [Space]
    [SerializeField] float m_double_tap_max_delay = .25f;

    [Header("Runtime:")]
    [SerializeField] Vector3 m_to_point;
    [SerializeField] Vector3 m_from_point;
    [SerializeField] Vector2 m_direction;
    [Space]
    [SerializeField] Character m_local_player;

    [Header("Gizmos:")]
    [SerializeField] Vector3 m_ground_offset = Vector3.up * .5f;

    [Header("Joystick:")]
    [SerializeField] RectTransform m_outer_circle;
    [SerializeField] RectTransform m_inner_circle;
    [SerializeField] RectTransform m_canvas;
    [Space]
    [SerializeField] float m_range = 250;

    #region Public Events

    public event Action<PointerEventData> OnBeginDragEvent;
    public event Action<PointerEventData> OnDragEvent;
    public event Action<PointerEventData> OnEndDragEvent;

    #endregion

    public void Init(Character character)
    {
        m_local_player = character;
        m_local_player.MoveModule.ResetDirection = false;

        OnDragEvent += eventData =>
        {
            if (m_local_player && !m_local_player.IsInputBlocked)
            {
                //var dir = (eventData.position - new Vector2(Screen.width * .5f, Screen.height * .5f));
                var dir = eventData.position - eventData.pressPosition;
                m_direction = dir.normalized;
                //Debug.Log($"<b>GameManager.OnDragEvent</b> {m_direction}");
                var rot_angle = (m_direction.x > 0 ? 1 : -1);
                m_local_player.MoveModule.Direction = m_direction;
            }
        };
        OnEndDragEvent += async eventData =>
        {
            if (m_local_player && !m_local_player.IsInputBlocked)
            {
                m_local_player.MoveModule.Direction = Vector2.zero;

                m_local_player.ThrowHammer();

#if UNITY_WEBGL
                this.TimerV(m_local_player.ThrowModule.AttackDelay, () =>m_local_player.HammerGetBack());
#elif !UNITY_WEBGL
                await Delay(m_local_player.ThrowModule.AttackDelay); 

                m_local_player.HammerGetBack();
#endif
            }
        };
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log($"<b>ScreenController.OnBeginDrag</b>");
        var rect = m_canvas.rect;
        var vieport_pos = m_cam.ScreenToViewportPoint(eventData.position);
        m_outer_circle.anchoredPosition = (vieport_pos * rect.size) - new Vector2(rect.width / 2, rect.height / 2);

        m_outer_circle.gameObject.SetActive(true);
        m_inner_circle.gameObject.SetActive(true);

        OnBeginDragEvent?.Invoke(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        var rect = m_canvas.rect;
        var vieport_pos = m_cam.ScreenToViewportPoint(eventData.position);
        var pos = (vieport_pos * rect.size) - new Vector2(rect.width / 2, rect.height / 2);
        var dir = eventData.position - eventData.pressPosition;
        if (Vector3.Distance(pos, m_outer_circle.anchoredPosition) > m_range)
        {
            m_inner_circle.anchoredPosition = m_outer_circle.anchoredPosition + /*pos.normalized*/ dir.normalized* m_range;
        }
        else
        {
            m_inner_circle.anchoredPosition = pos;
        }



        //Debug.Log($"<b>ScreenController.OnDrag</b>");
        OnDragEvent?.Invoke(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log($"<b>ScreenController.OnEndDrag</b>");
        m_outer_circle.gameObject.SetActive(false);
        m_inner_circle.gameObject.SetActive(false);

        OnEndDragEvent?.Invoke(eventData);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(m_to_point, m_to_point + Vector3.up);
        Gizmos.DrawLine(m_from_point, m_from_point + Vector3.up);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(m_from_point + m_ground_offset, m_to_point + m_ground_offset);

        //if (Application.isPlaying && GameManager.Instance && GameManager.Instance.LocalPlayer)
        //{
        //    var local = GameManager.Instance.LocalPlayer;
        //    Gizmos.color = Color.magenta;
        //    Gizmos.DrawLine(local.transform.position + Vector3.up, local.transform.position + m_direction + Vector3.up);
        //}


#if UNITY_EDITOR
        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.CircleHandleCap(0, m_outer_circle.transform.position, Quaternion.identity, m_range, EventType.Repaint); 
#endif
    }
}
