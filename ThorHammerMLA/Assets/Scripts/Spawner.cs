﻿using UnityEngine;

using System;
using System.Collections.Generic;

//using Photon.Pun;
//using Photon.Realtime;
//using PN = Photon.Pun.PhotonNetwork;
//using UnityUseful.PhotonUtils;

using UnityUseful.Misc;
using Random = UnityEngine.Random;

//[RequireComponent(typeof(PhotonView))]
public class Spawner : MonoBehaviour//, IPunObservable
{
    [Header("Prefs:")]
    [SerializeField] Character m_character_pref;

    [Header("Characters:")]
    [SerializeField, Reorderable] List<Character> m_characters;

    [Header("Spawn Position:")]
    [SerializeField] bool m_use_spawn_points = true;
    [Space]
    [SerializeField] Transform m_spawn_center;
    [SerializeField] float m_distance;
    [Space]
    [SerializeField, Reorderable] List<Transform> m_spawn_points;

    [Header("Components")]
    [SerializeField] GameManager m_gm;

    [Header("Settings:")]
    [SerializeField] int m_spawn_count_max = 12;
    [SerializeField] int m_spawn_count = 4;
    [SerializeField] int m_spawn_offset;
    [SerializeField] int m_local_players_count = 1;
    [Space]
    [SerializeField] bool m_spawn_on_start = true;
    [SerializeField] bool m_spawn_on_random_point = true;

    public float Distance { get => m_distance; set => m_distance = value; }
    public int LocalPlayersToSpawn { get => m_local_players_count; set => m_local_players_count = value; }

    #region Properties

    //public bool Block
    //{
    //    get
    //    {
    //        return m_block;
    //    }

    //    set
    //    {
    //        m_block = value;
    //    }
    //}

    //public PlayerObserver CurrentPlayer
    //{
    //    get
    //    {
    //        return m_current_player;
    //    }
    //}
    //public PlayerObserver LocalPlayer
    //{
    //    get
    //    {
    //        return m_local_player;
    //    }

    //    private set
    //    {
    //        m_local_player = value;
    //    }
    //}
    //public List<PlayerObserver> AIPlayers
    //{
    //    get { return m_players.FindAll(x => x.EnableAI); } //!
    //}

    //public PhotonView View
    //{
    //    get
    //    {
    //        return m_view;
    //    }

    //    private set
    //    {
    //        m_view = value;
    //    }
    //}

    //public List<PlayerObserver> Players
    //{
    //    get
    //    {
    //        return m_players;
    //    }
    //}

    //public bool ForceValidation { get => m_force_fix_validation; set => m_force_fix_validation = value; }

    //[SerializeField] GameObject m_no_connection_instance;

    #endregion

    #region Events

    //public event Action<PlayerObserver> OnNewPlayer;
    //public event Action<PlayerObserver> OnPreTurn;
    //public event Action<PlayerObserver> OnTurn;
    //public event Action<PlayerObserver> OnEndTurn;
    //public event Action OnEndOrNext;
    //public event Action OnFinishGame;
    //public event Action<Player> OnMasterClientChanged;
    //public event Action<Player, PlayerObserver> OnPlayerLeftRoomEvent;
    //public event Action OnInitFinished;

    #endregion

    public void Spawn(int spawn_count = 4, Transform parent = null,string[] guids = null)
    {
        //Debug.Log($"<b>Spawner.Spawn</b> {spawn_count}",gameObject);
        m_spawn_count = spawn_count;

        var random_pos_pool = new List<Vector3>();

        for (int i = 0; i < m_spawn_count_max; i++)
        {
            var angle = i * (360 / m_spawn_count_max);
            var target = m_spawn_center.position + Utils.AngleToDirection(angle) * Distance;
            random_pos_pool.Add(target);
        }

        for (int i = 0; i < spawn_count; i++)
        {
            var pos = Vector3.zero;
            var rot = Quaternion.identity;
            var forward = Vector3.forward;

            if (m_use_spawn_points)
            {
                var spawn_point = m_spawn_points[(int)Mathf.Repeat(i, m_spawn_points.Count)];

                pos = spawn_point.position;
                rot = spawn_point.rotation;
                forward = spawn_point.forward;
            }
            else
            {
                var index = i;

                if (m_spawn_on_random_point)
                {
                    index = Random.Range(0, random_pos_pool.Count);
                    pos = random_pos_pool[index];
                    random_pos_pool.RemoveAt(index);
                }
                else
                {
                    pos = random_pos_pool[index];
                }
            }

            var new_character = Instantiate(m_character_pref, pos + forward, rot,parent);

            if (!m_use_spawn_points)
            {
                new_character.transform.LookAt(m_spawn_center);
            }
            var new_name = guids != null ? guids[i] : Guid.NewGuid().ToString();

            var skin = GetOffsettedSkin(i, m_spawn_offset);

            new_character.Init(new_name);
            new_character.SkinModule.Skin = skin;



            m_characters.Add(new_character);
            var data = new PlayerData(null, null, new_character);

            UI.Instance?.Init(new_character.Nickname, new_character);
            WorldTextManager.Instance?.Init(new_character);
            m_gm.InitPlayerData(data);
        }

        if (m_characters.Count >= m_local_players_count)
        {
            for (int i = 0; i < m_local_players_count; i++)
            {
                var m_character_local = m_characters[i];

                m_gm.InitLocalPlayer(m_character_local);
            }
        }

        m_gm.StartGame();
    }

    void Awake()
    {
        //m_view = GetComponent<PhotonView>();
        m_spawn_offset = Random.Range(0, Enum.GetNames(typeof(Skin)).Length - 1);
    }
    void Start()
    {
        //Debug.Log($"<b>Spawner.Start</b> Offline Mode: {PN.OfflineMode}");

        if (m_spawn_on_start /*&& PN.OfflineMode*/)
        {
            var spawn_count = GameData.Instance.SpawnCount;
            Spawn(spawn_count);
        }
    }
    void OnDrawGizmosSelected()
    {
        if (m_spawn_center)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(m_spawn_center.position, Distance);

            for (int i = 0; i < m_spawn_count_max; i++)
            {
                var angle = i * (360 / m_spawn_count_max);
                var target = m_spawn_center.position + Utils.AngleToDirection(angle) * Distance;

                Gizmos.color = Color.red;
                Gizmos.DrawLine(m_spawn_center.position, target);
            }
        }
    }
    Skin GetOffsettedSkin(int i, int offset = 0)
    {
        var index = i;

        if (offset != 0)
        {
            index = (int)Mathf.Repeat(i + offset, Enum.GetNames(typeof(Skin)).Length-1);
        }
        return (Skin)index;
    }

    //void KickUnloadedPlayer()
    //{
    //    Debug.Log($"<b>TurnManager.KickUnloadedPlayer</b>");
    //    for (int i = 0; i < m_ready.Count; i++)
    //    {
    //        if (!m_ready[i] && i < PN.CountOfPlayers)
    //        {
    //            Debug.Log($"Room: {PN.CountOfPlayers}, observers: {m_players.Count}");
    //            var not_ready_player = PN.PlayerList[i];
    //            if (not_ready_player != null)
    //            {
    //                var observer = m_players.Find(x => x.PlayerName == not_ready_player.NickName);
    //                Debug.Log($"<b>TurnManager.KickNotReadyPlayer</b> {not_ready_player.NickName}({not_ready_player.UserId})");
    //                PN.CloseConnection(not_ready_player);
    //                //TurnUI.ShowFooter(observer.View.ViewID);
    //                OnPlayerLeftRoom(not_ready_player);
    //            }
    //        }
    //    }
    //}
    void OnApplicationQuit()
    {
        Debug.LogWarning($"TurnManager.<color=red>OnAppQuit</color> Leaving room...");
        //PN.LeaveRoom();
    }
    void OnDestroy()
    {
        this.StopAllCoroutinesLogged();
    }

    #region Turn

    //[PunRPC]
    //void FinishGameRPC(int[] winners_view_ids, byte additional_reward)
    //{
    //    Debug.Log($"{"[RPC]".WrapWithTag(tag_value: "green")} {"TurnManager.FinishGame".WrapWithTag(tag_name: "b")}. Count: {winners_view_ids.Length}, win ids:{string.Join(", ", winners_view_ids)}; Reward: {additional_reward}");
    //}

    #endregion

    #region PUNCallbacks

    //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        stream.SendNext(m_disconnect_time);
    //        stream.SendNext(m_stop_waiting_and_start);
    //        stream.SendNext(m_last_spawn_point_id);
    //    }
    //    else
    //    {
    //        m_disconnect_time = (int)stream.ReceiveNext();
    //        m_stop_waiting_and_start = (bool)stream.ReceiveNext();
    //        m_last_spawn_point_id = (byte)stream.ReceiveNext();
    //    }
    //}

    //public override void OnJoinedRoom()
    //{
    //    Debug.LogError($"<b>TurnManager.OnJoinedRoom</b> <color=red>me: {PN.NickName}</color>");
    //    //RequestOwner((int)PN.CurrentRoom.CustomProperties[PN.LocalPlayer.NickName]);
    //    if (PN.IsMasterClient)
    //    {
    //        SpawnPlayer(m_last_spawn_point_id);
    //    }
    //}

    //[PunRPC]
    //public void SpawnPlayer(byte spawn_id)
    //{
    //    Debug.Log($"<b>Spawner.SpawnPlayer</b> {spawn_id}");

    //    var pref_path = "Prefs/";
    //    var spawn_point = m_spawn_points[spawn_id];
    //    var inst_data = new object[] { spawn_id, PN.LocalPlayer.NickName };
    //    var local_shield = PN.Instantiate(pref_path + m_shield_pref.name, spawn_point.position, spawn_point.rotation, data: inst_data).GetComponent<Shield>();
    //    var local_player = PN.Instantiate(pref_path + m_character_pref.name, spawn_point.position + spawn_point.forward, spawn_point.rotation, data: inst_data).GetComponent<Character>();

    //    local_shield.Init(local_player, (Skin)spawn_id);
    //    local_player.Init(PN.LocalPlayer.NickName, local_shield);

    //    var data = new PlayerData(PN.LocalPlayer, local_shield, local_player);

    //    GameManager.Instance.InitLocalPlayer(local_player);
    //    GameManager.Instance.InitPlayerData(data);
    //}

    //public override void OnPlayerEnteredRoom(Player newPlayer)
    //{
    //    Debug.Log($"<b>TurnManager.OnPlayerEnteredRoom</b> <color=gray>player:</color> {newPlayer.NickName}");
    //    //View.RPC("SetPlayersOrder", newPlayer);
    //    if (PN.IsMasterClient)
    //    {
    //        m_last_spawn_point_id = (byte)Mathf.Repeat(m_last_spawn_point_id + 1, m_spawn_points.Count);
    //        View.RPC(nameof(SpawnPlayer), newPlayer, m_last_spawn_point_id);
    //    }
    //}
    //public override void OnPlayerLeftRoom(Player otherPlayer)
    //{
    //    Debug.Log($"<b>TurnManager.OnPlayerLeftRoom:</b> <color=red>{otherPlayer.NickName}</color>");
    //    //var observer = m_players.Find(x => x.PlayerName == otherPlayer.NickName); //Find  in props
    //    //var index = m_players.IndexOf(observer);

    //    //if (observer != null && index != -1)
    //    //{
    //    //    var view_id = observer.View.ViewID;
    //    //    Debug.Log($"<b>TurnManager.OnPlayerLeftRoom</b> set rdy index: {index}, view_id: {view_id}");
    //    //    m_ready[index] = true;
    //    //}

    //    //observer.Disconnected = true;
    //    //OnPlayerLeftRoomEvent?.Invoke(otherPlayer, observer);
    //}
    //public override void OnMasterClientSwitched(Player newMasterClient)
    //{
    //    Debug.Log($"<b>TurnManager.<color=red>OnMasterClientSwitched:</color></b> {newMasterClient.NickName}");
    //    //if (PN.IsMasterClient)
    //    //{
    //    //    var room = PN.CurrentRoom;
    //    //    for (int i = 0; i < room.PlayerCount; i++)
    //    //    {
    //    //        m_players[i].View.TransferOwnership(room.Players[i]);
    //    //    }
    //    //}
    //    OnMasterClientChanged?.Invoke(newMasterClient);
    //}
    //public override void OnDisconnected(DisconnectCause cause)
    //{
    //    Debug.LogError($"<b>TurnManager.<color=red>OnDisconnected:</color></b> cause: {cause}. Time: {DateTime.Now.ToLongTimeString()}");
    //    //if (cause == DisconnectCause.DisconnectByClientLogic || cause == DisconnectCause.ClientTimeout)
    //    //{
    //    //    SceneManager.LoadScene(0);
    //    //}
    //    //else
    //    //{
    //    //    Debug.LogError($"TurnManager.Unhandled disconnect");
    //    //    SceneManager.LoadScene(0);
    //    //    //if (PN.ReconnectAndRejoin())
    //    //    //{
    //    //    //    var my_view = (int)PN.CurrentRoom.CustomProperties[PN.LocalPlayer.NickName];
    //    //    //    Debug.Log($"Reconnected <color=green>succesfully</color> Try to request ownership: {my_view}");
    //    //    //    RequestOwner(my_view);
    //    //    //}
    //    //}
    //}

    #endregion
}