﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using UnityEditor;

using UnityEngine;
using UnityUseful.Misc;
using Object = UnityEngine.Object;

[CustomEditor(typeof(ContextMenu), true)]
[CanEditMultipleObjects]
public class ContextMenuInspector : Editor
{
    public struct ContextMenuData
    {
        public string menuItem;
        public MethodInfo function;
        public MethodInfo validate;

        public ContextMenuData(string item)
        {
            menuItem = item;
            function = null;
            validate = null;
        }
    }

    public static void GUICallback(Object target)
    {
        var data = FindContextMenu(target);
        DrawContextMenuButtons(data, target);
    }
    protected Dictionary<string, ContextMenuData> contextData = new Dictionary<string, ContextMenuData>();

    static Dictionary<string,ContextMenuData> FindContextMenu(Object target)
    {
        var contextData = new Dictionary<string, ContextMenuData>();

        // Get context menu
        Type targetType = target.GetType();
        Type contextMenuType = typeof(ContextMenu);
        MethodInfo[] methods = GetAllMethods(targetType).ToArray();
        for (int index = 0; index < methods.GetLength(0); ++index)
        {
            MethodInfo methodInfo = methods[index];
            foreach (ContextMenu contextMenu in methodInfo.GetCustomAttributes(contextMenuType, false))
            {
                if (contextData.ContainsKey(contextMenu.menuItem))
                {
                    var data = contextData[contextMenu.menuItem];
                    if (contextMenu.validate)
                        data.validate = methodInfo;
                    else
                        data.function = methodInfo;
                    contextData[data.menuItem] = data;
                }
                else
                {
                    var data = new ContextMenuData(contextMenu.menuItem);
                    if (contextMenu.validate)
                        data.validate = methodInfo;
                    else
                        data.function = methodInfo;
                    contextData.Add(data.menuItem, data);
                }
            }
        }
        return contextData;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var data = FindContextMenu(target);
        DrawContextMenuButtons(data,target);
    }

    public static void DrawContextMenuButtons(Dictionary<string,ContextMenuData> contextData, Object target)
    {
        if (contextData.Count == 0) return;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Context Menu", EditorStyles.boldLabel);
        foreach (KeyValuePair<string, ContextMenuData> kv in contextData)
        {
            bool enabledState = GUI.enabled;
            bool isEnabled = true;
            if (kv.Value.validate != null)
                isEnabled = (bool)kv.Value.validate.Invoke(target, null);

            GUI.enabled = isEnabled;
            if (GUILayout.Button(kv.Key) && kv.Value.function != null)
            {
                kv.Value.function.Invoke(target, null);
            }
            GUI.enabled = enabledState;
        }
    }

    static IEnumerable<MethodInfo> GetAllMethods(Type t)
    {
        if (t == null)
            return Enumerable.Empty<MethodInfo>();
        var binding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        return t.GetMethods(binding).Concat(GetAllMethods(t.BaseType));
    }
}

