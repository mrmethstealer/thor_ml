﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

using System.IO;
using System.Linq;


public class ScenesShower : EditorWindow
{
    static ScenesShower instance;

    const string m_title = "Scenes in project:";
    const string m_title_tooltip = "All scenes added to build in player settings:";
    const string m_total_count_format = "Total scene count in settings: {0}";

    static readonly GUIContent header = new GUIContent(m_title, m_title_tooltip);

    static readonly Vector2 size = new Vector2(202, 20);
    static readonly Vector2 offset = new Vector2(5,100);

    static int m_scene_to_load_id;

    static bool autosave;

    [MenuItem("Window/Show All Scenes")]
    public static void Init()
    {
        instance = GetWindow<ScenesShower>();
        instance.titleContent = new GUIContent("Editor Scene Shower");
        instance.Show();
    }

    void OnGUI()
    {
        ShowScenesGUI();

        ShowAdditionalGUI();
    }

    static void ShowAdditionalGUI()
    {
        var rect = new Rect(offset.x, offset.y + size.y * (/*scene_count*/ +2), size.x, size.y);
#if BUILD_INSPECTOR
        if (GUI.Button(rect, $"Open Build Inpector"))
        {            
            BuildReportInspector.OpenLastBuild();
        } 
        rect.y += size.y;
#endif
        rect.width /= 2;
        if (GUI.Button(rect, "Scenes"))
        {
            var obj = AssetDatabase.LoadAssetAtPath<Object>("Assets/Scenes");

            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
        rect.x += rect.width;
        if (GUI.Button(rect, "Scripts"))
        {
            var obj = AssetDatabase.LoadAssetAtPath<Object>("Assets/Scripts");

            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }

    static void ShowScenesGUI()
    {
        GUILayout.Label(header, EditorStyles.boldLabel);
        EditorGUILayout.Space();
        GUILayout.Label(string.Format(m_total_count_format, SceneManager.sceneCountInBuildSettings));
        autosave = EditorGUILayout.Toggle("Enable Scene Autosave: ", autosave);
        EditorGUILayout.Space();

        var scenes = EditorBuildSettings.scenes;
        m_scene_to_load_id = GUILayout.SelectionGrid(m_scene_to_load_id, scenes.Select(x => Path.GetFileNameWithoutExtension(x.path)).ToArray(), 3, EditorStyles.miniButton);

        if (GUILayout.Button($"Load Scene: {Path.GetFileNameWithoutExtension(scenes[m_scene_to_load_id].path)}", GUILayout.MaxWidth(size.x)))
        {
            var scene = scenes[m_scene_to_load_id];
            var choice = 0;
            if (!autosave)
            {
                choice = EditorUtility.DisplayDialogComplex("Save current scene?", "Do You want to save current scene?", "Yes", "No", "Cancel");
            }
            switch (choice)
            {
                case 0: EditorSceneManager.SaveOpenScenes(); EditorSceneManager.OpenScene(scene.path); break;
                case 1: EditorSceneManager.OpenScene(scene.path); break;
                default: break;
            }
        }
    }

    void OnInspectorUpdate()
    {
        Repaint();
    }

}
