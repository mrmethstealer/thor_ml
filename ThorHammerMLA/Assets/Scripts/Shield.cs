﻿using UnityEngine;

public class Shield : MonoBehaviour//, IPunObservable
{
    [Header("Static:")]
    [SerializeField] Rigidbody m_rb;

    [SerializeField] float m_follow_speed = 2f;
    [SerializeField] float m_follow_distance_threshold;
    [Space]
    [SerializeField] Vector3 m_base_euler;


    [Header("Runtime:")]
    [SerializeField] Material m_shield_mat;
    [SerializeField] Character m_owner;

    public Character Owner { get => m_owner; }


    void Awake()
    {
        m_base_euler = transform.eulerAngles;
    }
    void Start()
    {
        ApplyInstatiationData();
    }
    public void FixedUpdate()
    {
        //!!if (!m_is_recovering && !m_is_dead)
        {
            if (m_owner)
            {
                if (Vector3.Distance(transform.position, m_owner.transform.position) > m_follow_distance_threshold)
                {
                    var destination = transform.position + transform.forward * Time.fixedDeltaTime * m_follow_speed;
                    var new_pos = Vector3.Lerp(transform.position, destination, .5f);

                    m_rb.MovePosition(new_pos);

                    var dir = (m_owner.transform.position - transform.position);
                    dir.y = 0;
                    var dir_norm = dir.normalized;
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir_norm, Vector3.up), .5f);
                }
            }
        }
    }


    void ApplyInstatiationData()
    {
        //if (!PN.OfflineMode)
        //{
        //    var view = GetComponent<PhotonView>();
        //    if (view)
        //    {
        //        var player_name = (string)view.InstantiationData[1];
        //        if (Owner == null)
        //        {
        //            m_owner = FindObjectsOfType<Character>().Where(x => x.GetComponent<PhotonView>().Owner.NickName == view.Owner.NickName).First();
        //        }
        //        UI.Instance.Init(player_name, Owner);
        //    }
        //}
    }
    public void Init(Character character, Skin skin)
    {
        m_owner = character;
    }



    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, m_follow_distance_threshold);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position + Vector3.up, transform.position + transform.forward + Vector3.up);
    }
}
