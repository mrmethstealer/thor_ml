﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

using System;
using System.Collections.Generic;


using System.Threading;
using System.Linq;

using UnityUseful.Misc;
using UnityUseful.Vibrate;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers; 
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions; 
#endif

public enum GamePhase : byte { Init, PreGame, Game, Finish }
public class GameManager : MonoBehaviour//Singleton<GameManager>, IPunObservable
{
    #region Collision Rewards
    public const int m_shield_hit_reward = 1;
    public const int m_hammer_hammer_reward = 1;
    public const int m_character_hit_reward = 1;
    public const int m_player_kill_reward = 1;
    public const int m_hammer_damage = 1;
    #endregion

    [Header("Components:")]
    [SerializeField] UI m_ui;
    [SerializeField] CameraController m_cam_control;
    [SerializeField] Spawner m_spawner;

    [Header("Dynamic:")]

    [SerializeField] protected GamePhase m_phase;
    [Space]
    [SerializeField, Reorderable] protected List<Character> m_characters;

    public virtual  void Reset()
    {
        foreach (var character in Characters)
        {
            Destroy(character.ThrowModule.Hammer.gameObject);
            Destroy(character.gameObject);
        }

        Characters.Clear();

        m_phase = GamePhase.Init;
    }

    [SerializeField, Reorderable] protected List<PlayerData> m_players_data;
    [SerializeField, Reorderable] List<CollisionData> m_collisions;
    [SerializeField] protected PlayerData m_winner_data;
    [Space]
    [SerializeField] Character m_local_player;
    [Space]
    [SerializeField] float m_game_countdown;
    [SerializeField] float m_pregame_countdown;
    [Space]
    [SerializeField] bool m_is_paused;
    [Space]
    public UnityEvent OnPregame;
    public UnityEvent OnStart;
    public UnityEvent OnFinish;
    CancellationTokenSource m_timer_token = new CancellationTokenSource();

    public event Action<Character> OnLocalPlayerAssigned;

    #region Properties

    public Character LocalCharacter { get => m_local_player; }
    public List<Character> Characters { get => m_characters; }
    public bool IsPaused
    {
        get => m_is_paused;
        set
        {
            m_is_paused = value;
            Time.timeScale = value ? 1 : 0;
        }
    }

    public Spawner Spawner { get => m_spawner; }
    public GamePhase Phase { get => m_phase;}
    public PlayerData WinnerData { get => m_winner_data; }

    #endregion

    async void Start()
    {

        //if (!PhotonNetwork.OfflineMode)
        //{
        //    await WaitWhile(() => PhotonNetwork.NetworkClientState != ClientState.Joined);
        //}

    }
#if !UNITY_WEBGL
    public async virtual void StartGame()
    {
        var settings = GameData.Instance;

        m_pregame_countdown = settings.PregameTime;
        m_phase = GamePhase.PreGame;

        UI.Instance.CenterMessagePanel.ShowMessage("<b>Ready?</b>", duration: settings.PregameTime - .25f, fade_in: .1f, fade_out: .1f, priority: true);

        OnPregame?.Invoke();

        await ProgressTimer(settings.PregameTime, (prog, delta) =>
        {
            m_pregame_countdown -= delta;
            UI.Instance.CountdownTimerText.text = m_pregame_countdown.ToString("0");
        });

        var transition_delay = 2f;

        UI.Instance.CountdownTimerText.text = "<b>-</b>";
        UI.Instance.CenterMessagePanel.ShowMessage("<b>Go!</b>", duration: transition_delay, fade_in: .1f, fade_out: .1f, priority: true);

        await Delay(transition_delay);

        m_phase = GamePhase.Game;
        m_game_countdown = settings.GameTime;
        StartCountdown(m_timer_token);
        m_characters.ForEach(x => x.IsInputBlocked = false);

        var target_ui = GameUI.Instance;
        if (target_ui)
        {
            target_ui.From = m_local_player.transform;
            target_ui.Targets.AddRange(m_characters.Where(x => !x.IsLocal).Select(x => x.transform));
        }

        Time.timeScale = 1f;

        OnStart?.Invoke();
    } 
#elif UNITY_WEBGL
    public virtual void StartGame()
    {
        var settings = GameData.Instance;

        m_pregame_countdown = settings.PregameTime;
        m_phase = GamePhase.PreGame;

        UI.Instance.CenterMessagePanel.ShowMessage("<b>Ready?</b>", duration: settings.PregameTime - .25f, fade_in: .1f, fade_out: .1f, priority: true);

        OnPregame?.Invoke();

        this.ProgressTimerV(settings.PregameTime, (prog, delta) =>
        {
            m_pregame_countdown -= delta;
            UI.Instance.CountdownTimerText.text = m_pregame_countdown.ToString("0");
        }, finals: () =>
        {

            var transition_delay = 2f;

            UI.Instance.CountdownTimerText.text = "<b>-</b>";
            UI.Instance.CenterMessagePanel.ShowMessage("<b>Go!</b>", duration: transition_delay, fade_in: .1f, fade_out: .1f, priority: true);

            this.TimerV(transition_delay, () =>
             {

                 m_phase = GamePhase.Game;
                 m_game_countdown = settings.GameTime;
                 StartCountdown(m_timer_token);
                 m_characters.ForEach(x => x.IsInputBlocked = false);

                 var target_ui = GameUI.Instance;
                 if (target_ui)
                 {
                     target_ui.From = m_local_player.transform;
                     target_ui.Targets.AddRange(m_characters.Where(x => !x.IsLocal).Select(x => x.transform));
                 }

                 Time.timeScale = 1f;

                 OnStart?.Invoke();
             });
        });
    }
#endif


    void OnDestroy()
    {
        Time.timeScale = 1f;
        if (m_timer_token != null)
        {
            m_timer_token.Cancel();
        }
    }

    public virtual void InitLocalPlayer(Character character)
    {
        Debug.Log($"<b>GameManager.InitLocalPlayer</b>", character);

        m_local_player = character;
        m_local_player.Nickname = GameData.Instance.Nickname;
        m_local_player.IsLocal = true;
        m_local_player.OnPointsChanged += (value, delta) => 
        {
            m_ui.Score.text = value.ToString(); 
        };
        m_local_player.HealthModule.OnDeath.AddListener(() => GameData.Instance.GlobalScore += character.Points);
        ScreenController.Instance.Init(character);


        Destroy(m_local_player.GetComponent<AIModule>());
        Destroy(m_local_player.GetComponent<NavMeshAgent>());
        var ml_agent_comp = m_local_player.GetComponentInChildren<ThorCharacterAgent>(includeInactive: true);
        if (ml_agent_comp)
        {
            Destroy(ml_agent_comp.gameObject);
        }

        m_cam_control.Target = character.transform;

        var other_players = m_players_data.Select(x => x.Character).Where(x => !x.IsLocal).ToArray();
        OffscreenDirection.Instance.Init(character.transform, other_players);

        OnLocalPlayerAssigned?.Invoke(character);
    }
    public virtual void InitPlayerData(PlayerData data)
    {
        m_players_data.Add(data);
        m_characters.Add(data.Character);
        data.Character.IsInputBlocked = true;
        data.Character.HealthModule.OnDeath.AddListener( async () =>
        {

            var hammer = data.Character.HealthModule.KilledBy.GetComponent<Hammer>();
            if (hammer)
            {
                Debug.Log($"<b>GameManager.HealthModule.OnDeath:</b> +{m_player_kill_reward} to {hammer.Owner}, local:{data.Character.IsLocal}", hammer.Owner);
                hammer.Owner.Points += m_player_kill_reward;
            }
            data.Character.IsInputBlocked = true;
            data.Character.MoveModule.ShowDirectionArrow = false;

            var alive = m_players_data.FindAll(x => !x.Character.HealthModule.IsDead);
            if (alive.Count == 1)
            {
                FinishGame(GameEndReason.Lastman, alive.First());
            }
            else
            {
                if (data.Character.IsLocal)
                {
                    //CameraController.Instance.ShotAboveArea();

                    var msg = "";
                    GameEndUI.Instance.ShowGameOverPanel(msg, () => UnityEngine.SceneManagement.SceneManager.LoadScene(0));
                }
            }

            data.Character.ThrowModule.Hammer.CollidersEnabled = false;
        });
    }
    public async virtual void FinishGame(GameEndReason reason, PlayerData winner) 
    {
        Debug.Log($"<b>GameManager.FinishGame</b> reason: {reason}");
        m_phase = GamePhase.Finish;
        m_winner_data = winner;
        var scene_to_load = 0;
        m_ui.transform.GetChild(1).gameObject.SetActive(false); //

        var msg = "Time has expired!";
        switch (reason)
        {
            case GameEndReason.Timer:
                var max_points_data = m_players_data.OrderByDescending(x => x.Character.Points).Take(1).ToArray()[0];
                m_winner_data = new PlayerData(null, null, max_points_data.Character);
                break;
            case GameEndReason.Lastman:
                break;
            case GameEndReason.Other:
            default: throw new NotImplementedException();
        }

        m_timer_token.Cancel();

        var winner_character = m_winner_data.Character;

        msg = $"Winner: {winner_character.Nickname} with {winner_character.Points} points!";
        Debug.Log($"<b>GameManager.FinishGame</b> {msg}",winner_character);

        //await WaitWhile(()=>!winner_character.ThrowModule.HasHammer);

        //var hammer = winner_character.ThrowModule.Hammer;
        //hammer.transform.localScale *= .8f;

        foreach (var character in m_characters)
        {
            character.IsInputBlocked = true;
            var ai = character.GetComponent<AIModule>();
            if (ai)
            {
                Destroy(ai);
            }
            var ml = character.GetComponentInChildren<ThorCharacterAgent>();
            if (ml)
            {
                Destroy(ml.gameObject);
            }
        }
        m_ui.Canvas.enabled = false;

        winner_character.IsInputBlocked = true;
        winner_character.MoveModule.ShowDirectionArrow = false;
        winner_character.MoveModule.ShowDirectionAsPoints = false;
        winner_character.GetComponent<Animator>().SetTrigger("Win");

        if (winner_character.IsLocal)
        {
            GameData.Instance.GlobalScore += winner_character.Points;
        }
        else
        {
            VibrateControll.Vibrate(1.5f);
        }

        m_cam_control.WinnerShot(winner_character.transform);

        OnFinish?.Invoke();
#if !UNITY_WEBGL
        await Delay(6f); 
#elif UNITY_WEBGL
        this.TimerV(6f,()=> 
        {
#endif

        m_ui.Canvas.enabled = true;

        GameEndUI.Instance.ShowGameOverPanel(msg, () => UnityEngine.SceneManagement.SceneManager.LoadScene(scene_to_load));
        Time.timeScale = 0f;
        m_timer_token.Cancel();

#if UNITY_WEBGL
        }); 
#endif
    }

#if !UNITY_WEBGL
    async void StartCountdown(CancellationTokenSource token)
    {
        await ReverseTimer((int)m_game_countdown, (seconds, delta) =>
        {
            m_game_countdown = seconds;
            m_ui.CountdownTimerText.text = m_game_countdown.ToString("0");
        }, () => token.IsCancellationRequested);

        if (!token.IsCancellationRequested)
        {

            FinishGame(GameEndReason.Timer, null);
        }
    } 
#elif UNITY_WEBGL
    void StartCountdown(CancellationTokenSource token)
    {
        this.ReverseTimerV((int)m_game_countdown, (seconds, delta) =>
        {
            m_game_countdown = seconds;
            m_ui.CountdownTimerText.text = m_game_countdown.ToString("0");
        }, () => token.IsCancellationRequested, () =>
         {

             if (!token.IsCancellationRequested)
             {

                 FinishGame(GameEndReason.Timer, null);
             }
         });
    }
#endif
}
    public enum GameEndReason : byte
    {
        Timer,
        Lastman,
        Other
    }

    [Serializable]
    public class PlayerData
    {
        [SerializeField] Shield m_shield;
        [Space]
        [SerializeField] Character m_character;
        public Shield Shield { get => m_shield; }
        public Character Character { get => m_character; }

        public PlayerData(object _player, Shield _shield, Character _character)
        {
            m_shield = _shield;
            m_character = _character;
        }
    public override string ToString()
    {
        return $"Character: {m_character?.name??"null"}";
    }
}

[Serializable]
public class CollisionData
{
    [SerializeField] public GameObject m_from;
    [SerializeField] public Collider m_collider;
    [SerializeField] public GameObject m_to;
    [Space]
    [SerializeField] public string m_info;

    public CollisionData(GameObject from, GameObject to, string info)
    {
        m_from = from;
        m_to = to;
        m_info = info;
    }
}
