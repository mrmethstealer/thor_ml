﻿//using UnityEngine;

//using System;
//using System.Linq;
//using System.Collections;
//using System.Collections.Generic;

//using Photon.Pun;
//using PN = Photon.Pun.PhotonNetwork;

//using Newtonsoft.Json;
//using Photon.Realtime;

//using UnityUseful.Misc;
//using UnityUseful.CorotineInstructionsWrappers;
//using static UnityUseful.IEnumeratorExtension.IEnumeratorExtension;

//[RequireComponent(typeof(PhotonView))]
//public class PlayerObserver : MonoBehaviour//, IPunObservable, IPunOwnershipCallbacks
//{
//    const int move_track = 0;

//    #region Components
//    [SerializeField, HideInInspector] PhotonView m_view;
//    [SerializeField, HideInInspector] AnimationState m_state;
//    [SerializeField, HideInInspector] AI m_ai_module;
//    [SerializeField, HideInInspector] Spawner m_turn;
//    #endregion

//    [Header("Animation Values:")]
//    [SerializeField] AnimationName m_cur_name;
//    [SerializeField] AnimationName m_start_anim_name = AnimationName.Side_Idle;
//    [SerializeField] Skin m_skin;
//    [Space]
//    [Header("Game Data:")]
//    [SerializeField] byte m_attempts;
//    [SerializeField] byte m_score;
//    [SerializeField] byte m_id;
//    [Space]
//    [SerializeField] string m_player_name;
//    [Space]
//    [SerializeField] bool m_disconnected;
//    [SerializeField] bool m_inactive;
//    [Header("Movement Data:")]
//    [SerializeField] bool m_is_moving;
//    [Space]
//    [SerializeField] float m_movement_progress;
//    [Space]
//    [SerializeField,Tooltip("Index of point in some collection")] int m_position_id=-1;
//    [Space]
//    [SerializeField] List<MovementQueueItem> m_move_queue = new List<MovementQueueItem>();

//    #region Props

//    string MoveQueueString
//    {
//        get => JsonConvert.SerializeObject(m_move_queue);
//        set => m_move_queue = JsonConvert.DeserializeObject<List<MovementQueueItem>>(value);
//    }

//    public byte Attempts
//    {
//        get
//        {
//            return m_attempts;
//        }

//        set
//        {
//            m_attempts = value;
//        }
//    }
//    public byte Score
//    {
//        get
//        {
//            return m_score;
//        }

//        set
//        {
//            m_score = value;
//        }
//    }
//    public byte Id
//    {
//        get
//        {
//            return m_id;
//        }
//    }

//    public bool IsMoving
//    {
//        get
//        {
//            return m_is_moving;
//        }
//    }
//    public bool EnableAI
//    {
//        get => m_ai_module.enabled;
//        set
//        {
//            //Debug.Log($"PlayerObserver.EnableAI {value}, name: {name}",gameObject);
//            m_ai_module.enabled = value;
//        }
//    }

//    public AnimationName CurrentAnimation
//    {
//        get
//        {
//            return m_cur_name;
//        }
//    }
//    public Skin Skin
//    {
//        get
//        {
//            return m_skin;
//        }
//    }

//    public PhotonView View
//    {
//        get
//        {
//            return m_view;
//        }
//    }

//    public AnimationState State
//    {
//        get
//        {
//            return m_state;
//        }
//    }

//    public bool Disconnected { get => m_disconnected; set => m_disconnected = value; }
//    public bool Inactive { get => m_inactive; set => m_inactive = value; }
//    public string PlayerName { get => m_player_name; }
//    public int PositionId { get => m_position_id; set => m_position_id = value; }
//    public float MovementProgress { get => m_movement_progress; set => m_movement_progress = value; }
//    public List<MovementQueueItem> MoveQueueList { get => m_move_queue; }

//    #endregion

//    void Awake()
//    {

//        //m_view = GetComponent<PhotonView>();
//        //m_ai_module = GetComponent<AI>();

//        //var data = m_view.InstantiationData;
//        //if (data != null)
//        //{
//        //    name = data[0].ToString();
//        //    if ((bool)data[2])
//        //    {
//        //        //gameObject.AddComponent<AI>();
//        //    }
//        //    m_id = (byte)data[3];
//        //    m_skin = (Skin)m_id;
//        //    PositionId = m_id;
//        //    m_player_name = (string)data[4];
//        //}

//        //m_turn = FindObjectOfType<Spawner>();//

//        //if (m_turn)
//        //{
//        //    //var props = PN.CurrentRoom.CustomProperties;

//        //    //Score = (int)props[name];
//        //    //Debug.Log($"Get <color=green>{name}</color> score from room: {Score}");
//        //    m_turn.AddPlayer(this);
//        //}
//    }
//    void Start()
//    {

//    }
//    void OnDestroy()
//    {
//        //m_turn.RemoveUser(this);
//    }
//    void OnEnable()
//    {
//        PN.AddCallbackTarget(this);
//    }
//    void OnDisable()
//    {
//        PN.RemoveCallbackTarget(this);
//    }

//    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
//    {
//        stream.Serialize(ref m_is_moving);
//        stream.Serialize(ref m_disconnected);
//        stream.Serialize(ref m_inactive);
//        stream.Serialize(ref m_position_id);

//        if (stream.IsWriting)
//        {
//            stream.SendNext(Attempts);
//            stream.SendNext(Score);
//            stream.SendNext(m_movement_progress);
//            //stream.SendNext(MoveQueueString);
//        }
//        else
//        {
//            Attempts = (byte)stream.ReceiveNext();
//            Score = (byte)stream.ReceiveNext();
//            m_movement_progress = (float)stream.ReceiveNext();
//            //MoveQueueString = (string)stream.ReceiveNext();
//        }
//    }

//    public static Color SkinToColor(Skin skin)
//    {
//        switch (skin)
//        {
//            case Skin.Blue: return Color.blue;
//            case Skin.Green: return Color.green;
//            case Skin.Orange: return Color.yellow;
//            case Skin.Red: return Color.red;
//            default: return Color.white;
//        }
//    }
//#if UNITY_EDITOR
//    [SerializeField] Color[] m_gizmos_skin_colors;
//    void OnDrawGizmos()
//    {
//        if (m_move_queue.Count > 0)
//        {
//            Gizmos.color = m_gizmos_skin_colors[(int)m_skin];
//            Vector3 start_point = transform.position;

//            foreach (var item in m_move_queue)
//            {
//                Gizmos.DrawLine(start_point, item.destination);
//                start_point = item.destination;
//            }
//        }
//    }
//#endif

//    //public void OnOwnershipRequest(PhotonView targetView, Player requestingPlayer)
//    //{
//    //    Debug.LogWarning($"<b>PlayerObserver.OnOwnershipRequest</b> view_id: {targetView.ViewID}, requesting_player: {requestingPlayer.NickName}");
//    //}
//    //public void OnOwnershipTransfered(PhotonView targetView, Player previousOwner)
//    //{
//    //    Debug.LogWarning($"<b>PlayerObserver.OnOwnershipTransfered</b> view_id: {targetView.ViewID}, prev_owner: {previousOwner?.NickName}");
//    //    if (targetView.ViewID == View.ViewID)
//    //    {
//    //        Debug.Log($"<b>Player observer({name}): Ownership transfered. from {previousOwner?.NickName} to {View.Owner?.NickName} </b>");
//    //        var owner = View.Owner;
//    //        if (owner != null)
//    //        {
//    //            EnableAI = false;
//    //        }
//    //        m_turn.SetReadyRPCWrapper(Id);
//    //    }
//    //}
//}


//public enum AnimationName : byte
//{
//    Front_Happy, Front_Run, Front_Sad, //front view
//    Back_Idle, Back_Shoot, //backview
//    Side_Idle, Side_Run, // sideview
//    Side_Blow,
//    Boom,
//    Hit,
//    Idle
//}

//public struct AnimationQueueItem
//{
//    public AnimationName anim_name;
//    public bool loop;

//    public AnimationQueueItem(AnimationName _name, bool _loop)
//    {
//        anim_name = _name;
//        loop = _loop;
//    }
//}

//[Serializable]
//public class MovementQueueItem
//{
//    [JsonProperty(PropertyName = "t")] public float duration;
//    [Space]
//    [JsonProperty(PropertyName = "a")] public byte animation;
//    [JsonProperty(PropertyName = "n")] public byte next_animation;
//    [Space]
//    [JsonProperty(PropertyName = "p")] public int pos_id;
//    [Space]
//    [JsonProperty(PropertyName = "d")] public Vector3 destination;
//    [JsonProperty(PropertyName = "s", NullValueHandling = NullValueHandling.Ignore)] public Vector3? start_pos;

//    [JsonProperty(PropertyName = "f")] public bool finished;

//    [NonSerialized] public readonly Action OnDestinationComplete;

//    public MovementQueueItem(Vector3 _destination, float _duration = 2, byte _animation = 6, byte _next_animation = 5, Action _OnDestinationComplete = null, Vector3? _start_pos = null)
//    {
//        duration = _duration;
//        animation = _animation;
//        next_animation = _next_animation;
//        destination = _destination;
//        start_pos = _start_pos ?? null;
//        OnDestinationComplete = _OnDestinationComplete;
//    }
//}

//[Serializable]
//public class SendMovementData
//{
//    [JsonProperty(PropertyName ="v")]
//    public int view_id;
//    [JsonProperty(PropertyName = "s")]
//    public string serialized_queue;
//}