﻿using UnityEngine;

using System.Collections.Generic;

using UnityUseful.Misc;

[RequireComponent(typeof(AudioSource))]
public class AudioModule : MonoBehaviour
{
    [SerializeField, Reorderable] List<AudioClip> m_clips;

    [Header("Sources:")]
    [SerializeField] AudioSource m_source;
    [SerializeField] AudioSource m_additional_source;
    [SerializeField] AudioSource m_shield_source;

    #region Properties
    public AudioSource AdditionalSource { get => m_additional_source; }
    public AudioSource ShieldSource { get => m_shield_source; }
    #endregion

    void Awake()
    {
        m_source = GetComponent<AudioSource>();
    }
    void Start()
    {
        MuteAllSources(!GameData.Instance.EnableSound);
    }
    public void PlaySound(Sound type, Source use_source = Source.Main)
    {
        if (GameData.Instance.EnableSound)
        {
            AudioSource source = null;
            switch (use_source)
            {
                case Source.Main:
                    source = m_source;
                    break;
                case Source.Additional:
                    source = m_additional_source;
                    break;
                case Source.Shield:
                    source = m_shield_source;
                    break;
                default:
                    throw new System.NotImplementedException();
            }
            if (enabled)
            {
                source.PlayOneShot(m_clips[(int)type]);
            }
        }
    }
    void MuteAllSources(bool mute) 
    {
        Debug.Log($"<b>AudioModule.MuteAllSources</b> mute: {mute}");

        m_source.mute = mute;
        m_additional_source.mute = mute;
        m_shield_source.mute = mute;
    }
}
public enum Sound
{
    PowerEvent = 0,
    HitHammerSuccess,
    Throw,
}
public enum Source 
{
    Main,
    Additional,
    Shield,
}
