﻿using UnityEngine;
using UnityEngine.AI;

using System.Linq;
using System.Threading.Tasks;

[RequireComponent(typeof(Character), typeof(NavMeshAgent),typeof(Animator))]
public class AIModule : MonoBehaviour
{
    const float search_radius_const = 6f;

    [SerializeField, HideInInspector] Character m_character;
    [SerializeField, HideInInspector] NavMeshAgent m_agent;
    [SerializeField, HideInInspector] Animator m_animator;

    [Header("Init Data:")]
    [SerializeField] float m_search_radius = search_radius_const;
    [SerializeField] float m_attack_radius = 6f;
    [SerializeField] float m_random_radius = 2f;
    [SerializeField, Range(2f, 5f)] float m_max_random_cd;
    [Space]
    [SerializeField] LayerMask m_target_layer;
    [SerializeField] LayerMask m_sight_layer;
    [Space]
    [SerializeField, Range(.1f, 10f)] float m_scatter = .1f;
    [Space]
    [SerializeField] bool enable_random_movement = true;
    [SerializeField] bool enable_throwing = true;

    [Header("Dynamic:")]
    [SerializeField] Character m_current_target;
    [SerializeField] GameObject m_last_raycast;
    [Space]
    [SerializeField] int m_target_not_found_frames;
    [SerializeField] int m_last_random_find_loops;
    [Space]
    [SerializeField] bool m_on_line_of_sign;

    void Awake()
    {
        m_agent = GetComponent<NavMeshAgent>();
        m_character = GetComponent<Character>();
        m_animator = GetComponent<Animator>();

        m_character.HealthModule.OnDeath.AddListener(()=>
        {
            enabled = false;
        });
    }
    void OnDisable()
    {
        //Debug.LogWarning($"<b>AI.OnDisable</b>");
        if (m_agent && m_agent.isOnNavMesh)
        {
            m_agent.isStopped = true;
        }
        m_character.MoveModule.Direction = Vector2.zero;
    }
    void Update()
    {
        if (!m_character.HealthModule.IsDead && !m_character.IsInputBlocked)
        {
            if (enable_throwing)
            {
                if (FindTarget())
                {
                    AttackTarget();
                } 
            }

            m_character.HammerGetBack();

            if (enable_random_movement)
            {
                if (!m_agent.pathPending && m_agent.remainingDistance < 0.5f && !m_on_line_of_sign)
                {
                    MoveToRandomDirection();
                }
            }

            m_animator.SetBool("Move", !m_agent.isStopped);
            m_animator.SetFloat("DirY", !m_agent.isStopped ? 1 : 0);
        }

        if (m_character.MoveModule.IsStunned)
        {
            if (!m_agent.isStopped)
            {
                m_agent.isStopped = true;
                m_agent.ResetPath();
            }
        }
        else
        {
            if (m_agent.isStopped)
            {
                m_agent.isStopped = false;
            }
        }
    }

    bool FindTarget()
    {
        if (m_current_target)
        {
            if (m_current_target.HealthModule.IsDead)
                m_current_target = null;
        }
        else
        {
            var colls = Physics.OverlapSphere(transform.position, m_search_radius, m_sight_layer).Where(x =>
            {
                var shield = x.GetComponentInParent<HealthModule>();
                return shield != m_character.HealthModule && !shield.IsDead;
            });

            //Debug.LogError($"Colliders: {colls.Count()}, {string.Join(",", colls.ToList())}");

            if (colls.Count() > 0)
            {
                var cur_pos = transform.position;
                var closest = colls.Min(x => (Vector3.Distance(x.transform.position, cur_pos), x));

                //Debug.LogWarning($"{name} Closest: {closest.x.name}, distance: {closest.Item1}");
                m_current_target = closest.x.GetComponentInParent<Character>();

                m_target_not_found_frames = 0;
                m_search_radius = search_radius_const;
            }
            else
            {
                m_target_not_found_frames++;
            }
            if (m_target_not_found_frames % 99 == 0)
            {
                m_search_radius++;
                m_target_not_found_frames = 0;
            }
        }

        return m_current_target != null;
    }

    async void AttackTarget()
    {
        if (m_current_target && !m_current_target.HealthModule.IsDead)
        {
            if (m_character.ThrowModule.HasHammer)
            {

                m_on_line_of_sign = true;

                m_agent.isStopped = true;
                m_agent.ResetPath();

                var scatter_offset = Random.insideUnitSphere * m_scatter;
                scatter_offset.y = 0;
                var target_with_offset = m_current_target.transform.position + scatter_offset;


                transform.LookAt(target_with_offset, Vector3.up);



                m_character.MoveModule.Rb.velocity = Vector3.zero;
                m_agent.ResetPath();

                var random_cd = Random.Range(2f, m_max_random_cd);
                m_character.ThrowHammer(override_cd: random_cd);

                m_character.IsInputBlocked = true;
                await Task.Delay(500);
                m_character.IsInputBlocked = false;
            }
            else
            {
                m_on_line_of_sign = false;
                m_agent.isStopped = false;
            }
        }
    }
    void MoveToRandomDirection()
    {
        if (!m_character.IsInputBlocked && !m_character.MoveModule.IsStunned)
        {
            if (m_current_target && Vector3.Distance(transform.position, m_current_target.transform.position) > m_attack_radius)
            {
                var dir = m_current_target.transform.position - transform.position;
                var point = transform.position + dir * .5f;

                m_agent.SetDestination(point);
            }
            else
            {
                var exit = false;
                Vector2 in_circle_point;
                Vector3 point;
                m_last_random_find_loops = 0;

                do
                {
                    m_last_random_find_loops++;

                    in_circle_point = Random.insideUnitCircle;
                    point = m_character.transform.position + new Vector3(in_circle_point.x, 0, in_circle_point.y) * m_random_radius;

                    //if (Physics.Raycast(point + Vector3.up, Vector3.down, out var hit_info))
                    //{
                    //    if (hit_info.collider.CompareTag("Arena"))
                    //    {
                    //        exit = true;
                    //    }
                    //}
                    var colls = Physics.OverlapSphere(point, m_agent.radius);
                    if (colls.All(x => x.CompareTag("Arena")))
                    {
                        exit = true;
                    }

                } while (!exit);


                //Debug.Log($"<b>AI.MoveToRandomDirection</b> point: {point}");
                m_agent.SetDestination(point);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, m_search_radius);

        if (m_character && m_character.HealthModule)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(m_character.HealthModule.transform.position, m_random_radius);
        }

        if (m_current_target)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, m_attack_radius);
        }

        if (m_agent && m_agent.hasPath)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, m_agent.destination);
            Gizmos.DrawWireSphere(m_agent.destination, m_agent.radius);
        }
    }
}
