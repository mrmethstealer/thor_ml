﻿using UnityEngine;

using System.Collections;

public class MoveModule : MonoBehaviour
{
    static int hash_move = Animator.StringToHash("Move");
    static int hash_dirX = Animator.StringToHash("DirX");
    static int hash_dirY = Animator.StringToHash("DirY");
    static int hash_stunned = Animator.StringToHash("IsStunned");

    [Header("Config Data:")]
    [SerializeField] GameData m_data;

    [Header("Static components:")]
    [SerializeField] Rigidbody m_rb;
    [SerializeField] Animator m_animator;

    [Header("Other:")]
    [SerializeField] Transform m_directional_arrow;
    [SerializeField] Transform m_directional_points;

    [Header("Runtime:")]
    [SerializeField] Vector2 m_direction;
    [Space]
    [SerializeField] float m_move_speed = 2f;
    [SerializeField] float m_rot_speed = 3f;
    [Space]
    [SerializeField] bool m_is_stunned;
    [SerializeField] bool m_is_moving;
    [SerializeField] bool m_reset_direction = true;

    #region Properties
    public bool IsMoving
    {
        get => m_is_moving;

        set
        {
            m_is_moving = value;
            if (m_animator)
            {
                m_animator.SetBool(hash_move, value);
            }
        }
    }

    public Vector2 Direction
    {
        get => m_direction;
        set
        {
            m_direction = value;

            if (m_animator)
            {
                m_animator.SetFloat(hash_dirY, value.y);
                m_animator.SetFloat(hash_dirX, value.x);
            }

            IsMoving = m_direction.sqrMagnitude > 0;
        }
    }

    public Rigidbody Rb { get => m_rb; }

    public Animator Animator { get => m_animator; }

    public bool IsStunned { get => m_is_stunned; }

    public bool ResetDirection 
    {
        get => m_reset_direction;
        set => m_reset_direction = value;
    }
    public bool ShowDirectionArrow { set => m_directional_arrow.gameObject.SetActive(value); }

    public bool ShowDirectionAsPoints { set => m_directional_points.gameObject.SetActive(value); }
    #endregion

    void Awake()
    {
        if (m_data)
        {
            m_move_speed = m_data.CharacterMoveSpeed;
            m_rot_speed = m_data.CharacterRotSpeed;
        }
    }
    void Start()
    {
        GetComponent<HealthModule>().OnDeath.AddListener(() =>
        {

            Direction = Vector2.zero;

            m_rb.isKinematic = true;
        });
    }

    void FixedUpdate()
    {
        if (IsMoving)
        {
            var y = m_direction.y * Time.fixedDeltaTime * m_move_speed;

            if (m_direction.y < 0)
            {
                y /= 2f;
            }

            //var lerped = Vector3.Lerp(m_rb.position, m_rb.position + transform.forward * y, .5f);
            //m_rb.MovePosition(lerped);

            m_rb.AddForce(transform.forward * y * m_move_speed, ForceMode.VelocityChange);

            var dir = new Vector3(m_direction.x, 0, m_direction.y) * Time.fixedDeltaTime * m_move_speed;
            if (dir != Vector3.zero)
            {
                var look_rot = Quaternion.LookRotation(transform.TransformDirection(dir));
                m_directional_arrow.rotation = look_rot;

                //transform.RotateAround(transform.position, Vector3.up, m_direction.x * Time.fixedDeltaTime * m_rot_speed);
                m_rb.MoveRotation(Quaternion.Lerp(m_rb.rotation, look_rot, Time.fixedDeltaTime * m_rot_speed));
            }

            if (m_reset_direction)
            {
                m_direction = Vector3.zero;
            }
        }
        if (m_reset_direction)
        {

            IsMoving = m_rb.velocity.sqrMagnitude != 0;
        }
        else
        {
            IsMoving = m_direction.sqrMagnitude != 0;
        }
    }
    public void Restore() 
    {
        m_is_stunned = false;
        m_rb.isKinematic = false;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        var dir = new Vector3(Direction.x, 0, Direction.y);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.TransformDirection(dir));
    }
}
