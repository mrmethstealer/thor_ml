﻿using UnityEngine;

using System;
using System.Linq;
using System.Collections.Generic;

using UnityUseful.Misc;
using UnityEngine.Events;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using UnityUseful.AsyncExtensions; 
using System.Threading.Tasks;
#endif

public class HealthModule : MonoBehaviour
{
    [SerializeField] GameData m_data;
    [SerializeField] Rigidbody m_rb;
    [SerializeField] Character m_character;
    [Space]
    [SerializeField, Reorderable] List<ReceiveDamageModule> m_damage_receivers;

    [Header("Init Data:")]
    [SerializeField] int m_health_max = 5;
    [Space]
    [SerializeField] bool m_allow_self_collision;
    [SerializeField] bool m_init = false;
    [Space]
    [SerializeField] float m_invulnerability_time = 2f;
    [SerializeField] LayerMask m_enabled_layer = 11;
    [SerializeField] LayerMask m_disabled_layer = 12;

    [Header("Runtime:")]
    [SerializeField] float m_invulnerability_time_left;
    [SerializeField] float restore_progress;
    [Space]
    [SerializeField] int m_health_current;
    [Space]
    [SerializeField] bool m_is_dead;
    [SerializeField] bool m_is_recovering;
    [Space]
    [SerializeField] GameObject m_killed_by;

    #region Properties
    public int Health
    {
        get => m_health_current;
        set
        {
            if (!m_is_dead && m_invulnerability_time_left == 0)
            {
                m_health_current = Mathf.Clamp(value, 0, m_health_max);

                OnHealthChanged?.Invoke(m_health_current);
                var normalized = m_health_current / (float)m_health_max;
                OnHealthChangedNormalized?.Invoke(normalized);

                if (value > 0)
                {
                    OnHpChange();
                }
                else
                {
                    m_is_dead = true;

                    OnDeath?.Invoke();
                }
            }
        }
    }
    public bool IsDead { get => m_is_dead; }
    public GameObject KilledBy { get => m_killed_by; }
    public List<ReceiveDamageModule> DamageReceivers { get => m_damage_receivers; }
    public int HealthMax { get => m_health_max; }
    #endregion

    public UnityEventInt OnHealthChanged;
    public UnityEngine.UI.Scrollbar.ScrollEvent OnHealthChangedNormalized;
    public UnityEvent OnDeath;

    void Awake()
    {
        if (m_data)
        {
            m_health_max = m_data.Health;
        }
    }
    void Start()
    {
        if (m_init)
        {
            Init();
        }

        if (!m_allow_self_collision)
        {
            foreach (var dmg_receiver_source in DamageReceivers)
            {
                var collider = dmg_receiver_source.Collider;
                var other = DamageReceivers.Where(x => x != dmg_receiver_source);

                foreach (var other_source in other)
                {
                    Physics.IgnoreCollision(collider, other_source.Collider, true);
                }
                foreach (var hammer_collider_item in m_character.ThrowModule.Hammer.Colliders)
                {
                    Physics.IgnoreCollision(collider, hammer_collider_item, true);
                }
            }
        }
    }
    public void Init(int max_health_override = -1) 
    {
        if (max_health_override != -1 && max_health_override>0)
        {
            m_health_max = max_health_override;
        }

        Health = m_health_max;
        m_is_dead = false;
        m_killed_by = null;
    }
    public bool CheckCollision(Collision collision, ReceiveDamageModule receiveDamageModule)
    {
        var collider = collision.collider;
        //Debug.Log($"<b>Shield.OnCollisionEnter</b> collider: {collider.name}");

        var check = collider.gameObject.CompareTag("Hammer");

        if (check)
        {
            //Debug.Log($"<b>HealthModule.CheckCollision</b> {name} {Health}",gameObject);
            if (receiveDamageModule.ReceivedDamage >= Health)
            {
                m_killed_by = collider.gameObject;
            }
            Health -= receiveDamageModule.ReceivedDamage;
        }


        return check;
    }
    async void OnHpChange()
    {
        //Debug.Log($"<b>Shield.OnHpChange</b>");
        if (m_invulnerability_time > 0)
        {

            m_is_recovering = true;

            m_invulnerability_time_left = m_invulnerability_time;

            gameObject.layer = m_disabled_layer;

            m_rb.velocity = Vector3.zero;
            m_rb.isKinematic = true;

#if UNITY_WEBGL
            this.ReverseTimerV(m_invulnerability_time_left, (prog, delta) =>
            {
                m_invulnerability_time_left -= delta;
                restore_progress = prog;
            }, () => false, () =>
            {
#elif !UNITY_WEBGL
            await AsyncExtensions.ReverseTimer(m_invulnerability_time_left, (prog, delta) =>
            {
                m_invulnerability_time_left -= delta;
                restore_progress = prog;
            }, () => false);
#endif


         m_rb.isKinematic = false;
          restore_progress = 0f;

          m_invulnerability_time_left = 0f;

          gameObject.layer = m_enabled_layer;
          m_is_recovering = false;

#if UNITY_WEBGL
     });
#endif
        }
    }

#if UNITY_EDITOR
    [ContextMenu(nameof(SimulateDeath))]
    void SimulateDeath()
    {
        Health = 0;
    }

#endif

    //public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    //{
    //    if (stream.IsWriting)
    //    {
    //        stream.SendNext(m_health_current);
    //        stream.SendNext(m_is_dead);
    //        stream.SendNext(m_is_recovering);
    //    }
    //    else
    //    {
    //        m_health_current = (int)stream.ReceiveNext();
    //        m_is_dead = (bool)stream.ReceiveNext();
    //        m_is_recovering = (bool)stream.ReceiveNext();
    //    }
    //}

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        var text = $"{m_character.name}\nH:{m_character.HealthModule.Health}";
        UnityEditor.Handles.Label(transform.position, text);
    } 
#endif
}

[Serializable]
public class UnityEventInt : UnityEvent<int> { }
