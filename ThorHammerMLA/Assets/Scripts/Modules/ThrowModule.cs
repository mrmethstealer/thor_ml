﻿using UnityEngine;
using UnityEngine.Events;

using System;

#if UNITY_WEBGL
using System.Collections;
using UnityUseful.CorotineInstructionsWrappers;
#elif !UNITY_WEBGL
using System.Threading.Tasks;
using static UnityUseful.AsyncExtensions.AsyncExtensions;
#endif

public class ThrowModule : MonoBehaviour
{
    static int hash_throw = Animator.StringToHash("Throw");
    static int hash_catch = Animator.StringToHash("Catch");
    static int hash_has_hammer = Animator.StringToHash("HasHammer");


    [Header("Config:")]
    [SerializeField] GameData m_data;

    [Header("Components:")]
    [SerializeField] Animator m_animator;
    [SerializeField] ParticleSystem m_strength_particles_pref;
    [SerializeField] Transform m_hand_point;
    [SerializeField] Hammer m_hammer;

    [Header("Init Data:")]
    [SerializeField] float m_attack_cooldown = 1f;
    [SerializeField] float m_max_power_time = 2f;
    [SerializeField] float m_throw_force_impulse = 300f;

    [Header("Animation delay:")]
    [SerializeField] float m_throw_delay_ms = .3f;
    [SerializeField] float m_catch_main_delay = .5f;
    [SerializeField] float m_catch_last_point_delay = .25f;

    [Header("Runtime:")]
    [SerializeField] float m_cur_attack_cooldown;
    [SerializeField] float m_current_power;
    [SerializeField] float m_power_progress;
    [Space]
    [SerializeField] bool m_has_hammer;
    [SerializeField] bool m_use_power_increase = true;
    [SerializeField] bool m_allow_power_accumulation_over_time;
    [Space]
    [SerializeField] Vector3 m_hammer_base_scale;

    #region Properties
    public bool CanAttack { get => m_cur_attack_cooldown <= 0; }
    public bool HasHammer
    {
        get => m_has_hammer;
        set
        {
            m_has_hammer = value;
            m_animator.SetBool(hash_has_hammer, value);
        }
    }
    public Transform HammerPoint { get => m_hand_point; }

    public float AttackDelay { get => m_attack_cooldown; }
    public float CatchMainDelay { get => m_catch_main_delay; set => m_catch_main_delay = value; }
    public float PowerProgress
    {
        get => m_power_progress;
        set
        {
            if (m_power_progress < .5f && value > .5f)
            {
                //Debug.Log($"[Strength Event] {name}");
                var ps = Instantiate(m_strength_particles_pref, transform.position, m_strength_particles_pref.transform.rotation);
                Destroy(ps.gameObject, 2f);
                OnPower?.Invoke();
            }
            m_power_progress = value;
            m_hammer.StrengthProgress = value;
        }
    }
    public Hammer Hammer { get => m_hammer; }
    public bool AllowPowerIncreaseOverTime { get => m_allow_power_accumulation_over_time; set => m_allow_power_accumulation_over_time = value; }
    public float CurrentAttackCooldown { get => m_cur_attack_cooldown; }
    #endregion

    public UnityEvent OnPower;


    void Awake()
    {
        if (m_data)
        {
            m_attack_cooldown = m_data.CharacterAttackCooldown;
        }
    }
    void Start()
    {
        if (m_hammer)
        {
            var character = GetComponent<Character>();
            m_hammer.Init(character);
            m_has_hammer = true;
            m_hammer_base_scale = m_hammer.ColliderSize;
        }
    }
#if MLAGENTS
    void FixedUpdate() /// ML Learning
    {
        var delta = Time.fixedDeltaTime;
#elif !MLAGENTS
    void Update()
    {
        var delta = Time.deltaTime;
#endif
        if (m_cur_attack_cooldown > 0)
        {
            m_cur_attack_cooldown = Mathf.Clamp(m_cur_attack_cooldown - delta, 0, float.MaxValue);
        }
        if (m_use_power_increase && m_allow_power_accumulation_over_time && m_has_hammer)
        {
            m_current_power = Mathf.Clamp(m_current_power + delta, 0, m_max_power_time);
            PowerProgress = m_current_power / m_max_power_time;
            m_hammer.ColliderSize = m_hammer_base_scale * (1 + PowerProgress);
        }
    }

    public async void HammerGetBack(Action OnCatch = null)
    {
        if (!m_has_hammer && m_cur_attack_cooldown <= 0 && !m_hammer.IsReturning)
        {
            m_hammer.HammerGetBack(m_hand_point, m_catch_main_delay, m_catch_last_point_delay);

#if !UNITY_WEBGL
            await Delay(m_catch_main_delay); 
#elif UNITY_WEBGL
            this.TimerV(m_catch_main_delay,()=> 
            {
#endif

            if (m_animator)
            {
                m_animator.SetTrigger(hash_catch);

                m_current_power = 0;
                PowerProgress = 0;
                m_hammer.ColliderSize = m_hammer_base_scale;

                OnCatch?.Invoke();
            }
#if UNITY_WEBGL
            }); 
#endif
        }

    }

    void OnDrawGizmosSelected()
    {
        if (m_hand_point && m_hammer)
        {
            Gizmos.DrawWireSphere(m_hand_point.position, 0.05f);

            Gizmos.DrawLine(m_hand_point.position, m_hammer.transform.position);
        }
    }
#if !UNITY_WEBGL
    public async Task ThrowHammer(Vector3? target = null, float override_cd = -1f, Action<float> OnAttack = null)
    {
        //Debug.Log($"<b>Character.ThrowHammer</b> target: {target}, cd: {override_cd}");
        if (HasHammer && m_cur_attack_cooldown <= 0)
        {
            if (target.HasValue)
            {
                var dir = target.Value - transform.position;
                transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
                //transform.LookAt(target,Vector3.up);
            }

            if (override_cd >= 0)
            {
                m_cur_attack_cooldown = override_cd;
                OnAttack?.Invoke(override_cd);
            }
            else
            {
                m_cur_attack_cooldown = m_attack_cooldown;
                OnAttack?.Invoke(m_attack_cooldown);
            }

            HasHammer = false;


            m_animator.SetTrigger(hash_throw);

            await Delay(m_throw_delay_ms);

            m_hammer.HammerShoot(m_throw_force_impulse);

            await Delay(.1f);
        }

    }
#elif UNITY_WEBGL
    public IEnumerator ThrowHammer(Vector3? target = null, float override_cd = -1f, Action<float> OnAttack = null)
    {
        //Debug.Log($"<b>Character.ThrowHammer</b> target: {target}, cd: {override_cd}");
        if (HasHammer && m_cur_attack_cooldown <= 0)
        {
            if (target.HasValue)
            {
                var dir = target.Value - transform.position;
                transform.rotation = Quaternion.LookRotation(dir, Vector3.up);
                //transform.LookAt(target,Vector3.up);
            }

            if (override_cd >= 0)
            {
                m_cur_attack_cooldown = override_cd;
                OnAttack?.Invoke(override_cd);
            }
            else
            {
                m_cur_attack_cooldown = m_attack_cooldown;
                OnAttack?.Invoke(m_attack_cooldown);
            }

            HasHammer = false;


            m_animator.SetTrigger(hash_throw);

            yield return new WaitForSeconds(m_throw_delay_ms);

            m_hammer.HammerShoot(m_throw_force_impulse);

            yield return new WaitForSeconds(.1f);
        }
    } 
#endif


#if UNITY_EDITOR
    [Header("Gizmos:")]
    [SerializeField] float m_arc_angle = 45;
    [SerializeField] float m_arc_distance = 10;
    [Space]
    [SerializeField] Color m_arc_color = Color.green;
    [SerializeField] Vector3 m_arc_normal = Vector3.forward;
    void OnDrawGizmos()
    {
        var rot = transform.localRotation;
        var pos = transform.position;
        var start_angle = (90 + m_arc_angle / 2f) * Mathf.Deg2Rad;
        var local_dir = new Vector3(m_arc_distance * Mathf.Cos(start_angle), 0, m_arc_distance * Mathf.Sin(start_angle));

        var world_from_point = transform.TransformPoint(local_dir);
        var world_to_point = transform.TransformPoint(new Vector3(-local_dir.x, local_dir.y, local_dir.z));

        Gizmos.color = m_arc_color;
        Gizmos.DrawLine(pos, world_from_point);
        Gizmos.DrawLine(pos, world_to_point);

        UnityEditor.Handles.color = m_arc_color;
        UnityEditor.Handles.DrawWireArc(pos, transform.up, transform.TransformDirection(local_dir), m_arc_angle, m_arc_distance);
        m_arc_distance = UnityEditor.Handles.ScaleValueHandle(m_arc_distance, pos + transform.forward * m_arc_distance, rot, 1, UnityEditor.Handles.ArrowHandleCap, 1);
    }
#endif
}
