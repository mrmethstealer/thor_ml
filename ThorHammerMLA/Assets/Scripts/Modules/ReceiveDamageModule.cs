﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

using Random = UnityEngine.Random;

using UnityUseful.Misc;

[RequireComponent(typeof(Collider))]
public class ReceiveDamageModule : MonoBehaviour
{
    [SerializeField, HideInInspector] Collider m_collider;
    [SerializeField] HealthModule m_health_module;
    [Space]
    [SerializeField, Reorderable] ParticleSystem[] m_hit_particles_pref;
    [Space]
    [SerializeField] int m_contact_hit_ps_limit = 1;
    [SerializeField] int m_received_damage = 1;
    [Space]
    [SerializeField] int m_received_damage_factor = 1;
    [SerializeField] float m_particles_lifetime = 3f;

    #region Properties
    public int ReceivedDamage { get => m_received_damage * m_received_damage_factor; set => m_received_damage = value; }
    public ParticleSystem[] HitParticlesPrefs { get => m_hit_particles_pref; set => m_hit_particles_pref = value; }
    public Collider Collider { get => m_collider; }
    public HealthModule HealthModule { get => m_health_module; }
    #endregion

    #region Events
    public event Func<Collision, ReceiveDamageModule, bool> m_on_collision;
    public event Action<Collision> OnCollision;
    #endregion

    void Awake()
    {
        m_collider = GetComponent<Collider>();
    }
    void OnEnable()
    {
        m_health_module.DamageReceivers.Add(this);
    }
    void OnDisable()
    {
        m_health_module.DamageReceivers.Remove(this);
    }

    void OnCollisionEnter(Collision collision)
    {
        var success_check = m_health_module.CheckCollision(collision, this);

        if (success_check)
        {
            var contact_points = collision.contacts;
            for (int i = 0; i < contact_points.Length; i++)
            {
                var contact = contact_points[i];
                if (i < m_contact_hit_ps_limit)
                {
                    SpawnHitParticles(contact.point);
                }
            }
        }
        OnCollision?.Invoke(collision);
    }

    void SpawnHitParticles(Vector3 contact)
    {
        foreach (var ps in m_hit_particles_pref)
        {
            var ps_go = Instantiate(ps, contact, Quaternion.identity);

            Destroy(ps_go.gameObject, m_particles_lifetime);
        }
    }

#if UNITY_EDITOR
    [ContextMenu(nameof(SpawnDebrisRandom))]
    void SpawnDebrisRandom()
    {
        var rand_point = transform.position + Random.onUnitSphere * .2f;
        SpawnHitParticles(rand_point);
    }
#endif

}
