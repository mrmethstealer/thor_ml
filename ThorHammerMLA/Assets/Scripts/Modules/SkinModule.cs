﻿using UnityEngine;

using System.Collections;

using UnityUseful.Misc;
using System;

public class SkinModule : MonoBehaviour
{
    [Header("Skin Data:")]
    [SerializeField] Skin m_skin;
    [Space]
    [SerializeField] SkinAsset m_skin_asset;

    [Header("Override in:")]
    [SerializeField] ReceiveDamageModule m_receive_module_shield;
    [SerializeField] ReceiveDamageModule m_receive_module_character;
    [Space]
    [SerializeField] MeshRenderer m_hammer;
    [SerializeField, Reorderable] MeshRenderer[] m_shield;
    [SerializeField] SkinnedMeshRenderer m_character;

    [SerializeField, HideInInspector] string m_name;
    [SerializeField, HideInInspector] string m_so_path = "ScriptableObjects/Skins/";
    void Start()
    {
        m_name = name;
    }
    void OnValidate()
    {
        if (Application.isPlaying)
        {
            Skin = m_skin;
        }
    }
    public Color Color
    {
        get
        {
            if (m_skin_asset)
            {
                return m_skin_asset.Color;
            }
            else
            {
                return SkinToColor(m_skin);
            }
        }
    }
    public Skin Skin
    {
        get => m_skin;
        set
        {
            if (m_skin != value)
            {
                ApplySkin(value);
                m_skin = value;
            }

        }
    }

    public static Color SkinToColor(Skin skin)
    {
        switch (skin)
        {
            case Skin.Blue: return Color.blue;
            case Skin.Green: return Color.green;
            case Skin.Orange: return Color.yellow;
            case Skin.Red: return Color.red;
            case Skin.Purple: return new Color(0.6431373f, 0, 0.9803922f);
            case Skin.Neon: return Color.cyan;
            case Skin.Pink: return new Color(1, 0, 0.8470589f);
            case Skin.DarkBlue: return new Color();
            case Skin.DarkGreen: return new Color();
            case Skin.DarkOrange: return new Color();
            case Skin.DarkPink: return new Color();
            default: return Color.white;
        }
    }
    public void ApplySkin(Skin skin)
    {
        if (enabled)
        {
            Debug.Log($"<b>SkinModule.ApplySkin</b> {skin.ToString()}");
            if (!m_skin_asset)
            {
                //Debug.Log($"<b>SkinModule.ApplySkin</b> SkinAsset is missing.Loading from {m_so_path} ", gameObject);
            }
            m_skin_asset = Resources.Load<SkinAsset>(m_so_path + skin.ToString());

            if (m_skin_asset)
            {
                name = $"{m_name} {m_skin}";

                m_receive_module_shield.HitParticlesPrefs = m_skin_asset.ShieldCollideParticles;
                m_receive_module_character.HitParticlesPrefs = m_skin_asset.CharacterCollideParticles;

                m_hammer.material = m_skin_asset.HammerSkinMaterial;

                var parent = GetComponent<Character>().ThrowModule.Hammer.CenterPoint;
                Array.ForEach(m_skin_asset.HammerParticles, x =>
                {
                    var ps = Instantiate(x, parent);
                });

                m_character.material = m_skin_asset.CharSkinMaterial;
                Array.ForEach(m_shield, x => x.material = m_skin_asset.ShieldSkinMaterial);
            }
            else
            {
                //Debug.LogError($"<b>SkinModule.ApplySkin</b> Load from Resources. SkinAsset[{skin.ToString()}] is missing", gameObject);
            }
        }
    }
}

#region Enums
public enum Skin : sbyte
{
    None=-1,
    Blue,
    Green,
    Purple,
    Red,
    Neon,
    Orange,
    Pink,
    DarkOrange,
    DarkGreen,
    DarkBlue,
    DarkPink,
}
#endregion
